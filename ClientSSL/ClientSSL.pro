#-------------------------------------------------
#
# Project created by QtCreator 2017-10-18T13:03:09
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClientSSL
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

unix {
#    LIBS += -L$$PWD/../Dependencies/scanner/ -lScanner

#    INCLUDEPATH += $$PWD/../Dependencies/scanner/include
#    DEPENDPATH += $$PWD/../Dependencies/scanner/include
}

win32 {
    LIBS += -L$$PWD/../Dependencies/scanner/ -lScanner

    INCLUDEPATH += $$PWD/../Dependencies/scanner/include
    DEPENDPATH += $$PWD/../Dependencies/scanner/include

    win32-msvc*: QMAKE_LFLAGS += /DYNAMICBASE:NO
}

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    client.cpp \
    logindialog.cpp \
    registerdialog.cpp \
    defaultdialog.cpp

HEADERS += \
        mainwindow.h \
    client.h \
    constants.h \
    logindialog.h \
    registerdialog.h \
    defaultdialog.h

FORMS += \
        mainwindow.ui

