#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QObject>

// STRING MESSAGES

// Communication messages identifiers
static const QString TEXT_MESSAGE = "TEXT";
static const QString IMAGE_MESSAGE = "IMAGE";
static const QString REGISTRATION = "REGISTER";
static const QString LOGIN = "LOGIN";
static const QString LOGIN_SUCCESS = "LOGIN_SUCCESS";
static const QString LOGIN_FAILED = "LOGIN_FAILED";
static const QString LOGIN_NEW = "LOGIN_NEW";
static const QString LOGIN_INACTIVE = "LOGIN_INACTIVE";
static const QString REGISTER_SUCCESS = "REGISTER_SUCCESS";
static const QString REGISTER_FAILED = "REGISTER_FAILED";
static const QString REGISTER_IMAGE = "REGISTER_IMAGE";
static const QString FIND_IMAGE = "FIND_IMAGE";
static const QString LOGIN_IMAGE = "LOGIN_IMAGE";

// Status messages
static const QString CONNECTION_ESTABLISHED = "Connection to server ESTABLISHED";
static const QString CONNECTION_CLOSED = "Connection to server CLOSED";
static const QString HANDSHAKE_SUCCESS = "SSL handshake successful";
static const QString DUPLICITE_CONNECTION = "Connection already established";
static const QString NO_CONNECTION = "No established connection";
static const QString PATH_NOT_PROVIDED = "Path was not provided";
static const QString IMAGE_CORRUPTED = "Image not loaded correctly or at all";
static const QString NOT_IMPLEMENTED = "This functionality is not implemented yet";
static const QString NOT_IMPLEMENTED_SYSTEM = "This functionality is not implemented for your operating system yet";

// LOGIN/REGISTER STRINGS
static const QString LOGIN_WINDOW_TITLE = "Enter your credentials";
static const QString USER_LABEL = "Username";
static const QString PASSWORD_LABEL = "Password";
static const QString PASSWORD_LABEL_REPEAT = "Repeat Password";
static const QString LOGIN_BUTTON = "Login";
static const QString REGISTER_BUTTON = "Register";
static const int MIN_USERNAME_LEN = 6;
static const QString USERNAME_TOO_SHORT = QStringLiteral("Your username is too short, required number of characetrs is %1.").arg(MIN_USERNAME_LEN);
static const QString PASSWORD_WEAK = "Your password is too weak, it have to cotain at least 8 symbols, one special character, one upper character and one number.";
static const QString NOT_MATCHING_PASSWORDS = "Entered passwords do not match, prease try again.";
static const QString LOGIN_FAIL_STATUS = "Login process was unsuccessful, probably incorrect credentials.";
static const QString LOGIN_NEW_STATUS = "Login process was unsuccessful. \nYour account is not yet activated. \nTry again later or contact administrator.";
static const QString LOGIN_INACTIVE_STATUS = "Login process was unsuccessful, your account was deactivated.";
static const QString REGISTER_FAIL_STATUS = "Registration process was unsuccessful, probably username already in use.";
static const QString REGISTER_SUCCESS_STATUS = "Registration was successful. \nPlease wait for activation of your account by admin.";

// OTHER CONSTANTS
static const bool ALLOW_CLIENT_LIST = false;
static const bool CLEAN_EDITS = false;
static const int PORT_NUMBER = 10000;

static const QString IMAGE_DATA_NAME = "name";
static const QString FILE_DIALOG_TEXT = "Select Image";
static const QString IMAGE_SCAN_FORMAT = ".tif";
static const QString STRING_GENER_POOL = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

static const QString PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[/*_:<>?.!@#$%^&+=])(?=\\S+$).{8,}$";

static const int LABEL_WIDTH = 261;
static const int LABEL_HEIGHT = 291;
#endif // CONSTANTS_H
