#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSslSocket>
#include <QTcpSocket>
#include <QFileDialog>
#include <QBuffer>

#include "client.h"
#include "logindialog.h"

#ifdef Q_OS_WIN32
#include "scanner.h"
#endif

namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class - represents main window of client application
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief MainWindow - default constructor that connects signals from instance of Client class to MainWindow
     * @param QWidget* parent
     */
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

private slots:
    /**
     * @brief log_message - writes message to debug console and status bar of application
     * @param QString& message
     */
    void log_message(const QString& message);

    // window events
    /**
     * @brief on_connect_clicked - tries connecting to server by using connectToServer() method of Client class
     */
    void on_connect_clicked();

    /**
     * @brief on_disconnect_clicked - calls disconnectFromServer() method of Client class
     */
    void on_disconnect_clicked();

    /**
     * @brief on_pushButton_clicked - sends text message to server
     */
    void on_pushButton_clicked();

    // status methods

    /**
     * @brief statusCryptoConnected - calls log_message() method with message stating successful SSL handshake
     */
    void statusCryptoConnected();

    /**
     * @brief onConnectionStatusSuccess - calls log_message() method with message stating successful establishment
     * of connection, creates new LoginDialog and connects it with self and Client class
     */
    void onConnectionStatusSuccess();

    /**
     * @brief onConnectionStatusClosed - calls log_message() method with message stating closed connection
     */
    void onConnectionStatusClosed();

    /**
     * @brief onSendReply - displays reply from server in intended plain text window
     * @param const QString& message
     */
    void onSendReply(const QString& message);

    /**
     * @brief onSendStatusMessage - calls log_message() method with message received from other classes trough events
     * @param const QString& message
     */
    void onSendStatusMessage(const QString& message);

    // file management
    /**
     * @brief on_tryLoadImage_clicked - tries to load file using file dialog, saves it to local variable and
     * shows it in app window
     */
    void on_tryLoadImage_clicked();

    /**
     * @brief on_sendImageButton_clicked - if image is loaded and client app is contected to server and calls
     * sendImage() method of Client class
     */
    void on_sendImageButton_clicked();

    // scanner management

    /**
     * @brief on_scanImage_clicked - if scanner is connected, tries to start fingerprint scanning process
     */
    void on_scanImage_clicked();

    /**
     * @brief sendLoginFinger - sends fingerprint in login mode together with username
     * @param const QString& username
     */
    void sendLoginFinger(const QString& username);

private:
    /**
     * @brief ui - instance of Ui::MainWindow containing poiters to all window components
     */
    Ui::MainWindow *ui;

    /**
     * @brief client - instance of Client class
     */
    Client client;

    /**
     * @brief unprocessedImage - global variable representing image that will be sent to server
     */
    QImage unprocessedImage;
};

#endif // MAINWINDOW_H
