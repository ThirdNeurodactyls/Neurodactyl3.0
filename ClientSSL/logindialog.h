#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include "constants.h"
#include "defaultdialog.h"
#include "registerdialog.h"

/**
 * @brief The LoginDialog class - child of DefaultDialog window used in login process
 */
class LoginDialog : public DefaultDialog
{
Q_OBJECT

private:
    /**
     * @brief QComboBox* comboUsername - combo box for choosing of username from list of registered users
     * (only available if allowCombo is true) NOT IMPLEMENTED YET.
     */
    QComboBox* comboUsername;

    /**
     * @brief bool finger - if true, login dialog will be displayed in fingerprint mode
     */
    bool finger = false;

    /**
     * @brief allowCombo - if true username during login process can be chosen from comboUsername combo box
     * else has to be written manually
     */
    bool allowCombo = ALLOW_CLIENT_LIST;

protected:
    /**
     * @brief setUpDialog - see setUpDialog() of DefaultDialog
     */
    void setUpDialog();

    /**
     * @brief checkInputs - see checkInputs() of DefaultDialog
     * @param const QString& username
     * @return bool
     */
    bool checkInputs(const QString& username);

    /**
     * @brief maybeClearInputs - maybe clears inputs, but who knows
     */
    void maybeClearInputs();

public:
    /**
     * @brief LoginDialog - constructor of LoginDialog class. Calls setUpDialog and sets window title
     * @param QWidget* parent - person (usually biologically connected) that takes care of child during childhood.
     */
    explicit LoginDialog(bool finger, QWidget* parent = 0);

    /**
     * @brief QString& setUsername - NOT IMPLEMENTED YET
     */
    void setUsername(QString&);

    /**
     * @brief QString& setPassword - NOT IMPLEMENTED YET
     */
    void setPassword(QString&);

    /**
     * @brief setUsernameList - sets list of usernames to comboUsername combo box - NOT IMPLEMENTED YET
     * @param const QStringList& usernames - list of usernames fetched from server database
     */
    void setUsernameList(const QStringList& usernames);

signals:

    /**
     * @brief tryLogin - sends signal with login data to Client class to attempt login process
     * @param const QString& username
     * @param const QString& hash - hashed user password with salt
     */
    void tryLogin(const QString& username, const QString& hash);

    /**
     * @brief tryLoginWithFinger - sends signal with login data to MainWindow class to attempt login process using fingerprint
     * @param const QString& username
     */
    void tryLoginWithFinger(const QString& username);

    /**
     * @brief pushRegister - sends signal with registration data to Clent class (is listeening for this sinal) to attempt registration.
     * Data are received from RegistrationDialog class
     * @param const QString& username
     * @param const QString& hash - hashed user password with salt
     */
    void pushRegister(const QString& username, const QString& hash);

    /**
     * @brief pushRegisterReplyStatus - sends registration reply status from Client class to RegistrationDialog (that is listenig for this signal)
     * @param bool status
     */
    void pushRegisterReplyStatus(bool status);

    /**
     * @brief cancelLogin - on canceling of login dialog sends signal to end connection to server
     */
    void cancelLogin();

public slots:
    /**
     * @brief onPressLogin - when login button is pressed, checks user input and emits tryLogin signal
     */
    void onPressLogin();

    /**
     * @brief onPressRegister - when register button is pressed, creates RegisterDialog window, connets signals and shows it
     */
    void onPressRegister();

    /**
     * @brief onLoginReplyStatus - based on received login status from server accepts login dialog or alerts login failure
     * @param const QString& satus
     */
    void onLoginReplyStatus(const QString& satus);
};

#endif // LOGINDIALOG_H
