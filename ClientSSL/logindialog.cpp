#include "logindialog.h"

LoginDialog::LoginDialog(bool finger, QWidget *parent) : DefaultDialog(parent)
{
    this->finger = finger;
    setUpDialog();
    setWindowTitle(tr(LOGIN_WINDOW_TITLE.toStdString().c_str()));
    setModal(true);
}

void LoginDialog::setUpDialog()
{
    QGridLayout *formGrid = new QGridLayout(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    labelUsername = new QLabel(this);
    labelPassword = new QLabel(this);
    labelUsername->setText(tr(USER_LABEL.toStdString().c_str()));
    labelPassword->setText(tr(PASSWORD_LABEL.toStdString().c_str()));

    if (allowCombo)
    {
        comboUsername = new QComboBox(this);
        comboUsername->setEditable(true);
        labelUsername->setBuddy(comboUsername);
    }
    else
    {
        editUsername = new QLineEdit(this);
        labelUsername->setBuddy(editUsername);
    }

    editPassword = new QLineEdit(this);
    editPassword->setEchoMode(QLineEdit::Password);
    labelPassword->setBuddy(editPassword);

    buttons = new QDialogButtonBox(this);
    buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Open);
    buttons->button(QDialogButtonBox::Ok)->setText(tr(LOGIN_BUTTON.toStdString().c_str()));
    buttons->button(QDialogButtonBox::Open)->setText(tr(REGISTER_BUTTON.toStdString().c_str()));

    connect(buttons->button(QDialogButtonBox::Ok), SIGNAL(clicked()), this, SLOT(onPressLogin()));
    connect(buttons->button(QDialogButtonBox::Open), SIGNAL(clicked()), this, SLOT(onPressRegister()));
    connect(this, SIGNAL(rejected()), this, SIGNAL(cancelLogin()));

    alert = new QMessageBox(this);

    QRegularExpression regex(PASSWORD_REGEX);
    pass_validator = new QRegularExpressionValidator(regex);

    formGrid->addWidget(labelUsername, 0, 0);

    if (allowCombo)
    {
        formGrid->addWidget(comboUsername, 0, 1);
    }
    else
    {
        formGrid->addWidget(editUsername, 0, 1);
    }

    if (!this->finger)
    {
        formGrid->addWidget(labelPassword, 1, 0);
        formGrid->addWidget(editPassword, 1, 1);
    }
    else
    {
        editPassword->setVisible(false);
        labelPassword->setVisible(false);
    }
    formGrid->addWidget(buttons, 2, 1, 2, 3);
}

bool LoginDialog::checkInputs(const QString& username)
{
    bool valid = true;
    QString text = "";
    if (username.length() < MIN_USERNAME_LEN)
    {
        text += "\n" + USERNAME_TOO_SHORT + "\n";
        valid = false;
    }
    int pos = 0;
    QString pass = editPassword->text();
    if (pass_validator->validate(pass, pos) != QValidator::Acceptable && !this->finger)
    {
        text += "\n" + PASSWORD_WEAK + "\n";
        valid = false;
    }
    if (valid == false)
    {
        alert->setText(tr(text.toStdString().c_str()));
        alert->exec();
    }
    return valid;
}

void LoginDialog::onPressRegister()
{
    RegisterDialog *rd = new RegisterDialog(this);
    connect(rd, SIGNAL(tryRegister(const QString&,const QString&)), this, SIGNAL(pushRegister(const QString&,const QString&)));
    connect(this, SIGNAL(pushRegisterReplyStatus(bool)), rd, SLOT(onPushRegisterReplyStatus(bool)));
    rd->setAttribute(Qt::WA_DeleteOnClose);
    rd->show();
}

void LoginDialog::onLoginReplyStatus(const QString& status)
{
    if ((QString::compare(status, LOGIN_SUCCESS) == 0))
    {
        this->accept();
    }
    else if (QString::compare(status, LOGIN_FAILED) == 0)
    {
        alert->setText(tr(LOGIN_FAIL_STATUS.toStdString().c_str()));
        this->maybeClearInputs();
        alert->exec();
    }
    else if (QString::compare(status, LOGIN_NEW) == 0)
    {
        alert->setText(tr(LOGIN_NEW_STATUS.toStdString().c_str()));
        this->maybeClearInputs();
        alert->exec();
    }
    else if (QString::compare(status, LOGIN_INACTIVE) == 0)
    {
        alert->setText(tr(LOGIN_INACTIVE_STATUS.toStdString().c_str()));
        this->maybeClearInputs();
        alert->exec();
    }
}

void LoginDialog::maybeClearInputs()
{
    if (clearEditsOnErr)
    {
        editUsername->clear();
        editPassword->clear();
    }
}

void LoginDialog::onPressLogin()
{
    QString username = allowCombo == true ? comboUsername->currentText() : editUsername->text();
    if (checkInputs(username))
    {
        if (!this->finger)
            emit tryLogin(username, getSha256Hash(username, editPassword->text()));
        else
            emit tryLoginWithFinger(username);
    }
}
