#ifndef DEFAULTDIALOG_H
#define DEFAULTDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QComboBox>
#include <QGridLayout>
#include <QStringList>
#include <QDebug>
#include <QCryptographicHash>
#include <QMessageAuthenticationCode>
#include <QRegularExpression>
#include <QMessageBox>
#include <QCryptographicHash>

#include "constants.h"

/**
 * @brief The DefaultDialog class is parent class of LoginDialog and RegisterDialog since they share many
 * common parameters.
 */
class DefaultDialog : public QDialog
{
Q_OBJECT

protected:
    /**
     * @brief QLabel* labelUsername - label for username line edit
     */
    QLabel* labelUsername;

    /**
     * @brief QLabel* labelPassword - label for password line edit
     */
    QLabel* labelPassword;

    /**
     * @brief QLineEdit* editUsername - line edit for entering username durimng login process or registration
     */
    QLineEdit* editUsername;

    /**
     * @brief QLineEdit* editPassword - line edit for entering password durimng login process or registration
     */
    QLineEdit* editPassword;

    /**
     * @brief QDialogButtonBox* buttons - button box for controlling dialog (any buttons may be inserted)
     */
    QDialogButtonBox* buttons;

    /**
     * @brief QValidator* pass_validator - regex validator for controlling user password strength
     */
    QValidator* pass_validator;

    /**
     * @brief QMessageBox* alert - popup window for displaying messages during login or registration process
     */
    QMessageBox* alert;

    /**
     * @brief bool clearEditsOnErr - if true, all line edits will be cleared on unsuccessful login/register attempt
     */
    bool clearEditsOnErr = CLEAN_EDITS;

    /**
     * @brief setUpDialog - method responsible for building given dialog window (creating elements, placig
     * them to grid and setting up listeners)
     */
    void setUpDialog();

    /**
     * @brief chcekInputs - checks user inputs for different criteria, triggers allert on invalid inputs
     * @param const QString& username - username entered in login/registration process
     * @return bool - true if all inputh valid, false othervise
     */
    bool chcekInputs(const QString& username);

    /**
     * @brief getSha256Hash - creates SHA-256 hash from given data (password) and key (salt)
     * @param const QString& key - salt added during hashing process
     * @param const QString& data - data to be hashed (user password)
     * @return hash
     */
    QString getSha256Hash(const QString& key, const QString& data);

//    /**
//     * @brief PBKDF2 - Password based key derivation functon v.2 generates hash of given length based on
//     * given salt, password and number of iterations
//     * @param key - user password
//     * @param salt - aditional data to raise etropy of user password (should be randomply  generated of length +-8 bytes)
//     * @param bytes - number of bytes of generated key
//     * @param iters - number of iterations of hash function
//     * @return hash
//     */
//    QString PBKDF2(const QString& key, const QString& salt, int bytes, int iters);

public:
    /**
     * @brief DefaultDialog - constructor of DefaultDialog class
     * @param QWidget* parent
     */
    explicit DefaultDialog(QWidget *parent = 0);
};

#endif // DEFAULTDIALOG_H
