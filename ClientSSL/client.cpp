#include "client.h"

Client::Client(QObject *parent) : QObject(parent) {
    this->connected = false;
    connect(&clientSocket, SIGNAL(readyRead()), this, SLOT(readyToRead()));
    connect(&clientSocket, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErr(QList<QSslError>)));
    connect(&clientSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect(&clientSocket, SIGNAL(encrypted()), this, SLOT(connectionEstablished()));
}

void Client::connectToServer(const QString& host, int port)
{
    if (!this->isConnected())
    {
        clientSocket.connectToHostEncrypted(host, port);
    }
    else
    {
        emit sendStatusMessage(DUPLICITE_CONNECTION);
    }
}

void Client::disconnectFromServer()
{
    if (this->isConnected())
    {
        clientSocket.disconnectFromHost();
    }
    else
    {
        emit sendStatusMessage(NO_CONNECTION);
    }
}

bool Client::isConnected()
{
    return this->connected;
}

void Client::sendText(const QString& message)
{
    if(clientSocket.isEncrypted())
    {
        clientSocket.write(TEXT_MESSAGE.toStdString().c_str());
        clientSocket.write("\n");
        clientSocket.write(message.toStdString().c_str());
    }
}

long Client::sendImage(QImage& image, const QString& type, const QString& uname)
{
    if (!image.isNull())
    {
        QByteArray ba;
        QBuffer buffer(&ba);
        image.save(&buffer, "PNG");

        if (clientSocket.isEncrypted())
        {
            clientSocket.write(IMAGE_MESSAGE.toStdString().c_str());
            clientSocket.write("\n");
            clientSocket.write(type.toStdString().c_str());
            clientSocket.write("\n");
            if (QString::compare(type, LOGIN_IMAGE) == 0 && QString::compare(uname, "") != 0)
            {
                clientSocket.write(uname.toStdString().c_str());
                clientSocket.write("\n");
            }
            clientSocket.write(QString::number(ba.size()).toStdString().c_str());
            clientSocket.write("\n");
            clientSocket.write(ba);
        }
        return ba.size();
    }
    return -1;
}

QString Client::getRandomString(int len)
{
    const QString possibleCharacters(STRING_GENER_POOL);
    QString randomString;
    for(int i = 0; i < len; ++i)
    {
       int index = qrand() % possibleCharacters.length();
       QChar nextChar = possibleCharacters.at(index);
       randomString.append(nextChar);
    }
    return randomString;
}

/* SLOTS */

void Client::sslErr(QList<QSslError> errors)
{
    clientSocket.ignoreSslErrors();
    for (int i=0; i<errors.length(); i++) {
        qDebug() << errors.at(i).errorString();
    }
}

void Client::connectionEstablished()
{
    this->connected = true;
    emit connectionStatusSuccess();
}

void Client::onDisconnected()
{
    this->connected = false;
    emit connectionStatusClosed();
}

void Client::onTryLogin(const QString &username, const QString &hash)
{
    if(clientSocket.isEncrypted())
    {
        clientSocket.write(LOGIN.toStdString().c_str());
        clientSocket.write("\n");
        clientSocket.write(username.toStdString().c_str());
        clientSocket.write("\n");
        clientSocket.write(hash.toStdString().c_str());
    }
}

void Client::onPushRegister(const QString &username, const QString &hash)
{
    if(clientSocket.isEncrypted())
    {
        clientSocket.write(REGISTRATION.toStdString().c_str());
        clientSocket.write("\n");
        clientSocket.write(username.toStdString().c_str());
        clientSocket.write("\n");
        clientSocket.write(hash.toStdString().c_str());
    }
}

void Client::readyToRead() {
    QString message = clientSocket.readAll();

    QString output;

    if (QString::compare(message, LOGIN_SUCCESS) == 0) { emit loginReplyStatus(LOGIN_SUCCESS); }
    else if (QString::compare(message, LOGIN_FAILED) == 0) { emit loginReplyStatus(LOGIN_FAILED); }
    else if (QString::compare(message, LOGIN_NEW) == 0) { emit loginReplyStatus(LOGIN_NEW); }
    else if (QString::compare(message, LOGIN_INACTIVE) == 0) { emit loginReplyStatus(LOGIN_INACTIVE); }
    else if (QString::compare(message, REGISTER_SUCCESS) == 0) { emit registerReplyStatus(true); }
    else if (QString::compare(message, REGISTER_FAILED) == 0) { emit registerReplyStatus(false); }
    emit sendReply(message);
}
