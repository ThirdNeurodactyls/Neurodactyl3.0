#include "registerdialog.h"

RegisterDialog::RegisterDialog(QWidget *parent) : DefaultDialog(parent)
{
    setUpDialog();
    setWindowTitle(tr(LOGIN_WINDOW_TITLE.toStdString().c_str()));
    setModal(true);
}

void RegisterDialog::setUpDialog()
{
    QGridLayout *formGrid = new QGridLayout(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    labelUsername = new QLabel(this);
    labelPassword = new QLabel(this);
    labelPasswordRepeat = new QLabel(this);
    labelUsername->setText(tr(USER_LABEL.toStdString().c_str()));
    labelPassword->setText(tr(PASSWORD_LABEL.toStdString().c_str()));
    labelPasswordRepeat->setText(tr(PASSWORD_LABEL_REPEAT.toStdString().c_str()));

    editUsername = new QLineEdit(this);
    labelUsername->setBuddy(editUsername);

    editPassword = new QLineEdit(this);
    editPassword->setEchoMode(QLineEdit::Password);
    labelPassword->setBuddy(editPassword);

    editPasswordRepeat = new QLineEdit(this);
    editPasswordRepeat->setEchoMode(QLineEdit::Password);
    labelPasswordRepeat->setBuddy(editPasswordRepeat);

    buttons = new QDialogButtonBox(this);
    buttons->addButton(QDialogButtonBox::Ok);
    buttons->button(QDialogButtonBox::Ok)->setText(tr(REGISTER_BUTTON.toStdString().c_str()));

    connect(buttons->button(QDialogButtonBox::Ok), SIGNAL(clicked()), this, SLOT(onPressRegister()));
    connect(this, SIGNAL(rejected()), this, SIGNAL(cancelRegister()));

    alert = new QMessageBox(this);

    QRegularExpression regex(PASSWORD_REGEX);
    pass_validator = new QRegularExpressionValidator(regex);

    formGrid->addWidget(labelUsername, 0, 0);
    formGrid->addWidget(editUsername, 0, 1);
    formGrid->addWidget(labelPassword, 1, 0);
    formGrid->addWidget(editPassword, 1, 1);
    formGrid->addWidget(labelPasswordRepeat, 2, 0);
    formGrid->addWidget(editPasswordRepeat, 2, 1);
    formGrid->addWidget(buttons, 3, 1, 2, 3);
}

bool RegisterDialog::checkInputs(const QString& username)
{
    bool valid = true;
    QString text = "";
    if (username.length() < MIN_USERNAME_LEN)
    {
        text += "\n" + USERNAME_TOO_SHORT + "\n";
        valid = false;
    }
    int pos = 0;
    QString pass = editPassword->text();
    if (pass_validator->validate(pass, pos) != QValidator::Acceptable)
    {
        text += "\n" + PASSWORD_WEAK + "\n";
        valid = false;
    }
    if (QString::compare(editPassword->text(), editPasswordRepeat->text()) != 0)
    {
        text += "\n" + NOT_MATCHING_PASSWORDS + "\n";
    }
    if (valid == false)
    {
        alert->setText(tr(text.toStdString().c_str()));
        alert->exec();
    }
    return valid;
}

void RegisterDialog::onPressRegister()
{
    if (checkInputs(editUsername->text()))
    {
        emit tryRegister(editUsername->text(), getSha256Hash(editUsername->text(), editPassword->text()));
    }
}

void RegisterDialog::onPushRegisterReplyStatus(bool status)
{
    if (status)
    {
        alert->setText(tr(REGISTER_SUCCESS_STATUS.toStdString().c_str()));
        alert->exec();
        this->accept();
    }
    else
    {
        alert->setText(tr(REGISTER_FAIL_STATUS.toStdString().c_str()));
        if (clearEditsOnErr)
        {
            editUsername->clear();
            editPassword->clear();
            editPasswordRepeat->clear();
        }
        alert->exec();
    }
}
