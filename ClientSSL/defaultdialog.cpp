#include "defaultdialog.h"

DefaultDialog::DefaultDialog(QWidget *parent) : QDialog(parent) {}

QString DefaultDialog::getSha256Hash(const QString& key, const QString& data)
{
    QMessageAuthenticationCode code(QCryptographicHash::Sha256);
    code.setKey(key.toLocal8Bit());
    code.addData(data.toLocal8Bit());
    return QString(code.result().toHex());
}

//QString DefaultDialog::PBKDF2(const QString &key, const QString &salt, int bytes, int iters)
//{

//}

