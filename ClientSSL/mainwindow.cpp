#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&client, SIGNAL(connectionStatusSuccess()), this, SLOT(onConnectionStatusSuccess()));
    connect(&client, SIGNAL(connectionStatusClosed()), this, SLOT(onConnectionStatusClosed()));
    connect(&client, SIGNAL(sendReply(const QString&)), this, SLOT(onSendReply(const QString&)));
    connect(&client, SIGNAL(sendStatusMessage(const QString&)), this, SLOT(onSendStatusMessage(const QString&)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connect_clicked()
{
    if (ui->checkbox_finger->isChecked() && this->unprocessedImage.isNull())
    {
        log_message(IMAGE_CORRUPTED);
        return;
    }
    client.connectToServer(ui->IPhost->toPlainText(), PORT_NUMBER);
}

void MainWindow::on_disconnect_clicked()
{
    client.disconnectFromServer();
}

void MainWindow::on_pushButton_clicked()
{
    if (client.isConnected())
    {
        client.sendText(ui->plainTextEdit->toPlainText());
    }
    else
    {
        log_message(NO_CONNECTION);
    }
}

void MainWindow::on_tryLoadImage_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr(FILE_DIALOG_TEXT.toStdString().c_str()), "", tr("*.jpg *.jpeg *.png *.tif *.bmp"));

    if (fileName != "")
    {
        ui->IPhost_2->appendPlainText(fileName);
        unprocessedImage.load(fileName);
        QFileInfo f(fileName);
        unprocessedImage.setText(IMAGE_DATA_NAME, f.fileName());
        QPixmap pix = QPixmap::fromImage(unprocessedImage);
        ui->label->setPixmap(pix.scaled(LABEL_WIDTH, LABEL_HEIGHT, Qt::KeepAspectRatio));
    }
    else
    {
        log_message(PATH_NOT_PROVIDED);
    }
}

void MainWindow::on_sendImageButton_clicked()
{
    if (client.isConnected())
    {
        if (!unprocessedImage.isNull())
        {
            long size = client.sendImage(unprocessedImage, (ui->radio_save->isChecked()) ? REGISTER_IMAGE : FIND_IMAGE);
            ui->IPhost_3->appendPlainText("Image " + unprocessedImage.text(IMAGE_DATA_NAME) + QStringLiteral(" size: %1").arg(size));
        }
        else
        {
            log_message(IMAGE_CORRUPTED);
        }
    }
    else
    {
        log_message(NO_CONNECTION);
    }
}

void MainWindow::log_message(const QString& message)
{
    qDebug() << message;
    ui->statusBar->showMessage(message);
}

void MainWindow::onSendStatusMessage(const QString& message)
{
    log_message(message);
}

void MainWindow::onConnectionStatusSuccess()
{
    log_message(CONNECTION_ESTABLISHED);

    LoginDialog *lg = new LoginDialog(ui->checkbox_finger->isChecked(), this);
    connect(lg, SIGNAL(tryLogin(const QString&,const QString&)), &client, SLOT(onTryLogin(const QString&,const QString&)));
    connect(lg, SIGNAL(pushRegister(const QString&,const QString&)), &client, SLOT(onPushRegister(const QString&,const QString&)));
    connect(&client, SIGNAL(loginReplyStatus(const QString&)), lg, SLOT(onLoginReplyStatus(const QString&)));
    connect(&client, SIGNAL(registerReplyStatus(bool)), lg, SIGNAL(pushRegisterReplyStatus(bool)));
    connect(&client, SIGNAL(connectionStatusClosed()), lg, SLOT(reject()));
    connect(lg, SIGNAL(cancelLogin()), this, SLOT(on_disconnect_clicked()));
    connect(lg, SIGNAL(tryLoginWithFinger(const QString&)), this, SLOT(sendLoginFinger(const QString&)));
    lg->setAttribute(Qt::WA_DeleteOnClose);
    lg->show();
}

void MainWindow::onConnectionStatusClosed()
{
    log_message(CONNECTION_CLOSED);
}

void MainWindow::statusCryptoConnected()
{
    log_message(HANDSHAKE_SUCCESS);
}

void MainWindow::onSendReply(const QString& message)
{
    ui->reply->appendPlainText("-" + message);
}

void MainWindow::sendLoginFinger(const QString &username)
{
    if (client.isConnected())
    {
        if (!unprocessedImage.isNull())
        {
            long size = client.sendImage(unprocessedImage, LOGIN_IMAGE, username);
            ui->IPhost_3->appendPlainText("Image " + unprocessedImage.text(IMAGE_DATA_NAME) + QStringLiteral(" size: %1").arg(size));
        }
        else
        {
            log_message(IMAGE_CORRUPTED);
        }
    }
    else
    {
        log_message(NO_CONNECTION);
    }
}

void MainWindow::on_scanImage_clicked()
{
    #ifdef Q_OS_WIN32
    Scanner scanner;
    unprocessedImage = scanner.scanImage();

    if (!unprocessedImage.isNull())
    {
        unprocessedImage.setText(IMAGE_DATA_NAME, client.getRandomString(16) + IMAGE_SCAN_FORMAT);
        QPixmap pix = QPixmap::fromImage(unprocessedImage);
        ui->IPhost_2->appendPlainText(unprocessedImage.text(IMAGE_DATA_NAME));
        ui->label->setPixmap(pix.scaled(LABEL_WIDTH, LABEL_HEIGHT, Qt::KeepAspectRatio));
    }
    else
    {
        log_message(IMAGE_CORRUPTED);
    }
    #else
         log_message(NOT_IMPLEMENTED_SYSTEM);
    #endif
}
