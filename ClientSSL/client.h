#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QSslSocket>
#include <QTcpSocket>
#include <QFileDialog>
#include <QBuffer>

#include "constants.h"
#include "logindialog.h"

/**
 * @brief The Client class takes care of network communication between client application and sever. Contains QSslSocket
 * that connets to distant server and makes mutual communication possible. Class is communicating with MainWindow class
 * with help of Slots and signals.
 */
class Client : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Client::Client is a constructor method for client class assigning basic event listeners,
     * namely for readyRead(), sslErrors(QList<QSslError>) and disconnected() signals.
     * @param parent
     */
    Client(QObject* parent = 0);

    /**
     * @brief Client::connectToServer connects client QSSlsocket to provided host address and port providing it is
     * not already connected. In this case warning message will be diplayed.
     * @param const QString host - IP address of server
     * @param int port - port number on which server should be listening
     */
    void connectToServer(const QString& host, int port);

    /**
     * @brief Client::disconnectFromServer disconnects client QSSlsocket from server if it is connected.
     */
    void disconnectFromServer();

    /**
     * @brief Client::sendText if connection is encrypted, text message identifier followed by
     * text message is sent to host server.
     * @param const QString message - text message that will be sent to server
     */
    void sendText(const QString& message);

    /**
     * @brief Client::sendImage if image file is loaded (provided as parameter) and connection is encrypted,
     * image identifier, size of image and image itself is sent to host server. Also sends message addressing what should be done
     * with image and if it is used in login attempt sends also username
     * @param QImage image - image that will be sent to server
     * @param const QString& type - how image should be processed
     * @param const QString& uname - optional parameter if type is login
     * @return long - size of sent image
     */
    long sendImage(QImage& image, const QString& type, const QString& uname="");

    /**
     * @brief Client::getRandomString generates a pseudo-non-crypto-random QString of given length.
     * @param int len - desired lenth of generated QString
     * @return QString - generated string of length len
     */
    QString getRandomString(int len);

    /**
     * @brief Client::isConnected returns true, if client QSSlsocket is connected, else false.
     * @return bool
     */
    bool isConnected();

private:
    /**
     * @brief QSslSocket clientSocket main feature of this class, takes care of communication with server.
     */
    QSslSocket clientSocket;

    /**
     * @brief bool connected - true, if clientSocket is connected to server, false othervise
     */
    bool connected;

signals:
    /**
     * @brief connectionStatusSuccess is emited on encrypted() event by client QSSlsocket.
     */
    void connectionStatusSuccess();

    /**
     * @brief connectionStatusClosed is emited on disconnected() event by client QSSlsocket.
     */
    void connectionStatusClosed();

    /**
     * @brief sendStatusMessage is emited when status message needs to be shown in application window
     * or console
     * @param const QString message - message that is emited by signal
     */
    void sendStatusMessage(const QString& message);

    /**
     * @brief sendReply is emited on readyToRead() event, when message is ready to read from client socket.
     * It passes message to be processed by another classes.
     * @param const QString data - data read from client socket
     */
    void sendReply(const QString& message);

    /**
     * @brief loginReplyStatus is emited when server reply on login attempt is received.
     * @param const QString& status - represents cecision of server regarding login attempt.
     */
    void loginReplyStatus(const QString& status);

    /**
     * @brief loginReplyStatus is emited when server reply on registration attempt is received.
     * @param bool status - represents cecision of server regarding registration attempt.
     */
    void registerReplyStatus(bool status);

public slots:

    /**
     * @brief Client::connectionEstablished set connected state to true and emits connectionStatusSuccess()
     *  message on connection encrypted event.
     */
    void connectionEstablished();

    /**
     * @brief Client::onDisconnected set connected state to false and emits connectionStatusClosed() message
     * on disconnected event.
     */
    void onDisconnected();

    /**
     * @brief Client::readyToRead on readyRead() signal reads message arrived at client socket and sends
     * is to further processing by sendReply event.
     */
    void readyToRead();

    /**
     * @brief Client::sslErr writes all errors in list of errors obtained by ssl error event to debug console.
     * @param QList<QSslError> errors - list of errors
     */
    void sslErr(QList<QSslError> errors);

    /**
     * @brief onTryLogin is received from LoginDialog on login attempt by emiting tryLogin() signal
     * @param const QString& username - contains username of user trying to login
     * @param const QString& hash - contains hashed and salted password entered by user
     */
    void onTryLogin(const QString& username, const QString& hash);

    /**
     * @brief onPushRegister is received from RegisterDialog on registration attempt by emiting tryRegister()
     * signal. Signal is rehandled by LoginDialog class on the way
     * @param const QString& username - contains username of user trying to login
     * @param const QString& hash - contains hashed and salted password entered by user
     */
    void onPushRegister(const QString& username, const QString& hash);
};

#endif // CLIENT_H
