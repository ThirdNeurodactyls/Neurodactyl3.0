#ifndef REGISTERDIALOG_H
#define REGISTERDIALOG_H

#include "constants.h"
#include "defaultdialog.h"

/**
 * @brief The RegisterDialog class - child of DefaultDialog window used in registration process
 */
class RegisterDialog : public DefaultDialog
{
Q_OBJECT

private:
    /**
     * @brief QLabel* labelPasswordRepeat - label for aditional input field for repeated password
     */
    QLabel* labelPasswordRepeat;

    /**
     * @brief QLineEdit* editPasswordRepeat - aditional input field for repeated password
     */
    QLineEdit* editPasswordRepeat;


protected:
    /**
     * @brief setUpDialog - see setUpDialog() of DefaultDialog
     */
    void setUpDialog();

    /**
     * @brief checkInputs - see checkInputs() of DefaultDialog
     * @param const QString& username
     * @return bool
     */
    bool checkInputs(const QString& username);

public:
    /**
     * @brief RegisterDialog - default constructor of RegisterDialog class
     * @param parent
     */
    explicit RegisterDialog(QWidget* parent = 0);

signals:
    /**
     * @brief tryRegister - signal containing username and hashed password
     * @param const QString& username
     * @param const QString& password - password hashed using Sha256 hash with salt
     */
    void tryRegister(const QString& username, const QString& password);

    /**
     * @brief cancelRegister - aborts register dialog
     */
    void cancelRegister();

public slots:
    /**
     * @brief onPressRegister - checks validity of inputs, creates hash of password and sends tryRegister() signal
     */
    void onPressRegister();

    /**
     * @brief onPushRegisterReplyStatus - slot activated by server response to register attempt. Alerts status message to user
     * @param status
     */
    void onPushRegisterReplyStatus(bool status);
};

#endif // REGISTERDIALOG_H
