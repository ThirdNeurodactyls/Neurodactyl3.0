from os import system

# MODELS
model_mobnet = 'https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/1'

# img size 224*224
model_rest_v2_150 = 'https://tfhub.dev/google/imagenet/resnet_v2_152/classification/1'

model_rest_v2_50 = 'https://tfhub.dev/google/imagenet/resnet_v2_50/classification/1'

# img size 299*299
model_incetion_v3 = 'https://tfhub.dev/google/imagenet/inception_v3/classification/1'

#settings
steps = 6000
data = '/home/test/tp2018/TRAIN_DATA_NEW/tensor_data/'
learning_rate = 0.007
evaluate = 10
batch_size = 400
val_batch_size = 400
flip = True


python = '/home/test/Downloads/anaconda3/bin/python3'

script = "/home/test/tp2018/main_project/Neurodactyl3.0/Python/retrain.py " \
         "--image_dir {} " \
         "--tfhub_module {} " \
         "--learning_rate {} " \
         "--eval_step_interval {} " \
         "--train_batch_size {} " \
         "--validation_batch_size {} " \
         "--how_many_training_steps {} " \
    .format(data, model_rest_v2_50, learning_rate, evaluate, batch_size, val_batch_size, steps)

naked_script = "/home/test/tp2018/main_project/Neurodactyl3.0/Python/retrain.py " \
         "--image_dir {} " \
         "--tfhub_module {} " \
         "--how_many_training_steps {} " \
         "--learning_rate {} " \
         "--eval_step_interval {} " \
    .format(data, model_incetion_v3, steps, learning_rate, evaluate)

system('{} {}'.format(python, script))