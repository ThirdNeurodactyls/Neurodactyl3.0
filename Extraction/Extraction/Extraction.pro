#-------------------------------------------------
#
# Project created by QtCreator 2018-05-02T21:35:28
#
#-------------------------------------------------

QT       += gui

TARGET = Extraction
TEMPLATE = lib

DEFINES += EXTRACTION_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        extraction.cpp

HEADERS += \
        extraction.h \
        extraction_global.h 

unix {

    target.path = /usr/lib
    INSTALLS += target

    unix:!macx: LIBS += -L$$PWD/../../Dependencies/preprocessing/ -lPreprocessing

    INCLUDEPATH += $$PWD/../../Dependencies/preprocessing/include
    DEPENDPATH += $$PWD/../../Dependencies/preprocessing/include

}

win32 {
    LIBS += -L$$PWD/../../../../opencv/build/x64/vc14/lib/ -lopencv_world340d

    INCLUDEPATH += $$PWD/../../../../opencv/build/include
    DEPENDPATH += $$PWD/../../../../opencv/build/include

    LIBS += -L$$PWD/'../../../../Program Files/ArrayFire/v3/lib/' -laf

    INCLUDEPATH += $$PWD/'../../../../Program Files/ArrayFire/v3/include'
    DEPENDPATH += $$PWD/'../../../../Program Files/ArrayFire/v3/include'

    LIBS += -L$$PWD/../../Preprocessing/build-Preprocessing-Desktop_Qt_5_9_1_MSVC2017_64bit-Debug/debug/ -lPreprocessing

    INCLUDEPATH += $$PWD/../../Preprocessing/Preprocessing
    DEPENDPATH += $$PWD/../../Preprocessing/Preprocessing
}
