#include "extraction.h"


cv::Mat Extraction::getCv_imgInOrig() const
{
    return cv_imgInOrig;
}

void Extraction::setCv_imgInOrig(const cv::Mat value)
{
    cv_imgInOrig = value;
}

cv::Mat Extraction::getCv_imgOutRect() const
{
    return cv_imgOutRect;
}

void Extraction::setCv_imgOutRect(const cv::Mat &value)
{
    cv_imgOutRect = value;
}

cv::Mat Extraction::getCv_imgOutSkelRect() const
{
    return cv_imgOutSkelRect;
}

void Extraction::setCv_imgOutSkelRect(const cv::Mat &value)
{
    cv_imgOutSkelRect = value;
}

Extraction::Extraction()
{
}

std::vector<std::pair<cv::Mat, std::string>> Extraction::run()
{
    this->cv_imgOutRect = this->cv_imgInOrig.clone();
    this->cv_imgOutSkelRect = this->cv_imgIn.clone();

    af::array af_oMap = OrientationMap::run(Converter::cvMat_to_afArray(this->cv_imgInOrig), 1, 17);
    float **cpp_oMap = Converter::afArray_to_cpp2Darray(af_oMap);

    std::vector<std::pair<cv::Mat, std::string>> vector;

    cv::Rect roi_cn(this->kSize/2 - 1 , this->kSize/2 - 1, 3, 3);
    cv::Rect roi_block(0, 0, this->kSize, this->kSize);


    std::vector <int> minX, maxX, minY, maxY;

    for (int i = 0; i < this->cv_imgIn.rows; i++) {
        minX.push_back(-1);
        maxX.push_back(-1);
    }

    for (int i = 0; i < this->cv_imgIn.cols; i++) {
        minY.push_back(-1);
        maxY.push_back(-1);
    }

//    std::cerr << minX.size() << " " << maxX.size() << " " << minY.size() << " " << maxY.size() << std::endl;

    for (int i = 0; i < this->cv_imgIn.rows; i++) {
        for (int j = 0; j < this->cv_imgIn.cols; j++) {
            if (int(this->cv_imgIn.at<uchar>(i, j)) == 0) {
                /* minX */
                if ( minX[i] == -1 )
                    minX[i] = j;

                /* maxX */
                if ( maxX[i] < j )
                    maxX[i] = j;

                /* minY */
                if ( minY[j] == -1 )
                    minY[j] = i;

                /* maxY */
                if ( maxY[j] < i )
                    maxY[j] = i;
            }
        }
    }

//    for (int i = 0; i < maxX.size(); i++)
//        std::cerr << maxX[i] << std::endl;

    for (int i = 0; i < this->cv_imgIn.rows - this->kSize; i++) {
        roi_cn.y = i + this->kSize/2 - 1;
        roi_block.y = i;

        for (int j = 0; j < this->cv_imgIn.cols - this->kSize; j++) {
            roi_cn.x = j + this->kSize/2 - 1;
            roi_block.x = j;

            if (minX[i + this->kSize/2] < j && maxX[i + this->kSize/2] - this->kSize > j && minY[j + this->kSize/2] < i && maxY[j + this->kSize/2] - this->kSize> i ) {
                if (int(this->cv_imgIn.at<uchar>(i + this->kSize/2, j + this->kSize/2)) == 0) {
                    if (this->CN(this->cv_imgIn(roi_cn)) != 2.0f) {
                        //                    std::cerr << ": " << this->CN(this->cv_imgIn(roi_cn));
//                        cv::rectangle(this->cv_imgOutRect, roi_block, cv::Scalar(0, 0, 0));
//                        cv::rectangle(this->cv_imgOutSkelRect, roi_block, cv::Scalar(0, 0, 0));
                        vector.push_back(std::pair<cv::Mat, std::string>(this->cv_imgInOrig(roi_block), std::to_string(i + this->kSize/2) + ";" + std::to_string(j + this->kSize/2) + ";" + std::to_string(cpp_oMap[i + this->kSize/2][j + this->kSize/2])));
                    }
                }
            }
        }
    }

    return vector;
}

double Extraction::CN(cv::Mat cv_block)
{
    double cn = 0;

    double p1 = int(cv_block.at<uchar>(0,0))/255;
    double p2 = int(cv_block.at<uchar>(1,0))/255;
    double p3 = int(cv_block.at<uchar>(2,0))/255;
    double p4 = int(cv_block.at<uchar>(2,1))/255;
    double p5 = int(cv_block.at<uchar>(2,2))/255;
    double p6 = int(cv_block.at<uchar>(1,2))/255;
    double p7 = int(cv_block.at<uchar>(0,2))/255;
    double p8 = int(cv_block.at<uchar>(0,1))/255;

    cn += abs(p1-p2);
    cn += abs(p2-p3);
    cn += abs(p3-p4);
    cn += abs(p4-p5);
    cn += abs(p5-p6);
    cn += abs(p6-p7);
    cn += abs(p7-p8);
    cn += abs(p8-p1);

    cn *= 0.5;

    return cn;
}

int Extraction::getKSize() const
{
    return kSize;
}

void Extraction::setKSize(int value)
{
    kSize = value;
}

cv::Mat Extraction::getCv_imgIn() const
{
    return cv_imgIn;
}

void Extraction::setCv_imgIn(const cv::Mat value)
{
    value.convertTo(value, CV_8UC1);
    cv_imgIn = value;
}

cv::Mat Extraction::getCv_imgOut() const
{
    return cv_imgOut;
}

void Extraction::setCv_imgOut(const cv::Mat value)
{
    cv_imgOut = value;
}

