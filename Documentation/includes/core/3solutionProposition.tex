\section{Návrh riešenia} \label{prop}

\subsection {Spracovanie odtlačku} \label{sprOd}

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{includes/images/blokovaSchemaOdtlacok.png}
	\caption{Bloková schéma spracovania odtlačku spracovania odtlačku}
	\label{blockOdt}
\end{figure}

Na obrázku \ref{blockOdt} je zobrazená bloková schéma spracovania odlačku. Spracovanie prebieha v troch krokoch, ktoré sú farebne odlíšené a sú to:
\begin{enumerate}
	\item Predspracovanie (zelená)
	\item Extrakcia (červená)
	\item Identifikácia (modrá)
\end{enumerate}
Výsledný výstup je následne ukladaný v databáze. V časti predspracovania sa taktiež nachádza senzor, ktorý sme využívali na zaznamenávanie odtlačkov.

\subsubsection {Predspracovanie}

Kvalita obrazu má výrazný vplyv na efektivitu systému pre rozpoznávanie odtlačkov prstov. Hlavným cieľom tohto procesu je túto efektivitu zvýšiť, pripraviť obraz pre ďalšie fázy spracovania. Prítomnosť nedokonalostí, šumu v obraze môže znekvalitniť extrakciu markantov z odtlačku, čo môže spôsobiť negatívne efekty: markanty nemusia byť extrahované, môžu sa objaviť falošné markanty. Aby sme týmto chybám predišli, práve na tento účel slúži proces predspracovania, ktorého úlohou je zvýšiť konzistentnosť papilárneho terénu v obrazoch nižšej kvality. \\
\indent Vzhľadom nato, že poznáme typické prípady chýb, vyskytujúcich sa v zoskenovaných odtlačkoch, náš proces by mal pozostávať z 3 kľúčových čŕt:
\begin{itemize}
	\item znovu spojiť oddelené papilárne línie, ktoré mohli vzniknúť pri suchej koži pri odoberaní odtlačkov.
	\item oddeliť nesprávne reprezentované papilárne línie, ktoré mohli vzniknúť naopak pri príliš vlhkej koži pri odoberaní odtlačkov.
	\item zachovať pôvodné zakončenia papilárnych línií a miesta ich rozdvojenia.
\end{itemize}
Najväčšou výzvou tohto procesu sú odtlačky nízkej kvality. V prípadne nesprávnych výpočtov, nesprávne zvolených algoritmov sa nám môže stať, že sa nám vo výslednom obraze objavia artefakty, čo následne znemožní správne fungovanie daktyloskopického systému na identifikáciu markantov a verifikácie osôb. \\
\indent V tejto sekcii sa budeme venovať opisu použitých metód a algoritmov v tejto práci. Proces predspracovania odtlačkov prstov sme rozdelili do nasledovných krokov: 
\begin{enumerate}
	\item Segmentácia
	\item Normalizácia
	\item Odhad výpočtu smerovej mapy
	\item Výpočet frekvenčnej mapy
	\item Gaborov filter, zakrivený Gaborov filter
	\item Binarizácia
\end{enumerate}

\paragraph{Segmentácia}\mbox{}\\


Prvý krok predspracovacieho procesu je segmentácia obrazu. Segmentácia je proces oddelenia obrazovej časti obsahujúcej papilárny terén odtlačku od pozadia, ktoré neobsahuje žiadne použiteľné informácie pre ďalšiu prácu s obrazom. Pozadie môže obsahovať rušivé elementy, ktoré môžu znekvalitniť ďalšie fázy predspracovacieho procesu. Cieľom segmentácie je odstránenie týchto elementov ~\cite{thaifingerprint} ~\cite{709565}. \\
\indent V obraze obsahujúcom odtlačok, pozadie vykazuje výrazne vyššiu varianciu (rozptyl) hodnôt šedej farby oproti časti obsahujúcej papilárne línie, medzery medzi nimi. Využívajúc tento fakt, používame nasledujúci postup: 
\begin{enumerate}
  \item Obraz rozdelíme na štvorcové bloky s veľkosťou \ensuremath{W} x \ensuremath{W}.
  \item Pre každý blok pomocou rovnice \ref{equation_variance} vypočítame hodnotu variancie šedej farby.
\begin{equation} \label{equation_variance}
V(k) = \frac{1}{W^2} \sum_{i=0}^{W-1} \sum_{j=0}^{W-1} (I(i,j)-M(k))^2
\end{equation}
\begin{itemize}
  \item[] \ensuremath{I(i,j)} - hodnota obrazového bodu v obraze
  \item[] \ensuremath{M(k)} - aritmetický priemer hodnôt v bloku
  \item[] \ensuremath{V(k)} - variancia bloku \ensuremath{k}
  \item[] \ensuremath{W} - rozmer bloku
\end{itemize}
  \item V prípade ak je variancia bloku \ensuremath{k} nižšia ako nami zvolený parameter, blok budeme ďalej považovať za pozadie.
\end{enumerate}

\paragraph{Normalizácia intenzity obrazového bodu}\mbox{}\\


Ďalšou fázou predspracovacieho procesu je normalizácia obrazu. Cieľom normalizácie je znížiť nekonzistentný rozsah hodnôt sivej farby v obraze medzi papilárnymi líniami a medzier medzi nimi pre efektívnejšie fungovanie nasledujúcich fáz v procese predspracovania. Pre tento účel používame dva typy metód.
V prvej metóde postupujeme nasledovne:

\begin{enumerate}
  \item Pre daný obraz vypočítame priemer a varianciu hodnôt šedej farby.
  \item Pre každý pixel pomocou rovnice \ref{equation_normalisation1} vypočítame normalizovanú hodnotu.
\begin{equation} \label{equation_normalisation1}
N(i,j)=\begin{cases}
 M_0 + \sqrt{\frac{V_0 (I(i,j)-M)^2}{V}} & \text{ak \ensuremath{I(i,j)>M}} \\    
 M_0 - \sqrt{\frac{V_0 (I(i,j)-M)^2}{V}} & \text{inak} 
\end{cases}
\end{equation}

\begin{itemize}
  \item[] \ensuremath{I(i,j)} - hodnota obrazového bodu v obraze
  \item[] \ensuremath{M} - aritmetický priemer hodnôt obrazu 
  \item[] \ensuremath{M_0} \ \ - požadovaný priemer hodnôt obrazu 
  \item[] \ensuremath{V} - variancia hodnôt obrazu
  \item[] \ensuremath{V_0} \ \ - požadovaná variancia hodnôt obrazu
\end{itemize}
\end{enumerate}

Nevýhodu tejto metódy je fixnosť parametrov požadovaného priemeru a variancie, ktoré musia byť nastavené podľa charakteristík daného obrázka ~\cite{709565}.
To znemožňuje korektné použitie normalizácie pre hromadné spracovanie databázy odtlačkov, ktoré nemajú podobnú charakteristiku. Taktiež táto metóda nie je vhodná pre použitie v situácii, ak sú odtlačky v nižšej kvalite alebo majú neuniformné hodnoty sivej farby vo väčších častiach obrazu. Na vyriešenie tohto
problému sme použili flexibilnejší algoritmus adaptívnej normalizácie, ktorý je vylepšením prvej metódy t.j. namiesto dvoch fixných parametrov priemeru a variancie dosadíme vypočítané hodnoty unikátne pre každý blok obrazu. Postupujeme nasledovne.

\begin{enumerate}
  \item Ekvalizácia histogramu. Táto metóda upravuje (zvyšuje) kontrast rastrového obrazu s využitím jeho histogramu. Jednotlivé hodnoty jasu sú v obraze
následne lepšie distribuované, čím zvyšujeme šancu rozpoznania charakteristických znakov v obraze.
  \item Pre daný obraz vypočítame priemer a varianciu hodnôt šedej farby.
  \item Pre každý blok vypočítame jeho priemernú hodnotu a varianciu.
  \item Pre každý blok vypočítame pomocou rovnice \ref{equation_normalisation21} špecifický priemer hodnôt a rovnice \ref{equation_normalisation22} špecifickú varianciu.

\begin{equation} \label{equation_normalisation21}
M_0 = M - \alpha_1 (M_i-M)
\end{equation}

\begin{equation} \label{equation_normalisation22}
V_0 = V - \alpha_2 (V_i-V)
\end{equation}

\begin{itemize}
  \item[] \ensuremath{M} - aritmetický priemer hodnôt obrazu 
  \item[] \ensuremath{M_0} \ \ - výsledný priemer hodnôt obrazu unikátny pre každý blok
  \item[] \ensuremath{M_i} \ \ - priemerná hodnota pre každý blok
  \item[] \ensuremath{V} - variancia hodnôt obrazu
  \item[] \ensuremath{V_0} \ \ - výsledná variancia obrazu unikátna pre každý blok
  \item[] \ensuremath{V_i} \ \ - variancia hodnôt pre každý blok
  \item[] \ensuremath{\alpha_1, \alpha_2} \ \ - váhy prispenia daného parametra
\end{itemize}

\end{enumerate}

Použitím tejto metódy dosiahneme presnejšie výsledky aj pre obrazy v nižšej kvalite, pretože sa zohľadňujú charakteristické vlastnosti každého bloku. Táto metóda dosahuje najlepšie výsledky ak je vstupný obraz už prvotne vysegmentovaný, aby sme nedostali skreslené hodnoty pre celkovú priemernú hodnotu obrazu a varianciu obrazu. Poprípade segmentáciu môžeme použiť ako súčasť tejto metódy ~\cite{iet:/content/journals/10.1049/el_20020507}.

\paragraph{Výpočet smerovej mapy}\mbox{}\\


\begin{figure}[!htbp]
\centering
\includegraphics{img/Peter/orientationMap}
\caption{Smer papilárnej línie v odtlačku.}
\end{figure}
Smerová mapa obrazu určuje smer papilárnych línií, ktoré sú obsiahnuté v odtlačku. Tento krok je veľmi dôležitý, pretože kvalita Gaborovho filtra je závislá od
správne vypočítaných hodnôt v smerovej mape. Pre jej výpočet sme najskôr používali blokovú metódu založenú na výpočtoch gradientov:

\begin{enumerate}
  \item Pre každý obrazový bod vypočítame gradienty $\partial_x(i,j)$ a $\partial_y(i,j)$. Pre výpočet $\partial_x(i,j)$ použijeme horizontálny Sobelov operátor:
\begin{equation}
\begin{pmatrix}
    {1} & {0} & {-1} \\
    {2} & {0} & {-2} \\
    {1} & {0} & {-1} \\
    \end{pmatrix}
\end{equation}
Pre výpočet $\partial_y(i,j)$ použijeme vertikálny Sobelov operátor:
\begin{equation}
\begin{pmatrix}
    {1} & {2} & {1} \\
    {0} & {0} & {0} \\
    {-1} & {-2} & {-1} \\
\end{pmatrix}
\end{equation}
\item Obraz rozdelíme na štvorcové bloky s veľkosťou \ensuremath{W} x \ensuremath{W}.
\item Lokálny smer v každom bloku odhadneme pomocou nasledujúcich rovníc \ref{equation_vx}, \ref{equation_vy}, \ref{equation_theta}:
\begin{equation} \label{equation_vx}
V_x(k) = \sum_{i=0}^{W-1} \sum_{j=0}^{W-1} 2\partial_x(i,j)\partial_y(i,j)
\end{equation}
\begin{equation} \label{equation_vy}
V_y(k) = \sum_{i=0}^{W-1} \sum_{j=0}^{W-1} \partial^2_x(i,j)-\partial^2_y(i,j)
\end{equation}
\begin{equation} \label{equation_theta}
\theta(k) = \frac{1}{2}tan^{-1}\frac{V_x(k)}{V_y(k)}
\end{equation}
\begin{itemize}
  \item[] $\theta$(k) - odhadovaný smer pre daný blok
  \item[] \ensuremath{W} - rozmer bloku
\end{itemize}
    \item Vyhladenie smerovej mapy použitím Gaussovho filtra. Smerová mapa z predchádzajúceho kroku je najskôr prevedená do kontinuálneho vektorového poľa definovaným vzťahmi \ref{equation_phix}, \ref{equation_phiy}.

\begin{equation} \label{equation_phix}
\phi_x(i,j) = cos(2\theta (i,j))
\end{equation}

\begin{equation} \label{equation_phiy}
\phi_y(i,j) = sin(2\theta (i,j))
\end{equation}

\begin{itemize}
  \item[] \texorpdfstring{$\phi$\textsubscript{x}} \ \ - x-ový komponent vektorového poľa
  \item[] \texorpdfstring{$\phi$\textsubscript{y}} \ \ - y-ový komponent vektorového poľa
\end{itemize}

Po vypočítaní vektorového poľa aplikujeme Gaussov filter pomocou vzťahov \ref{equation_phix2},  \ref{equation_phiy2}

\begin{equation} \label{equation_phix2}
\phi^{'}_x(i,j) = \sum_{u=-\frac{w_\phi}{2}}^{\frac{w_\phi}{2}} \sum_{v=-\frac{w_\phi}{2}}^{\frac{w_\phi}{2}} G(u,v)\phi_x(i-uw,j-uw)
\end{equation}

\begin{equation} \label{equation_phiy2}
\phi^{'}_y(i,j) = \sum_{u=-\frac{w_\phi}{2}}^{\frac{w_\phi}{2}} \sum_{v=-\frac{w_\phi}{2}}^{\frac{w_\phi}{2}} G(u,v)\phi_y(i-uw,j-uw)
\end{equation}

\begin{itemize}
  \item[] \ensuremath{G} - veľkosť \ensuremath{w}\textsubscript{$\phi$} x \ensuremath{w}\textsubscript{$\phi$} filtrovacieho kernela Gaussovho filtra
\end{itemize}

Výsledná vyhladená smerová mapa je definovaná vzťahom \ref{equation_o}.

\begin{equation} \label{equation_o}
O(i,j) = \frac{1}{2}tan^{-1}\frac{\phi^{'}_y(i,j)}{\phi^{'}_x(i,j)}
\end{equation}

\end{enumerate}

Vyššie uvedená metóda vykazovala uspokojivé výsledky, no obsahuje pár nedokonalostí, keďže predpokladá to, že vypočítanú hodnotu pre každý blok majú aj
všetky obrazové body obsiahnuté v danom bloku. Na ešte presnejší výpočet smerovej mapy sme použili rozšírený spôsob algoritmu založenom na výpočte gradientov, ktorý pre každý obrazový zohľadňuje jeho okolie. Treba podotknúť, že táto metóda je omnoho náročnejšia na výpočtový výkon ~\cite{thaifingerprint}.
V tomto prípade sú uvedené len body, ktorými sa táto metóda líši od vyššie spomenutej:
\begin{enumerate}
\setcounter{enumi}{1}
\item Obrazový bod \ensuremath{(i,j)} je stredom bloku s veľkosťou \ensuremath{W} x \ensuremath{W}. Túto operáciu opakujeme pre každý obrazový bod v obraze.
\item  Lokálny smer v každom pixeli odhadneme pomocou nasledujúcich rovníc \ref{equation_vx2}, \ref{equation_vy2}, \ref{equation_theta2}:
\begin{equation} \label{equation_vx2}
V_x(i,j) = \sum_{u=i-\frac{W}{2}}^{i+\frac{W}{2}} \sum_{v=i-\frac{W}{2}}^{i+\frac{W}{2}} 2\partial_x(u,v)\partial_y(u,v)
\end{equation}
\begin{equation} \label{equation_vy2}
V_y(i,j) =  \sum_{u=i-\frac{W}{2}}^{i+\frac{W}{2}} \sum_{v=i-\frac{W}{2}}^{i+\frac{W}{2}} \partial^2_x(u,v)-\partial^2_y(u,v)
\end{equation}
\begin{equation} \label{equation_theta2}
\theta(i,j) = \frac{1}{2}tan^{-1}\frac{V_x(i,j)}{V_y(i,j)}
\end{equation}
\begin{itemize}
  \item[] $\theta$(i,j) - odhadovaný smer pre daný pixel
  \item[] \ensuremath{W} - rozmer bloku
\end{itemize}
\end {enumerate}

\paragraph{Gaborov filter}\mbox{}\\


Gaborova funkcia, vo forme Gaborovho filtra, má širokú škálu využitia v grafickom spracovaní obrazu a rozpoznávaní vzorov útvarov. Nižšie sú uvedené tie najdôležitejšie formy použitia:
\begin{itemize}
	\item Segmentácia textúr a ich klasifikácia, napr. rozoznávanie druhov v tropických dažďových pralesoch.
	\item Aplikácia v medicíne, biológii, napr. zvýraznenie žíl, svalových vláken v ultrazvukových obrazoch, ciev v očnej sietnici, skúmanie mozgovej aktivity.
	\item Rozpoznávanie písma, znakov, použitých fontov.
	\item Rozpoznávanie objektov, napr. áut.
	\item Biometria, napr. verifikácia pomocou dúhovky, tvári, výrazov tvári.
	\item Odtlačky prstov. Dodatočne mimo obsahu tejto práce sa využívajú na tvorbu synetických odtlačkov.  
\end{itemize}
Po implementovaní troch predchádzajúcich fáz procesu predspracovania je obraz pripravený na filtrovanie. Dvojrozmerný Gaborov filter je reálna časť Gaborovej funkcie viď \ref{equation_gaborReal}, ktorá sa skladá z goniometrickej funkcie sínus určitého smeru a určitej frekvencie, ktorá je modulovaná Gaussovou krivkou. Gaborov filter sa v daktyloskopii používa kvôli svojim dvom špecifickým parametrom a to sú nastaviteľná frekvencia a nastaviteľný smer. Tie umožňujú filtru byť responzívnym k papilárnym líniám odtlačku, pričom sú zachované ich tvary a v obraze redukuje šum a nečistoty ~\cite{thaifingerprint} ~\cite{Jain:2014:IB:2616414} ~\cite{Wang2008301}.
\begin{equation} \label{equation_gaborReal}
G(x,y;\lambda,\theta,\psi,\sigma)=exp(-\frac{x^{'2}+\gamma^2y^{'2}}{2\sigma^2})cos(2\pi\frac{x'}{\lambda}+\psi)
\end{equation}
\begin{equation} \label{equation_xTheta}
x_\theta = x cos \theta + y sin \theta
\end{equation}
\begin{equation} \label{equation_yTheta}
y_\theta = -x sin \theta + y cos \theta
\end{equation}
\begin{itemize}
  \item[] $\gamma$ - pomer strán
  \item[] $\lambda$ - vlnová dĺžka
  \item[] $\psi$ - fázový posun
  \item[] $\sigma$ - smerodajná odchýlka Gaussovej krivky
  \item[] $\theta$ - smer
\end{itemize}
\begin{figure}[!htbp]
\centering
\includegraphics[scale=0.45]{img/Peter/gaborFilter.png}
\caption{Ukážka symetrického Gaborovho filtra v priestore.}
\end{figure}
Gaborov filter je aplikovaný na obraz konvolúciou samotného obrazu s filtrovacou maskou (kernelom). Tento filter je špecifický tým, že pre každý pixel je vytvorená vlastná filtrovacia maska. Na jej vytvorenie potrebujeme korešpondujúci smer k obrazovému bodu \ensuremath{(i,j)} zo smerovej mapy \ensuremath{O(i,j)}. Prefiltrovaný obraz získame nasledovne.
\begin{equation} \label{equation_gaborFilter}
E(i,j) = \sum_{u=-\frac{w_x}{2}}^{\frac{w_x}{2}} \sum_{v=-\frac{w_y}{2}}^{\frac{w_y}{2}} G(u,v;O(i,j))N(i-u,j-v)
\end{equation}
\begin{itemize}
  \item[] \ensuremath{E} - výsledná hodnota pixelu po konvolúcii
  \item[] \ensuremath{G} - filtrovacia maska
  \item[] \ensuremath{N} - normalizovaný obraz
  \item[] \ensuremath{O(i,j)} - smer prislúchajúci pixelu zo smerovej mapy
  \item[] \ensuremath{w}\textsubscript{x},\ensuremath{w}\textsubscript{y} - výška a šírka filtrovacej masky
\end{itemize}

\begin{figure}[!htbp]
\centering
\includegraphics[scale=0.75]{img/Peter/convolution.png}
\caption{Ukážka princípu konvolúcie.}
\end{figure}

\paragraph{Binarizácia}\mbox{}\\


Väčšina algoritmov na extrakciu kľúčových prvkov odtlačku pracuje s binárnymi obrazmi kde sa nachádzajú len dva body záujmu: čierne obrazové body reprezentujú papilárne línie a biele obrazové body reprezentujú medzery medzi papilárnymi líniami. Binarizácia je proces ktorý prevádza bezfarebný obraz do binárneho obrazu. Táto operácia zlepšuje kontrast medzi papilárnymi líniami a medzier medzi nimi, čo pri ďalšej fáze extrakcii kľúčových prvkov zvyšuje efektivitu. \\
\indent Po prefiltrovaní odtlačku Gaborovým filtrom je binarizácia prevedená s globálnym parametrom 0. Proces binarizácie skúma hodnotu jednotlivých pixelov v prefiltrovanom obraze. Ak je hodnota väčšia ako globálny parameter, tak sa nastaví hodnota obrazového bodu na 1, inak na 0 ~\cite{thaifingerprint}.

\paragraph{Skeletonizácia}\mbox{}\\
\indent  Po binarizácii využijeme Guo-Hallov stenčovací algoritmus \cite{Guo-Hall}. Vytváranie kostry odtlačku je proces, pri ktorom postupne zužujeme papilárne línie na veľkost jedného pixelu. Potom budeme prechádzať obrázok po papilárnych líniách a pomocou funkcie Crossing Number zisťovať miesta, na ktorých by sa mohli vyskytovať markanty.
\subsubsection {Extrakcia}
\indent Extrakcia markantov bude prebiehať v dvoch krokoch. Prvým krokom je prechádzanie kostry odtlačku funkciou Crossing Number. Vstupom do funkcie je blok obrázku veľkosti 3x3, kde stredový pixel je pixel kostry odtlačku. Funkcia vypočíta hodnotu crossing number a podľa tejto hodnoty môžeme určiť miesta, na ktorých sa môže nachádzať markant. V prípade, že výsledná hodnota je rovná jednotke, môžeme považovať daný blok ako miesto kde sa nachádza ukončenie, ak je hodnota rovná trojke, považujeme daný blok za možné rozdvojenie. Následne vystrihneme obrázok s rovnakým stredom vo veľkosti 17x17 z originálneho obrázka. \\
\indent Druhým krokom je klasifikácia vystrihnutého obrázku pomocou neurónovej siete. Neurónovú sieť sme natrénovali na 19639 trénovacích dátach, ktoré sme rozdelili do troch tried a to ukončenia(9209), rozdvojenia(5033) a ignor(5397). Pôvodným plánom bolo využiť knižnicu Caffe 2.0, s ktorou však nastali komplikácie či už pri buildovaní na Windowse a aj Linuxe tovnako ako aj pri práci s ňou. Preto sme a vytvorenie siete sme využili knižnicu TensorFlow. Vyskúšali sme viacero architektúr hlbokých neurónových sietí ako napríklad Inception a ResNet. Výstupom neurónovej siete bude zaklasifikovaný vstupný obrázok. V prípade, že sa jedná o markant, aplikácia si zapamätá polohu markantu, rovnako ako aj smer na danej polohe vďaka smerovej mape.
\subsubsection {Identifikácia}
\indent Identifikovať osobu budeme pomocou NIST programu Bozorth3. Vstupom do Bozorth3 sú dva .xyt súbory, z ktorých nám vypočíta ich skóre. Vo všeobecnosti platí, že čím vyššie skóre tým väčšia pravdepodobnosť, že sa jedná o ten istý odtlačok. Určiť však hranicu, podľa ktorej vieme určiť či ide o rozdielny alebo rovnaký odtlačok, je možné len po rozsiahlom testovaní.
\subsection {Klient/server aplikácia}

\subsubsection{Diagram činností}

Podľa špecifikácie sme identifikovali procesy, ktoré sa budú vykonávať za pomoci našej aplikácie. Ide hlavne o procesy na identifikáciu osôb za pomoci odtlačkou:
\begin{enumerate}
	\item Prihlihlásenie používateľa.
	\item Kontrola/vyhľadanie otlačku v databáze.
\end{enumerate}
Pri ďalšom rozbore zadania sme dospeli k ďalším procesom, ktoré sa majú v systéme vykonávať:
\begin{enumerate}
	\item Registrácia nového používateľa.
	\item Zabezpečenie prístupu k databáze.
	\item Ukladanie nového odtlačku do databázy z klienta.
\end{enumerate}
Z tejto skupiny procesov boli vybraté tri zložitejšie procesy, na ktoré sme následne vytvorili diagramy činností.

\paragraph{Prihlásenie používateľa}\mbox{}\\

Pri prihlasovaní používateľa predpokladáme, že klientská aplikácia je pripojená na bežiaci server. 

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{includes/images/logInUser_activity.png}
	\caption{Diagram čínností pre prihlásenie používateľa}
	\label{loginAct}
\end{figure}

Ako môžme pozorovať na obrázku \ref{loginAct} ide o komunikáciu medzi klientom a serverom. Klient má možnosť voľby prihlásenia pomocou prihlasovacích údajov alebo pomocou odtlačku prsta. Prihlasovanie pomocou prihlasovacích údajov musí byť sprístupnené z dôvodu, že ak uživateľ je nový používateľ, v systéme nemá zatiaľ uloženeé svoje odtlačky prstov. Následne ak používateľ nemá možnosť zosnímať odtlačok prsta pomocou snímača je mu umožnené si svoj odtlačok uchovať na bezpečnom mieste v digitálnej podobe a následne ho neskôr používať. Ak sa používateľ prihlasuje pomocou prihlasovacích údajov server vytvorí dopyt na základe týchto údajov. To ale neplatí v prípade prihlasovania sa pomocou odtlačku. Samotný odtlačok musí prejsť spracovaním, ako to už bolo spomenuté vyššie(podkapitola \ref{sprOd}) a až potom sa vytvára dopyt na datábazu. Po úspešnom prihlásení si server uchováva id prihlaseného používateľa a používateľovi sa zobrazí správa či už ide o úspešné alebo neúspešne prihlásenie. 

\paragraph{Registrácia nového používateľa}\mbox{}\\

V predchádzajúcom diagrame sme uvažovali o prihlásení používateľa do systému, no na to je potrebné tohto používateľa aj registrovať.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{includes/images/registration_activity.png}
	\caption{Diagram čínností pre registrácu nového používateľa}
	\label{registrationAct}
\end{figure}
Na obrázku \ref{registrationAct} je zobrazený diagram vytvorenia používateľa. Na začiatku celého procesu používateľ zadáva svoje údaje, ktoré budú musieť spĺňať určité podmienky ohľadom špeciálnych znakov a pod. Údaje sú odoslané na server kde sa spracujú a snažia sa uložiť do databázy. Ak používateľské meno je už obsadené, používateľ je vyzvaný k tomu, aby zadal nové meno.

\paragraph{Ukladanie nového odtlačku do databázy z klienta}\mbox{}\\

Na vyhľadávanie odtlačkou je ale potrebné odtlačky aj ukladaj. Pred vykonávaním procesy sa predpokladá, že používateľ je prihlásený na server a server si uchováva jeho \textit{id}.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{includes/images/scan_activity.png}
	\caption{Diagram čínností pre pridenie nového odtlačku do databázy}
	\label{newFinAct}
\end{figure}
Po príhlásení používateľ može svoje odtlačky pridávať do databázy z klientskej aplikácie a to pomocou naskenovania nového odtlačku, alebo ak nemá prístup k snímaču može použiť obrázok so svojim odtlačkom. Na servery sa odtlačok spracuje ako to bolo popísané v časti \ref{sprOd} a uloží sa do databázy. Ide o jednoduchý proces, ktorý je ale umožňuje používateľom pridávať svoje odtlačky do databázy. 

\paragraph{Zabezpečenie prístupu k databáze}\mbox{}\\

Databáza odtlačkou nemôže byť prístupná všetkým používateľom, ktorý vlastnia klientsku aplikáciu, ale iba schváleným. Po registrovaní sa novým používateľom nie je mu umožnené sa prihlásiť do databázy. Administrátor pomocou aplikácie na strane servera, má ale možnosť mu tento prístup udeliť. Ide o jednoduchý postup, ktorý upravý záznam o používateľovi v databáze a nastavia sa mu nové privilégia. Podobným sposôbom administrátor dokáže tieto prístupy k databáze aj odoberať.\newpage
\paragraph{Kontrola/vyhľadanie otlačku v databáze}\mbox{}\\

Ide o jednoduchý proces, pri ktorom klient odosiela odlačok prsta na server a ten mu následne povie či tento odtlačok sa v databáze nachadza alebo mu povie ku komu patrí. Diagram činností je podobný ako v prípade ukladania nového odtlačku \ref{sprOd} ale namiesto ukladania odlačku prebieha jeho vyhľadávanie v databáze.

\subsubsection{Model prípadov použitia}

Celý model prípadov použitia je rozdelený na dve časti. V prvej časti sa zaoberáme klientskou aplikáciou a  v druhej serverovou aplikáciou.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{includes/images/client_useCase.png}
	\caption{Model prípadov použitia pre prácu s klientskou aplikáciou}
	\label{clientUseCasae}
\end{figure}
Na obrázku \ref{clientUseCasae} je zobrazený model prípadov použitia pre klientsku aplikáciu. S klientskou aplikáciu, budú používať iba používatelia, ktorým bude pomocou nej umožnené iba sa prihlasiť, ukladať nové odtlačky a vyhľadávať existujúce  odtlačky v databáze. Samozrejme je preto potrebné aby táto aplikácia nadviazala spojenie so serverom. Používateľ bude toto spojenie vytvárať manuálne. V aplikácii bude implmentovaná komunikácia so snímačom odtlačkou.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{includes/images/server_useCase.png}
	\caption{Model prípadov použitia pre prácu so serverovou aplikáciou}
	\label{serverUseCasae}
\end{figure}
Aplikáciu na servery bude mocť obsluhovať iba administrátor a to priamym prístupom na server. Ako môžme pozorovať (obrázok \ref{serverUseCasae}) aplikácia umožnuje správu servera a používateľov. Tým udeľuje a odoberá prístupy s prstupu na server a dokáže taktiež manuálne odpájať pripojené klientske aplikácie, alebo im posielať správy.

\subsubsection{Model údajov}\label{modeUdajov}

V tejto časti sa budeme venovať dátovému modelu údaju odtlačkou a ich reprezentácií v databáze. Zo špecifikácie a vytvorených procesov pozorujeme, že uložené odtlačky prstov v databáze je potrebné priraďovať k osobám, čím umožníme prihlasovanie sa do aplikácie. Tie ale musia byť zašifrované kvôli bezpečnosti. Podobným sposobom sa šifrujú a prihlasovacie heslá. Aby sa používateľ dokázal s určitosťou prihhlásiť do aplikácie je potrebné, aby mal uložených viacero otlačkou.

\begin{figure}[H]
	\centering
	\includegraphics[width=.8\linewidth]{includes/images/database.png}
	\caption{Model údajov}
	\label{dataModel}
\end{figure}
Model údajov (obrázok \ref{dataModel}) obsahuje iba dve tabuľky a to \textit{person} a \textit{fingerprint}. Tabuľka \textit{person} uchovováva v sebe údaje o používateľovi, jeho prihlasovacie meno a heslo, ale je taktiež rozšíritelná o ďalšie údaje. Tabuľka \textit{fingerprint} slúži na ukladanie odtlačkou a priradzovanie ich k používateľom, čo je ošetrené pomocou cudzieho kľuča na tabuľku \textit{person}. Týmto spôsobom je taktiež ošetrené ukladanie viacero odtlačkou pre jednu osobu. Odtlačky sú reprezentované pomocou výstupu zo spracovania odtlačkou \ref{sprOd} a to vo formáte \textit{xyt}.
\subsubsection{Zabezpečenie spojenia}

Keďže aplikácia bude pracovať s citlivými dátami, je dôležité aby bola zabezpečená. Z tohoto dôvodu je sieťové pripojenie medzi klientskou a serverovou časťou aplikácie zabezpečené pomocou TSL protokolu. Konkrétne sa jedná o verziu TLS1.2 \cite{TLS} z roku 2008 alebo vyššie. Pri pokuse o pripojenie zo strany klienta nastáva takzvané potrasenie rukou, pri ktorom sa strany dohodnú na použitých algoritmoch, navzájom sa overia a pomocou Diffie hellman algoritmu si vymenia symetrické kľúče, ktorými bude komunikácia šifrovaná. Kľúče sú jedinečné pre jedno spojenie. Použité šifrovacie algoritmy sú RSA pre výmenu kľúčov a AES v CBC móde na symtrické šifrovanie. Ako hashovacia funkcia sa využíva SHA224 a vyššie.

\newpage
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{includes/images/blokovaSchemaKomunikacia.png}
	\caption{Bloková schéma klient/server komunikácie}
	\label{blockKom}
\end{figure}

Na obrázku \ref{blockKom} je zobrazená bloková komunikácie medzi klientom a serverom. Taktiež je tu zobrazená komunikácia medzi klientom a scannerom. Pre prácu so senzorom bola vytvorená dynamická knižnica \textit{Senzor DLL}

