\babel@toc {english}{}
\thispagestyle {empty}
\contentsline {section}{\'{U}vod}{1}{section*.2}
\contentsline {section}{\numberline {1}Ciele pr\IeC {\'a}ce a motiv\IeC {\'a}cia}{2}{section.3}
\contentsline {section}{\numberline {2}\IeC {\'U}vod do problematiky}{3}{section.4}
\contentsline {subsection}{\numberline {2.1}Biometria}{3}{subsection.5}
\contentsline {subsection}{\numberline {2.2}V\IeC {\'y}hody biometrie}{5}{subsection.6}
\contentsline {subsection}{\numberline {2.3}Vyu\IeC {\v z}itie biometrie v praxi}{5}{subsection.8}
\contentsline {subsection}{\numberline {2.4}Verifik\IeC {\'a}cia}{5}{subsection.9}
\contentsline {subsubsection}{\numberline {2.4.1}Verifik\IeC {\'a}cia s centralizovan\IeC {\'y}m ukladan\IeC {\'\i }m d\IeC {\'a}t }{5}{subsubsection.10}
\contentsline {subsubsection}{\numberline {2.4.2}Verifik\IeC {\'a}cia s decentralizovan\IeC {\'y}m ukladan\IeC {\'\i }m d\IeC {\'a}t}{6}{subsubsection.11}
\contentsline {subsection}{\numberline {2.5}Identifik\IeC {\'a}cia}{6}{subsection.12}
\contentsline {subsection}{\numberline {2.6}Daktyloskopia}{6}{subsection.13}
\contentsline {subsubsection}{\numberline {2.6.1}Individu\IeC {\'a}lnos\IeC {\v t}}{7}{subsubsection.14}
\contentsline {subsubsection}{\numberline {2.6.2}Nemennos\IeC {\v t}}{7}{subsubsection.15}
\contentsline {subsubsection}{\numberline {2.6.3}Neodstr\IeC {\'a}nite\IeC {\v l}nos\IeC {\v t}}{7}{subsubsection.16}
\contentsline {subsection}{\numberline {2.7}Daktyloskopia v kriminalistike}{7}{subsection.17}
\contentsline {subsection}{\numberline {2.8}Vyu\IeC {\v z}itie daktyloskopie}{8}{subsection.18}
\contentsline {subsection}{\numberline {2.9}Odtla\IeC {\v c}ky prstov}{8}{subsection.19}
\contentsline {subsection}{\numberline {2.10}Papil\IeC {\'a}rne l\IeC {\'\i }nie}{8}{subsection.20}
\contentsline {subsection}{\numberline {2.11}Delenie pr\IeC {\'\i }znakov odtla\IeC {\v c}ku}{9}{subsection.22}
\contentsline {subsubsection}{\numberline {2.11.1}Level-1 znaky}{9}{subsubsection.23}
\contentsline {subsubsection}{\numberline {2.11.2}Level-2 znaky}{11}{subsubsection.28}
\contentsline {subsubsection}{\numberline {2.11.3}Level-3 znaky}{12}{subsubsection.30}
\contentsline {subsection}{\numberline {2.12}Porovn\IeC {\'a}vac\IeC {\'\i } algoritmus}{13}{subsection.33}
\contentsline {subsection}{\numberline {2.13}Konvolu\IeC {\v c}n\IeC {\'e} neur\IeC {\'o}nov\IeC {\'e} siete}{13}{subsection.37}
\contentsline {subsubsection}{\numberline {2.13.1}Konvolu\IeC {\v c}n\IeC {\'a} vrstva}{14}{subsubsection.39}
\contentsline {subsubsection}{\numberline {2.13.2}Pooling vrstva}{15}{subsubsection.41}
\contentsline {subsubsection}{\numberline {2.13.3}ReLU vrstva}{15}{subsubsection.43}
\contentsline {subsection}{\numberline {2.14}U\IeC {\v c}enie siete}{16}{subsection.45}
\contentsline {section}{\numberline {3}N\IeC {\'a}vrh rie\IeC {\v s}enia}{17}{section.46}
\contentsline {subsection}{\numberline {3.1}Spracovanie odtla\IeC {\v c}ku}{17}{subsection.47}
\contentsline {subsubsection}{\numberline {3.1.1}Predspracovanie}{17}{subsubsection.48}
\contentsline {paragraph}{Segment\IeC {\'a}cia}{18}{Item.54}
\contentsline {paragraph}{Normaliz\IeC {\'a}cia intenzity obrazov\IeC {\'e}ho bodu}{18}{Item.58}
\contentsline {paragraph}{V\IeC {\'y}po\IeC {\v c}et smerovej mapy}{20}{equation.67}
\contentsline {paragraph}{Gaborov filter}{22}{equation.87}
\contentsline {paragraph}{Binariz\IeC {\'a}cia}{24}{figure.caption.93}
\contentsline {paragraph}{Skeletoniz\IeC {\'a}cia}{25}{figure.caption.93}
\contentsline {subsubsection}{\numberline {3.1.2}Extrakcia}{25}{subsubsection.94}
\contentsline {subsubsection}{\numberline {3.1.3}Identifik\IeC {\'a}cia}{25}{subsubsection.95}
\contentsline {subsection}{\numberline {3.2}Klient/server aplik\IeC {\'a}cia}{26}{subsection.96}
\contentsline {subsubsection}{\numberline {3.2.1}Diagram \IeC {\v c}innost\IeC {\'\i }}{26}{subsubsection.97}
\contentsline {paragraph}{Prihl\IeC {\'a}senie pou\IeC {\v z}\IeC {\'\i }vate\IeC {\v l}a}{26}{Item.102}
\contentsline {paragraph}{Registr\IeC {\'a}cia nov\IeC {\'e}ho pou\IeC {\v z}\IeC {\'\i }vate\IeC {\v l}a}{27}{figure.caption.103}
\contentsline {paragraph}{Ukladanie nov\IeC {\'e}ho odtla\IeC {\v c}ku do datab\IeC {\'a}zy z klienta}{28}{figure.caption.104}
\contentsline {paragraph}{Zabezpe\IeC {\v c}enie pr\IeC {\'\i }stupu k datab\IeC {\'a}ze}{29}{figure.caption.105}
\contentsline {paragraph}{Kontrola/vyh\IeC {\v l}adanie otla\IeC {\v c}ku v datab\IeC {\'a}ze}{30}{figure.caption.105}
\contentsline {subsubsection}{\numberline {3.2.2}Model pr\IeC {\'\i }padov pou\IeC {\v z}itia}{30}{subsubsection.106}
\contentsline {subsubsection}{\numberline {3.2.3}Model \IeC {\'u}dajov}{31}{subsubsection.109}
\contentsline {subsubsection}{\numberline {3.2.4}Zabezpe\IeC {\v c}enie spojenia}{32}{subsubsection.111}
\contentsline {section}{\numberline {4}Pou\IeC {\v z}it\IeC {\'e} v\IeC {\'y}vojov\IeC {\'e} prostriedky}{33}{section.112}
\contentsline {subsection}{\numberline {4.1}Softv\IeC {\'e}r}{33}{subsection.113}
\contentsline {subsubsection}{\numberline {4.1.1}C++}{33}{subsubsection.114}
\contentsline {subsubsection}{\numberline {4.1.2}CUDA}{33}{subsubsection.115}
\contentsline {subsubsection}{\numberline {4.1.3}MariaDB}{34}{subsubsection.117}
\contentsline {subsubsection}{\numberline {4.1.4}cuDNN}{35}{subsubsection.118}
\contentsline {subsubsection}{\numberline {4.1.5}Array Fire}{35}{subsubsection.119}
\contentsline {subsubsection}{\numberline {4.1.6}OpenCV}{35}{subsubsection.120}
\contentsline {subsubsection}{\numberline {4.1.7}Qt}{36}{subsubsection.121}
\contentsline {subsubsection}{\numberline {4.1.8}TensorFlow}{36}{subsubsection.123}
\contentsline {subsection}{\numberline {4.2}Hardv\IeC {\'e}r}{37}{subsection.124}
\contentsline {subsubsection}{\numberline {4.2.1}HiScan-PRO - Fingerprint Scanner}{37}{subsubsection.125}
\contentsline {subsubsection}{\numberline {4.2.2}V\IeC {\'y}po\IeC {\v c}tov\IeC {\'y} v\IeC {\'y}kon}{37}{subsubsection.126}
\contentsline {section}{\numberline {5}Klient/Server aplik\IeC {\'a}cia}{38}{section.127}
\contentsline {subsection}{\numberline {5.1}Klientsk\IeC {\'a} aplik\IeC {\'a}cia}{38}{subsection.128}
\contentsline {subsubsection}{\numberline {5.1.1}Spr\IeC {\'a}va pripojenia}{39}{subsubsection.130}
\contentsline {subsubsection}{\numberline {5.1.2}Pr\IeC {\'a}ca s odtla\IeC {\v c}kami prstov}{40}{subsubsection.133}
\contentsline {subsubsection}{\numberline {5.1.3}Ostatn\IeC {\'e} s\IeC {\'u}\IeC {\v c}asti}{41}{subsubsection.135}
\contentsline {subsection}{\numberline {5.2}Server}{42}{subsection.136}
\contentsline {subsubsection}{\numberline {5.2.1}Datab\IeC {\'a}za}{43}{subsubsection.138}
\contentsline {subsection}{\numberline {5.3}Ovl\IeC {\'a}dac\IeC {\'\i } panel (1)}{43}{subsection.139}
\contentsline {subsubsection}{\numberline {5.3.1}Sp\IeC {\'a}va servera (1.1)}{43}{subsubsection.140}
\contentsline {subsubsection}{\numberline {5.3.2}Mana\IeC {\v z}ment prihl\IeC {\'a}sen\IeC {\'y}ch klientov (1.2)}{43}{subsubsection.141}
\contentsline {subsubsection}{\numberline {5.3.3}Mana\IeC {\v z}ment registrovan\IeC {\'y}ch klientov (1.3)}{44}{subsubsection.142}
\contentsline {subsection}{\numberline {5.4}Zobrazovanie inform\IeC {\'a}cii (2)}{44}{subsection.143}
\contentsline {subsubsection}{\numberline {5.4.1}Zobrazenie z\IeC {\'\i }skan\IeC {\'e}ho obr\IeC {\'a}zku a kostry odtla\IeC {\v c}ku (2.1, 2.2)}{44}{subsubsection.144}
\contentsline {subsubsection}{\numberline {5.4.2}Logovac\IeC {\'\i } syst\IeC {\'e}m (2.3)}{44}{subsubsection.145}
\contentsline {subsubsection}{\numberline {5.4.3}Spr\IeC {\'a}vy od klienta a stavov\IeC {\'y} riadok (2.4, 2.5)}{45}{subsubsection.146}
\contentsline {section}{\numberline {6}Dosiahnut\IeC {\'e} v\IeC {\'y}sledky}{46}{section.147}
\contentsline {section}{Z\'{a}ver}{47}{section*.148}
\contentsline {section}{Zoznam pou\v {z}itej literat\'{u}ry}{48}{section*.149}
