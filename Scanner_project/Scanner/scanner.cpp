#include "scanner.h"

Scanner::Scanner()
{

}

QImage Scanner::scanImage()
{
    int err, imageSize;
    FxBuffer gImage;
    BYTE *pImage;
    QImage fingerprint;

    err = FxISO_Init();
    if (err){
        qDebug() << err;
    }

    err = FxISO_Mem_NewBuffer(&gImage);
    if (err){
        qDebug() << err;
    }
    float * quality = new float;
    err = FxISO_Fing_AcquireSupervised(NULL, 50, 50, quality);
    if (err){
        qDebug() << err;
    }

    err = FxISO_Fing_SaveToMemory(&gImage, NATIVE_RESOLUTION, NULL);
    if (err){
        qDebug() << err;
    }

    imageSize = gImage.imageWidth * gImage.imageHeight;
    pImage = new BYTE[imageSize];

    err = FxISO_Mem_CopyBufferToArray (&gImage, pImage, imageSize);
    if (err){
        qDebug() << err;
    }
    fingerprint = QImage ((uchar *)pImage, gImage.imageHeight, gImage.imageWidth, QImage::Format_Grayscale8);

    err = FxISO_Mem_DeleteBuffer(&gImage);
    if (err){
        qDebug() << err;
    }

    err = FxISO_End();
    if (err){
        qDebug() << err;
    }

    return fingerprint;
}
