#ifndef SCANNER_GLOBAL_H
#define SCANNER_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SCANNER_LIBRARY)
#  define SCANNERSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SCANNERSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // SCANNER_GLOBAL_H
