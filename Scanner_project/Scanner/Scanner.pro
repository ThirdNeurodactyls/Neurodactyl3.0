#-------------------------------------------------
#
# Project created by QtCreator 2018-03-19T21:25:15
#
#-------------------------------------------------

QT       += core gui

TARGET = Scanner
TEMPLATE = lib

DEFINES += SCANNER_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
SOURCES += \
        scanner.cpp

HEADERS += \
        scanner.h \
        scanner_global.h

win32{
    LIBS += $$_PRO_FILE_PWD_/scanner_dll/fxISOenrdlg_64.lib \
            $$_PRO_FILE_PWD_/scanner_dll/fxISO_64.lib
}
unix {
    QMAKE_CXXFLAGS += -DFXDLL_LINUX -O3 -idirafter .. -I$(INC)
#    LIBS += lib64/libfxISOL.a lib64/libfxISOscan.a lib64/libfxoem.a lib64/libftd2xx.a



}

#unix:!macx: LIBS += -L$$PWD/lib/ -lftd2xx

#INCLUDEPATH += $$PWD/include
#DEPENDPATH += $$PWD/include

#unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libftd2xx.a

#unix:!macx: LIBS += -L$$PWD/lib/ -lfxISOL

#INCLUDEPATH += $$PWD/include
#DEPENDPATH += $$PWD/include

#unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libfxISOL.a

#unix:!macx: LIBS += -L$$PWD/lib/ -lfxISOscan

#INCLUDEPATH += $$PWD/include
#DEPENDPATH += $$PWD/include

#unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libfxISOscan.a

#unix:!macx: LIBS += -L$$PWD/lib/ -lfxoem

#INCLUDEPATH += $$PWD/include
#DEPENDPATH += $$PWD/include

#unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libfxoem.a

unix:!macx: LIBS += -L$$PWD/lib64/ -lfxoem

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

unix:!macx: PRE_TARGETDEPS += $$PWD/lib64/libfxoem.a

unix:!macx: LIBS += -L$$PWD/lib64/ -lfxISOscan

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

unix:!macx: PRE_TARGETDEPS += $$PWD/lib64/libfxISOscan.a

unix:!macx: LIBS += -L$$PWD/lib64/ -lfxISOL

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

unix:!macx: PRE_TARGETDEPS += $$PWD/lib64/libfxISOL.a

unix:!macx: LIBS += -L$$PWD/lib64/ -lftd2xx

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

unix:!macx: PRE_TARGETDEPS += $$PWD/lib64/libftd2xx.a
