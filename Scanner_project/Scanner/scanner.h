#ifndef SCANNER_H
#define SCANNER_H

#include <QImage>
#include <QDebug>

#include "scanner_include/fxISOdll.h"
#include "scanner_global.h"

class SCANNERSHARED_EXPORT Scanner
{

public:
    Scanner();
    QImage scanImage();
};

#endif // SCANNER_H
