#ifndef NORMALISATION_H
#define NORMALISATION_H

#include "normalisation_global.h"

#include "arrayfire.h"

class NORMALISATIONSHARED_EXPORT Normalisation
{
public:
    Normalisation();

    static af::array run(af::array &);
};

#endif // NORMALISATION_H
