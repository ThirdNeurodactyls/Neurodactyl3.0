#ifndef GABOR_H
#define GABOR_H

#include "gabor_global.h"

#include "arrayfire.h"

#include "helpers.h"
#include "orientationmap.h"
#include "segmentation.h"

class GABORSHARED_EXPORT Gabor
{
public:
    af::array af_imgIn;
    af::array af_imgOut;

    //Histogram Equalization
    int cpp_histEqual_min;
    int cpp_histEqual_max;
    int cpp_histEqual_nBins;

    //Segmentation
    int cpp_segm_kSize;
    int cpp_segm_varThreshold;

    //Orientation Map
    int cpp_oMap_kSize;
    int cpp_oMap_gaussKSize;
    
    //Kernel settings
    float cpp_gamma;
    float cpp_lambda;
    float cpp_psi;
    float cpp_sigma;
    int cpp_kSize;

    Gabor();

    void run();
    af::array compute_gaborKernels();

    void setAf_imgIn(const af::array&);
    af::array getAf_imgIn();
    af::array getAf_imgOut();
};
#endif // GABOR_H
