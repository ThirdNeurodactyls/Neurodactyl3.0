#ifndef ORIENTATIONMAP_GLOBAL_H
#define ORIENTATIONMAP_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ORIENTATIONMAP_LIBRARY)
#  define ORIENTATIONMAPSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ORIENTATIONMAPSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ORIENTATIONMAP_GLOBAL_H
