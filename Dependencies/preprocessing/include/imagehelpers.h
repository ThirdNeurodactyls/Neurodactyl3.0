#ifndef IMAGEHELPERS_H
#define IMAGEHELPERS_H

#include "imagehelpers_global.h"

#include "arrayfire.h"
//#include "opencv2/imgproc.hpp"
#include <QImage>

#include "helpers.h"

class IMAGEHELPERSSHARED_EXPORT ImageHelpers
{
private:

public:
    ImageHelpers();

    static af::array binarizeImage(af::array af_in, int cpp_bin_threshold);
    static af::array invertImage(af::array af_in);
    static af::array normalizeImage(af::array af_in);
};

#endif // IMAGEHELPERS_H
