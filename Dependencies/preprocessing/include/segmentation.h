#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include "segmentation_global.h"

#include "arrayfire.h"

#include "imagehelpers.h"

class SEGMENTATIONSHARED_EXPORT Segmentation
{
public:
    Segmentation();

    static af::array run(af::array af_imgIn, int cpp_kSize, int cpp_varThreshold);
};

#endif // SEGMENTATION_H
