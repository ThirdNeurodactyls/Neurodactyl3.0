#ifndef PREPROCESSING_H
#define PREPROCESSING_H

//Project imports
#include "converter.h"
#include "gabor.h"
#include "normalisation.h"
#include "preprocessing_global.h"
#include "segmentation.h"
#include "skeleton.h"

//Libraries imports
#include "arrayfire.h"
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <QImage>
#include <vector>


typedef struct settings
{
    settings() {
        cpp_use_histEqual = true;

        cpp_use_segm = false;
        cpp_segm_kSize = 17;
        cpp_segm_varThreshold = 100;

        cpp_oMap_mode = 0;
        cpp_oMap_kSize = 17;
        cpp_oMap_useGauss = true;
        cpp_oMap_gaussKSize = 5;

        cpp_gabor_kSize = 17;
        cpp_gabor_gamma = 1.0f;
        cpp_gabor_lambda = 8.0f;
        cpp_gabor_psi = 0.0f;
        cpp_gabor_sigma = 4.0f;

        cpp_use_binarization = true;
        cpp_bin_threshold = -125;

        cpp_use_skeletonization = true;
    }

    /********************************/
    /* Normalisation */
    /********************************/
    ///
    /// \brief cpp_histEqual_mode
    /// Histogram equalization usage
    /// true - enabled
    /// false - disabled
    ///
    bool cpp_use_histEqual;


    /********************************/
    /* Segmentation */
    /********************************/
    ///
    /// \brief cpp_segm_mode
    /// Segmentation usage
    /// true - enabled
    /// false - disabled
    ///
    bool cpp_use_segm;

    ///
    /// \brief cpp_segm_kSize
    /// Size of square kernel in pixels for variance threshold method
    ///
    int cpp_segm_kSize;

    ///
    /// \brief cpp_segm_mean
    /// Desired mean for variance threshold method
    ///
    int cpp_segm_mean;

    ///
    /// \brief cpp_segm_variance
    /// Desired variance for variance threshold method
    int cpp_segm_variance;

    ///
    /// \brief cpp_segm_varThreshold
    /// Variance threshold. If computed variance in kernel is bigger than threshold, this kernel is considered to be fingerprint.
    int cpp_segm_varThreshold;


    /********************************/
    /* Orientation map */
    /********************************/
    ///
    /// \brief cpp_oMap_mode
    /// Orientation map mode.
    /// 1 - Quailty version, performance fallback
    /// 2 - Less quality version, performance improvement
    int cpp_oMap_mode;

    ///
    /// \brief cpp_oMap_kSize
    /// Size of square kernel in pixels for orientation map computation. Bigger kernel size results in bigger performance fallback.
    ///
    int cpp_oMap_kSize;

    ///
    /// \brief cpp_oMap_useGauss
    /// Whether use filtering with Gauss kernel to get results with fewer anomalies.
    bool cpp_oMap_useGauss;

    ///
    /// \brief cpp_oMap_gaussKSize
    /// Size of square Gauss kernel for filtering.
    int cpp_oMap_gaussKSize;

    /********************************/
    /* Frequency map */
    /********************************/
    int cpp_fMap;
    int cpp_fMap_kSize;
    bool cpp_fMap_useGauss;
    int cpp_fMap_gaussKSize;
    int cpp_fMap_globMinimsPers;

    /********************************/
    /* Gabor filtering */
    /********************************/
    ///
    /// \brief cpp_gabor_kSize
    /// Size of square kernels for Gabor filtering.
    int cpp_gabor_kSize;

    int cpp_gabor_gamma;
    int cpp_gabor_lambda;
    int cpp_gabor_psi;
    int cpp_gabor_sigma;

    /********************************/
    /* Binarization */
    /********************************/
    bool cpp_use_binarization;
    int cpp_bin_threshold;

    /********************************/
    /* Skeletonization */
    /********************************/
    bool cpp_use_skeletonization;
} Settings;

class PREPROCESSINGSHARED_EXPORT Preprocessing
{
public:
    af::array af_imgIn;
    af::array af_imgOut;
    QImage qt_skeleton;

    Preprocessing();
    Preprocessing(Settings settings);

    Gabor *g;

    Settings settings;
    std::map<std::string, double> timing;

    void run();

    /********************************/
    /* Getter and setters functions */
    /********************************/
    af::array getAf_imgIn() const;
    void setAf_imgIn(const af::array &value);

    af::array getAf_imgOut() const;
};

#endif // PREPROCESSING_H
