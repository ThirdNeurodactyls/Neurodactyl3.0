// FXDLL_LINUX must be defined for compilation under Linux

#ifndef HEADER_FXISODLL_H
#define HEADER_FXISODLL_H

#ifdef FXISO_IA
	#ifndef ARM9
	#define BkFloat __int32
	#else
	#define BkFloat int
	#endif
#else
#define BkFloat float
#endif

#ifndef FXDLL_LINUX
	// win32
#ifndef ARM9
	#ifndef WINVER
		#include <windows.h>
  #endif

  #if WINVER<0x0400
		#error WINVER must be at least 0x0400
  #endif
	
	#define _fxcall __stdcall
#else
	#include "type.h"
	#include "Target.h"
	#define _fxcall
#endif

	#ifdef _LIB
		#define _fxexport
	#else
		#define _fxexport __declspec(dllexport)
	#endif

#else // linux
	#define _fxexport 
	#define _fxcall 
	#ifndef HWND
	#define HWND void*
	#endif
#endif // FXDLL_LINUX



#ifdef __cplusplus
extern "C" {
#endif

typedef int FXISOerror;
#define FxISO_OK                        0
#define FxISO_InsufficientMemory        1
#define FxISO_TimeOut                   3
#define FxISO_FileNotExisting           4
#define FxISO_InvalidFingerprintData    5
#define FxISO_TooLowQuality             6
#define FxISO_InvalidModelFile          7
#define FxISO_ModelNotCreated           8
#define FxISO_CannotWritetoDisk         9
#define FxISO_CannotUpdateModel         10
#define FxISO_InvalidMode               11
#define FxISO_FingerprintNotAvailable   12
#define FxISO_CannotProcessFingerprint  13
#define FxISO_LowNumberOfMinutiae       14
#define FxISO_ModelNotAvailable         15
#define FxISO_Cancel                    16
#define FxISO_InvalidSize               17
#define FxISO_InvalidParameter          18
#define FxISO_InitNotCalled             20




#define FxISO_AcquisitionError          100
// Acquisition error specification (added to FxISO_Acquisition Error)
	#define FxISO_SCAN_DllNotFound 10
	#define FxISO_SCAN_CorruptedDll 11
	#define FxISO_SCAN_InitializationFailure 12
	#define FxISO_SCAN_SettingFailure 13
	#define FxISO_SCAN_GrabbingFailure 14
	#define FxISO_SCAN_UnInitializationFailure 15
	#define FxISO_SCAN_ShowError 16
	#define FxISO_SCAN_TimerError 17
	#define FxISO_SCAN_InvalidParameters 18
	#define FxISO_SCAN_NoOnlineCapability 20
	#define FxISO_SCAN_InvalidFingerprintImage 21
	#define FxISO_SCAN_InvalidLatent 22
	#define FxISO_SCAN_InvalidLiveness 23
	#define FxISO_SCAN_FeatureNotAvailable 24
	#define FxISO_SCAN_CommunicationError 25
	#define FxISO_SCAN_FilePermissionError 26
	#define FxISO_SCAN_FileSystemError 27

// Format modes
#define ISOFORMAT_STANDARD	0
#define ISOFORMAT_CARD		1
#define ISOFORMAT_COMPACT	2

// Speed Mode
#define SPEED_MODE_STANDARD 1
#define SPEED_MODE_FAST     2

// Acquisition Targets
#define TARGET_PC 1
#define TARGET_SCANNER 2

// Connections
#define CONNECTION_ON 1
#define CONNECTION_OFF 2

//ResolutionMode
#define NATIVE_RESOLUTION 1	//NATIVE is an Obsolete definition: please use a specific DPI output
#define DPI500_RESOLUTION 2
#define DPI569_RESOLUTION 3
#define DPI1000_RESOLUTION 4

//Fingerprints position index
#define FING_POS_UNKNOWN     -1
#define FING_RIGHT_THUMB      0
#define FING_RIGHT_INDEX      1
#define FING_RIGHT_MEDIUM     2
#define FING_RIGHT_RING       3
#define FING_RIGHT_LITTLE     4
#define FING_LEFT_THUMB       5
#define FING_LEFT_INDEX       6
#define FING_LEFT_MEDIUM      7
#define FING_LEFT_RING        8
#define FING_LEFT_LITTLE      9

//Fingerprint Led colors
#define FING_LED_OFF       0
#define FING_LED_RED       1
#define FING_LED_ORANGE    2
#define FING_LED_GREEN     3
#define FING_LED_BLUE      4


//------------------------------------------------------------------------------
// ------------------- Initial configuration functions -------------------------
//------------------------------------------------------------------------------
// Functions to initialize the library and set a custom encryption key for the models
//------------------------------------------------------------------------------

_fxexport FXISOerror _fxcall FxISO_Init(void);
// Initialization; creates FingSlot SMslot and MMslot
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   FxISO_InsufficientMemory: insufficient memory for initialization

_fxexport FXISOerror _fxcall FxISO_End(void);
// Releases memory and terminates
// RETURNED VALUE
//   FxISO_OK: the function succeeded

_fxexport FXISOerror _fxcall FxISO_Version(char* fxISO,char* fxISOscan,char* fxISOenrdlg);
// Return the version of fxISO, fxISOscan and fxISOenrdlg dlls
// PARAMETERS
//         fxISO: Input - pointer to the char array where the fxISO.dll version will be copied.
//                      The array must be allocated by the caller (15 byte is the max size of the string returned)
//                      If the caller is not interested pass NULL          
//     fxISOscan: Input - pointer to the char array where the fxISOscan.dll version will be copied.
//                      The array must be allocated by the caller (15 byte is the max size of the string returned) 
//                      If the caller is not interested pass NULL
//   fxISOenrdlg: Input - pointer to the char array where the fxISOenrdlg.dll version will be copied.
//                      The array must be allocated by the caller (15 byte is the max size of the string returned) 
//                      If the caller is not interested pass NULL


_fxexport FXISOerror _fxcall FxISO_SetLanguage(int langCode);
// Set the language used on graphic interfaces like AGI, EGI and AFIS
// PARAMETERS
//   langCode: Input - the language code is one of follows:
//                     0 - ITALIAN
//                     1 - ENGLISH
// RETURNED VALUE
//   FxISO_OK: the function succeeded

_fxexport FXISOerror _fxcall FxISO_GetLanguage(int *langCode);
// Get the current language used on graphic interfaces like AGI, EGI and AFIS
// PARAMETERS
//   langCode: Output - the language code
// RETURNED VALUE
//   FxISO_OK: the function succeeded



//------------------------------------------------------------------------------
// ------------------ Enumeration methods to address devices -------------------
//------------------------------------------------------------------------------
// Functions to open a scanner different from the default scanner, and to know
// the properties of the connected scanners
//------------------------------------------------------------------------------

//Structure for enumeration of connected devices
typedef struct
{
	DWORD devID;			//handle of the device
	DWORD width, height;	//width and height of the image
	DWORD dpi;				//Dot Per Inch of the image
	DWORD status;			//status of the scanner
	BYTE serialNumber[16];	//univocal scanner serial number
	char scannerName[16];	//"HiScan", "Fx3000", "Fx2000"...
	BYTE unused[16];		//reserved for future use
} FxDevInfo;

_fxexport FXISOerror _fxcall FxISO_Dev_Enumerate(int *nScanners);
//	Start the enumeration of the connected devices. 
//	PARAMETERS
//		nScanners: Output - the number of scanners connected
//	RETURNED VALUE
//		FXISO_OK: the function succeeded

_fxexport FXISOerror _fxcall FxISO_Dev_GetNext(FxDevInfo *scannerInfo);
//	Get the info on the first available scanner. 
//	Repeatedly calling this functions all the scanners are queried.
//	PARAMETERS
//		scannerInfo: Output - info structure on the available scanner. The "devID" field is the device handle.
//	RETURNED VALUE
//		FXISO_OK: the function succeeded

_fxexport FXISOerror _fxcall FxISO_Dev_SetActive(DWORD hDevice);
//	Set the default scanner that will be opened for the next acquisition. 
//	PARAMETERS
//		hDevice: Input - the handle of a scanner as returned in the "devID" field of 
//                       the info device returned by a call to FxISO_Dev_GetNextDevice()
//	RETURNED VALUE
//		FXISO_OK: the function succeeded

_fxexport FXISOerror _fxcall FxISO_Dev_GetActive(FxDevInfo *scannerInfo);
//	Get the default scanner that is used for acquisitions
//	PARAMETERS
//		scannerInfo: Output - the info structure on the current active scanner.
//	RETURNED VALUE
//		FXISO_OK: the function succeeded


//------------------------------------------------------------------------------
// ----------------- Functions to acquire an image in FingSlot -----------------
//------------------------------------------------------------------------------
// Functions to acquire an image in Automatic or Attended mode, using Biometrika
// user interface or silent mode
//------------------------------------------------------------------------------


_fxexport FXISOerror _fxcall FxISO_Fing_EnableAcquisitionRollMode(BOOL enabled, int windowPosition);
// Enable the acquisition of a rolled fingerprint
// PARAMETERS
//   enabled: Input - TRUE to enable the rolled mode, FALSE to disable
//	 windowPosition: Input -	0 for the leftmost acquisition window
//								1 for the centered acquisition window
//								2 for the rightmost acquisition window
// RETURNED VALUE
//   FxISO_OK: the function succeeded

#ifndef ARM9
_fxexport FXISOerror _fxcall FxISO_Fing_AcquireAutomatic(HWND hwnd, int x, int y, float* quality, int det_th, float timeout, float delay);
// Captures one fingerprint image automatically from the device into FingSlot
//   Opens an Auto AGI dialog box which shows the live grabbing.
// PARAMETERS
//   hwnd: Input - handler of the window which will be the parent window of ACQDIAG.
//         NULL can be used for console applications or if an hwnd is not available.
//   x,y: Input - coordinates of the first corner (left,up) where ACQDIAG will be drawn. 
//        use negative valeus (i.e. -1,-1) to automatically center ACQDIAG in the screen.             
//   quality: Output - quality of the returned fingerprint image.
//   det_th: Input - detection presence threshold (0-100);
//           as soon as "detection" exceeds det_th a fingerprint image is captured and returned.      
//   t: Input - timeout (in seconds)
//   d: Input - delay (in seconds)

// RETURNED VALUE
//   FxISO_OK: the function succeeded, a valid fingerprint image has been copied in FingSlot
//   FxISO_Cancel: the user pressed the button 'Cancel'
//   FxISO_TimeOut: the function returned because of the timeout
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FxISOSCAN errors) 

_fxexport FXISOerror _fxcall FxISO_Fing_AcquireSupervised(HWND hwnd, int x, int y, float* quality);
//	Captures one fingerprint image from the device into FingSlot. 
//	Supervised by the operator means: the user choose when to grab the image.
//	Opens a Manual AGI dialog box which shows the live grabbing.
// PARAMETERS
//   hwnd: Input - handler of the window which will be the parent window of manual AGI dialog box.
//         NULL can be used for console applications or if an hwnd is not available.
//   x,y: Input - coordinates of the first corner (left,up) where the AGI will be drawn. 
//        use negative valeus (i.e. -1,-1) to automatically center the AGI on the screen. 
//   quality: Output - quality of the returned fingerprint image.
// RETURNED VALUE
//   FxISO_OK: the function succeeded, a valid fingerprint image has been copied in FingSlot
//   FxISO_Cancel: the user pressed the button 'Cancel'
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FxISOSCAN errors) 
#endif

_fxexport FXISOerror _fxcall FxISO_Fing_MuteAcquireAutomatic(float* quality,int det_th,float timeout,float delay);
// Behaves exactly like FxISO_Fing_AcquireAutomatic, but no graphical display is performed

#ifndef __PGRABCALLBACK
typedef int (*PGRABCALLBACK)(int presence100,int quality100,unsigned char *image,unsigned short ix,unsigned short iy);
#define __PGRABCALLBACK
#endif
_fxexport FXISOerror _fxcall FxISO_Fing_CallBackAcquireSupervised(float* quality,PGRABCALLBACK cbFunc);
_fxexport FXISOerror _fxcall FxISO_Fing_CallBackAcquireAutomatic(float* quality,int det_th,float timeout,float delay,PGRABCALLBACK cbFunc);
// Behaves exactly like FxISO_Fing_AcquireSupervised and FxISO_Fing_AcquireAutomatic respectively,
//   but a callback function is here provided for supervisioning the captured images


//------------------------------------------------------------------------------
// --------- Functions to create memory buffers and Load/Save on file ----------
//------------------------------------------------------------------------------
// All these functions begins with FxISO_Mem_ and handle memory buffers used by 
// the SDK.
// Buffers are used to hold:
//	+fingerprint images
//	+compressed fingerprint images (wsq)
//	+fingerprint models
//------------------------------------------------------------------------------

typedef struct
{
	DWORD hMemory;			//Handle of the allocated memory
	int sizeUsed;			//size used
	int imageWidth;			//if the buffer holds a bitmap, these are the image width
	int imageHeight;		// and height
	DWORD reserved;			//reserved for future use
} FxBuffer;

_fxexport FXISOerror _fxcall FxISO_Mem_NewBuffer(FxBuffer *buffer);
//Allocates a memory buffer
//Each allocated buffer should be freed with a call to FxISO_Mem_DeleteBuffer() 

_fxexport FXISOerror _fxcall FxISO_Mem_DeleteBuffer(FxBuffer *buffer);
//Releases the previously allocated memory buffer

_fxexport FXISOerror _fxcall FxISO_Mem_LoadBufferFromFile(char *pathname, FxBuffer *buffer);
//Loads buffer data from "pathname" file. Automatically allocates memory for the buffer if needed
// PARAMETERS
//   pathname:	Input  - source file pathname
//	 buffer:	Output - the FxBuffer that will hold the readed informations. 
//				If the size is not enough, it will automatically allocates new memory and deallocates the old memory
//				If the file is a TIF image (or a bmp) the size is in "imageWidth" and "imageHeight" fields.
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the file has been loaded in buffer
//   FxISO_FileNotExisting: the file denoted by pathname does not exist.
//   FxISO_InvalidParameter: invalid parameter or file format.

_fxexport FXISOerror _fxcall FxISO_Mem_SaveBufferToFile(char *pathname, FxBuffer *buffer);
//Saves buffer data on "pathname" file. If data is an image a correct file name extension should be 
//used: �.bmp� to save the image as a Windows bitmap, or �.tif� to save the image in TIFF format. 
// PARAMETERS
//   pathname:	Input  - output file pathname
//	 buffer:	Input - the FxBuffer that holds the informations to be written. 
//				If the file is a TIF image (or a bmp) the size should be set in "imageWidth" and "imageHeight" fields.
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the buffer has been saved
//   FxISO_CannotWritetoDisk: the file specified by pathname cannot be written
//   FxISO_InvalidParameter: invalid parameter or file format.

_fxexport FXISOerror _fxcall FxISO_Mem_CopyBufferToArray(FxBuffer *buffer, BYTE *userArray, int nMaxItems);
//Copy the content of the memory buffer to an user defined array
// PARAMETERS
//	 buffer:	Input - the FXBuffer that holds the informations to be written. 
//	 userArray	Output - the user allocated array, destination of the copy
//	 nMaxItems	Input - the lenght of the array
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the file has been copied into the user supplied array
//   FxISO_InvalidParameter: invalid parameter or file format.

_fxexport FXISOerror _fxcall FxISO_Mem_CopyArrayToBuffer(BYTE *userArray, int nItems, FxBuffer *buffer);
//Copy the content of the user defined array to the memory buffer
// PARAMETERS
//	 userArray	Input - the user allocated array, source of the copy
//	 nMaxItems	Input - the lenght of the array
//	 buffer:	Output - the FxBuffer destination of the copy 
//				If the size is not enough, it will automatically allocates new memory and deallocates the old memory
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the file has been loaded in buffer
//   FxISO_InvalidParameter: invalid parameter or file format.

//------------------------------------------------------------------------------
// ------------------------- Bitmap specific functions -------------------------
//------------------------------------------------------------------------------
// If an FxBuffer holds a fingerprint bitmap, these functions gives access to the
// image. 
//------------------------------------------------------------------------------

_fxexport FXISOerror _fxcall FxISO_Mem_CopyImageArrayToBuffer(BYTE *imageArray, int imageWidth, int imageHeight, FxBuffer *buffer);
//Copy the content of the user defined array to the memory buffer As in "FxISO_Mem_CopyArrayToBuffer"
// and set the imageWidth and imageHeight of the buffer
// PARAMETERS
//	 userArray	 Input - the user allocated array, source of the copy, contains the image data 
//	 imageWidth	 Input - the width of the image
//	 imageHeight Input - the height of the image
//	 buffer:	 Output - the FxBuffer destination of the copy 
//				If the size is not enough, it will automatically allocates new memory and deallocates the old memory
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the file has been loaded in buffer
//   FxISO_InvalidParameter: invalid parameter

typedef struct 
{
	DWORD	size;
    DWORD	width;
	DWORD	height;
	BYTE	info[1052];
} FxBITMAPINFO;

_fxexport BYTE* _fxcall FxISO_Mem_GetImage(FxBuffer *imageBuffer, FxBITMAPINFO *bmpInfo);
// Gives access to the image stored into the buffer 
// PARAMETERS
//	imageBuffer:   Input - the buffer structure with the image.
//					The image size is stored in "imageWidth" and "imageHeight" fields.
//	bmpInfo: Output - contains the size of the image. Can be used in Windows Platform SDK functions 
//					when a BITMAPINFO is required.
// RETURNED VALUE
//	returns a pointer to the memory region where a WidthxHeight pixel image is stored by rows 
//	from the top to the bottom and in the row from the left to the right.	
//	This is the original image location stored in "imageBuffer", do NOT free this pointer.
// NOTE. Usefull to create a bitmap, for example calling:
//
//		BYTE *bmpData = FxISO_Mem_GetImage(&imageBuffer, &bmpInfo);
//		Gdiplus::Bitmap* pBitmap = Gdiplus::Bitmap.FromBITMAPINFO((BITMAPINFO*)&bmpInfo, (VOID*)bmpData);
//

_fxexport BYTE _fxcall FxISO_Mem_GetImagePixel(FxBuffer *imageBuffer, int x, int y);
//Given the (x,y) point on the image returns the pixel color. 
//If (x,y) is outside the image boundaries the color is white (255).
// PARAMETERS
//	imageBuffer:   Input - the buffer structure with the image.
//					The image size is stored in "imageWidth" and "imageHeight" fields.

_fxexport BYTE* _fxcall FxISO_Mem_GetImageRow(FxBuffer *imageBuffer, int numRow);
//Given "numRow" the number of the row, returns a pointer to an array of pixels.
//If "numRow" if beyond the image height, returns a NULL pointer.
// PARAMETERS
//	imageBuffer:   Input - the buffer structure with the image.
//					The image size is stored in "imageWidth" and "imageHeight" fields.
// RETURNED VALUE
//	returns a pointer to the memory region where a row of "Width" pixel is stored from 
//	the left to the right.
//	This is the original image location stored in "imageBuffer", do NOT free this pointer.
//


//------------------------------------------------------------------------------
// --------------- Functions to Load/Save an image in FingSlot -----------------
//------------------------------------------------------------------------------
// All these functions begins with FxISO_Fing_ and gives access to the internally 
// stored image in FingSlot
//------------------------------------------------------------------------------

_fxexport FXISOerror _fxcall FxISO_Fing_LoadFromMemory(FxBuffer* imageBuffer, float* q);
// Loads(copies) one fingerprint image from memory into FingSlot
// PARAMETERS
//   imageBuffer: Input - the buffer structure with the image.
//					The image size is stored in "imageWidth" and "imageHeight" fields.
//   q: Output - quality of the loaded fingerprint image.
// RETURNED VALUE
//   FxISO_OK: the function succeeded, a valid fingerprint image has been loaded in FingSlot
//   FxISO_TooLowQuality: the image quality is too low and it cannot be processed by FxISO;
//                      no fingerprint has been loaded in FingSlot.
//   FxISO_InvalidSize: the Width or/and Height passed are not valid

_fxexport FXISOerror _fxcall FxISO_Fing_SaveToMemory(FxBuffer* imageBuffer,
														unsigned char resolutionMode, 
														int *OutputResolution);
// Saves(copies) the fingerprint image in FingSlot to memory
// The image is saved at the required DPI resolution if supported by the fingerprint scanner.
// Images saved by this function can be reloaded into the FingSlot by using the 
// FxISO_Fing_LoadFingerprintFromMemory function
// PARAMETERS
//   imageBuffer: Output - the buffer structure with the image.
//					The image size is stored in "imageWidth" and "imageHeight" fields.
//					** NOTE **: if the "memory" handler is NULL or the allocated size is 
//					insufficient the SDK will automatically allocates a memory region.
//					The user can free the allocated buffer calling FxISO_Mem_DeAllocateBuffer()
//
//   resolutionMode: Input - DPI500_RESOLUTION, DPI569_RESOLUTION or DPI1000_RESOLUTION
//   outputResolution: Output - the resolution of the saved image
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the fingerprint image (in FingSlot) has been stored
//   FxISO_FingerprintNotAvailable: no fingerprints in FingSlot

_fxexport FXISOerror _fxcall FxISO_Fing_SaveToWSQ(FxBuffer* wsqBuffer, float bitrate,
																unsigned char resolutionMode, 
																int *OutputResolution);
// Exports the fingerprint image to a standard compressed WSQ buffer
// The image is saved at the required DPI resolution if supported by the fingerprint scanner.
// Note: images saved by this function can be reloaded into the FingSlot
// PARAMETERS
//   wsqBuffer: Output - the buffer structure with the wsq compressed image.
//						The size of the buffer is stored in "sizeUsed" field.
//					** NOTE **: if the "memory" handler is NULL or the allocated size is 
//					insufficient the SDK will automatically allocates a memory region.
//					The user should free the allocated buffer calling FxISO_Mem_DeAllocateBuffer()
//
//   bitrate: Input - the compression bit rate. Suggested values:
//                 0.75 (about 1:15 ratio, excellent compression and acceptable quality)
//                 1.0  (about 1:12 ratio)
//                 1.2  (about 1:10 ratio, good image quality)
//                 1.5  (about 1:8 ratio)
//                 2.25 (about 1:5 ratio, excellent image quality)
//   resolutionMode: Input - DPI500_RESOLUTION, DPI569_RESOLUTION or DPI1000_RESOLUTION
//   outputResolution: Output - the resolution of the saved image
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the fingerprint image (in FingSlot) has been stored
//   FxISO_FingerprintNotAvailable: no fingerprints in FingSlot

_fxexport FXISOerror _fxcall FxISO_Fing_LoadFromWSQ(FxBuffer* wsqBuffer, float* q);
// Loads one fingerprint image from a WSQ compressed buffer into FingSlot
// PARAMETERS
//   wsqBuffer: Input - the buffer structure with the wsq compressed image.
//						"memory" is a pointer to the memory region where the image is stored.
//						The size of the buffer is stored in "sizeUsed" field.
//   q: Output - quality of the loaded fingerprint image.
// RETURNED VALUE
//   FxISO_OK: the function succeeded, a valid fingerprint image has been loaded in Fslot
//   FxISO_InvalidFingerprintData: invalid file format.
//   FxISO_TooLowQuality: the image quality is too low and it cannot be processed by FxISO;
//                      no fingerprint has been loaded in FingSlot.

//------------------------------------------------------------------------------
// ---------------------- FX Scanner Additional Commands -----------------------
//------------------------------------------------------------------------------
// All these functions begins with FxISO_Dev_ and gives access to specific scanner 
// features
//------------------------------------------------------------------------------

_fxexport int _fxcall FxISO_Dev_Standby(void);
// Put the sensor in standy mode (power save)
// The sensor must be re-activated before calling acquisition/setting function  
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FxISOSCAN errors) 

_fxexport int _fxcall FxISO_Dev_Resume(void);
// Re-activate the sensor from standby mode.
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FxISOSCAN errors) 

_fxexport int _fxcall FxISO_Dev_GetConfig(unsigned char* LiveMode,unsigned char* InternalLight,unsigned char* RedLed,unsigned char* GreenLed);
// Read the sensor configuration: 
// If a NULL value is passed as a parameter pointer the corresponding setting is ignored
// PARAMETERS
//       LiveMode: Output - denotes the visual feedback mode during acquisition; use:
//                 1 for FULL_IMAGE
//                 2 for HALF_IMAGE
//                 3 for BLIND
//  InternalLight: Output - Internal light (red leds) activation ( 1:ON - 2:OFF )
//         RedLed: Output - Red led activation ( 1:ON - 2:OFF )
//       GreenLed: Output - Green Led activation ( 1:ON - 2:OFF )
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FxISOSCAN errors) 

_fxexport int _fxcall FxISO_Dev_SetConfig(unsigned char* LiveMode,unsigned char* InternalLight,unsigned char* RedLed,unsigned char* GreenLed);
// Set the sensor configuration: 
// If a NULL value is passed as a parameter pointer the corresponding setting is ignored
// PARAMETERS
//       LiveMode: Input - denotes the visual feedback mode during acquisition; use:
//                 1 for FULL_IMAGE (Default)
//                 2 for HALF_IMAGE
//                 3 for BLIND
//  InternalLight: Input - Internal light (red leds) activation ( 1:ON - 2:OFF )
//         RedLed: Input - Red led activation ( 1:ON - 2:OFF )
//       GreenLed: Input - Green Led activation ( 1:ON - 2:OFF )
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FxISOSCAN errors) 

_fxexport int _fxcall FxISO_Dev_SetAcquisitionTarget(unsigned char Target);
// Set the target of fingerprint acquisition. 
//   By defualt all the acquisition functions FxISO_Fing_Acquire... acquire a fingerprint image
//   on the PC (the default Target is the PC).
//   To perform Match On Board (only with scanners supporting MOB like FX3000) the acquisition
//   target must be the Scanner, so that the (last) fingerprint image is stored into the scanner
//   and not transferred to the PC. Even if the scanner is set as acquisition target, depending
//   on the LiveMode set with FxISO_Dev_SetConfig, the intermediate fingerprint images collcted during
//   the acquisition process can be still sent to the PC to provide a visual feedback to the user.
//   If one wants to avoid sending (intermediate) fingerprint images to the PC, he/she can
//   simply set LiveMode = BLIND.  
// PARAMETERS
//     Target: 1 TARGET_PC  (Default)
//						 2 TARGET_SCANNER
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FX3SCAN errors) 

_fxexport int _fxcall FxISO_Dev_ForceScannerConnection(unsigned char Connection);
// By default each time an FxISO_Dev_*** or FX3_MOB_*** command have to be executed, a new communication
//   is established with the scanner and, at the end of the command, the communication channel is closed.
//   This is OK for most of the standard applications, while it could be not good for applications
//   using MOB capabilities of MOB scanners (like Fx3000). In fact, if the application needs to send
//   a sequence of FX3_MOB commands, opening and closing the communication channel at each single
//   command is not efficient. By using this command the application can force the communication channel
//   to remain open (CONNECTION_ON) until an explicit closing command is sent (CONNECTION_OFF).
//   The command also works for non-MOB scanners like Fx2000. 
// PARAMETERS
//     Connection: 1 CONNECTION_ON    
//								 2 CONNECTION_OFF  
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FX3SCAN errors) 

_fxexport int _fxcall FxISO_Dev_SetFakeDetection(unsigned char LatentCheck,unsigned char LivenessCheck);
// Set the sensor fake detection mode: 
//   When fake detection is enabled the acquisition function may return an error (see the above FX3SCAN errors)
//   in case a fake finger has been detected
//       LatentCheck: Input - enables/disables check of latent fingerprint left on the sensor surface
//                    O Disabled
//                    1 Enabled
//     LivenessCheck: Input - enables/disables check of finger liveness (not yet implemented)
//										O Disabled
//                    1 Enabled
//       
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FX3SCAN errors) 

_fxexport int _fxcall FxISO_Dev_EnterAutoDetection(void);
// Enter AutoDetection mode: this function must be called before FxISO_Dev_Presence
//  The internal illumination is activated and the two led turned off.
//  The sensor is ready to be polled by using FxISO_Dev_Presence
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FX3SCAN errors) 

_fxexport int _fxcall FxISO_Dev_Presence(unsigned char* Presence);
// Polls the sensor to check the fingerprint presence
// Before using this function you must enter Autodetection mode (FxISO_Dev_EnterAutoDetection)
// At the end of AutoDetection you must call FxISO_Dev_ExitAutoDetection  
// The function takes about 150 ms before returning, even if practically
//   no CPU time is wasted since the PC CPU is internally released by the function during the sensor computations;   
//   therefore the PC can implement a strict polling routine without the risk of subtracting useful time
//   to other processes.  
// PARAMETERS
//     Presence: indicates the finger presence on the sensor
//       0: indicates minimum presence (no finger on the sensor)
//       100: indicates maxinum presence
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FX3SCAN errors) 


_fxexport int _fxcall FxISO_Dev_ExitAutoDetection(void);
// Exit AutoDetection mode: this function must be called to leave autodetection mode
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   >= FxISO_AcquisitionError: an hardware/software problem has been encountered;  
//                            if > FxISO_AcquisitionError the residual (value - FxISO_AcquisitionError )
//                            defines the acquisition suberror occurred (see the above FX3SCAN errors) 


_fxexport FXISOerror _fxcall FxISO_Dev_SetAcquisitionRotation(int rotation);
//Set the rotation of the fingerprint scanner as follows:
//	0: Normal
//	1: 90� clockwise rotation
//	2: 180� rotation
//	3: 90� counterclockwise rotation
// RETURNED VALUE
//	FxISO_OK: the function succeeded
//	FxISO_InvalidMode: rotation not supported


_fxexport FXISOerror _fxcall FxISO_Dev_ActivateCleaning(BOOL enableAuto, BOOL enableUser);
//	Enable the image cleaning procedure in automatic every time the scanner starts an acquisition
//	PARAMETERS
//		enableAuto: Input - TRUE to enable the authomatic image cleaning at every acquisition; FALSE to disable
//		enableUser: Input - TRUE to enable the user to perform a clean of the image when the AGI is displayed
//	RETURNED VALUE
//		FX3_OK: the function succeeded


#define LED_442_HAND_RIGHT   0x3C0
#define LED_442_HAND_LEFT    0x00F
#define LED_442_THUMBS       0x030
#define THUMB_RIGHT_BITMASK  0x020
#define INDEX_RIGHT_BITMASK  0x040
#define MEDIUM_RIGHT_BITMASK 0x080
#define RING_RIGHT_BITMASK   0x100
#define LITTLE_RIGHT_BITMASK 0x200
#define THUMB_LEFT_BITMASK   0x010
#define INDEX_LEFT_BITMASK   0x008
#define MEDIUM_LEFT_BITMASK  0x004
#define RING_LEFT_BITMASK    0x002
#define LITTLE_LEFT_BITMASK  0x001

_fxexport FXISOerror _fxcall FxISO_Dev_EnlightIndicationLed(DWORD activationFlag);
// Enlight the indicator leds on HiScan-FG scanner and 442 scanners.
// Acquisition functions will use the indicator leds, and will turn off the leds 
// automatically at the end of the acquisition.
// PARAMETERS
//      activationFlag: Input - is the activation flag of the scanner's indicator leds
//								It is the same bitmask of the EGI dialog (cfr. fxISOenrdlg.h). 
//								Values are here reported for quick reference:
//										THUMB_RIGHT_BITMASK  0x020
//										INDEX_RIGHT_BITMASK  0x040
//										MEDIUM_RIGHT_BITMASK 0x080
//										RING_RIGHT_BITMASK   0x100
//										LITTLE_RIGHT_BITMASK 0x200
//										THUMB_LEFT_BITMASK   0x010
//										INDEX_LEFT_BITMASK   0x008
//										MEDIUM_LEFT_BITMASK  0x004
//										RING_LEFT_BITMASK    0x002
//										LITTLE_LEFT_BITMASK  0x001
//								Up to 3 fingerprint leds can be turned on at the same time
//								combining the bitmasks.
//								Additional values are available for 442 scanners:		
//										LED_442_HAND_RIGHT   0x3C0
//										LED_442_HAND_LEFT    0x00F
//										LED_442_THUMBS       0x030
//								Only listed values are accepted, invalid values are ignored
// RETURNED VALUE
//     FxISO_OK: the function succeeded


_fxexport FXISOerror _fxcall FxISO_Dev_SetFingerprintLedColor(BYTE vFingerprintLedStatus[10]);
// Sets immediately the color of indicator leds on HiScan-FG scanner.
// It is responsability of the application to turn on/off the leds.
// PARAMETERS
//      vFingerprintLedStatus: Input - is an array, one byte for each fingerprint led.
//                              The fingerprint numeration is the same used in the models,
//                              from FING_RIGHT_THUMB=0 to FING_LEFT_LITTLE=9.
//                              Each led can be set to a different status: 
//								FING_LED_OFF / FING_LED_RED / FING_LED_ORANGE / FING_LED_GREEN / FING_LED_BLUE
//								No more that 3 fingerprint leds can be turned on at the same time.
// RETURNED VALUE
//     FxISO_OK: the function succeeded

_fxexport FXISOerror _fxcall FxISO_Dev_SetAcquisitionDPI(int DPI);
// When working with a scanner that supports more than one DPI settings, it changes the acquisition DPI
// For example the Fx2100 that by default works at 500 DPI, can be set in 569 DPI mode.
// PARAMETERS
//      DPI: Input - the DPI value, 500 or 569. If an invalid DPI is set, the scanner is 
//                   initialized to the default DPI value and an error is returned
// RETURNED VALUE
//     FxISO_OK: the function succeeded
//     FxISO_InvalidParameter: the DPI is not valid, or the scanner doesn't allow multiple settings


//------------------------------------------------------------------------------
// ---------------------------- SingleModel functions --------------------------
//------------------------------------------------------------------------------
// All these functions begins with FxISO_SM_ and operates on the SingleModel object,
// also called SMslot
//------------------------------------------------------------------------------

_fxexport FXISOerror _fxcall FxISO_SM_LoadFromMemory(FxBuffer* model, int format);
// Loads(copies) a model into SMslot from a model buffer
// PARAMETERS
//   model:  Input - the buffer structure with the model.
//					"memory" is a pointer to the memory region where a valid model is stored.
//					The size of the buffer is stored in "sizeUsed" field.
//   format: Input - one of {ISOFORMAT_STANDARD, ISOFORMAT_CARD, ISOFORMAT_COMPACT}
//					The application should know the format used to save the model.
// NOTE:
//   The format defines the size of the minutia: 6, 5 or 3 Bytes
//   If the buffer contains more than one model only the first is loaded
// RETURNED VALUE
//   FxISO_OK: the function succeeded, a 1-model has been loaded into SMslot
//   FxISO_InvalidModelFile: invalid model format.

_fxexport FXISOerror _fxcall FxISO_SM_SaveToMemory(FxBuffer* model, int format, BOOL extended);
// Writes the model from SMslot to a memory buffer
// PARAMETERS
//   model:  Output - the buffer structure with the model.
//					"memory" is a pointer to the memory region where a valid model is stored.
//					The size of the buffer is stored in "sizeUsed" field.
//					** NOTE **: if the "memory" pointer is NULL or the allocated size is 
//					insufficient the SDK will automatically allocates a memory region.
//					The user should free the allocated buffer calling FxISO_Mem_DeAllocateBuffer()
//
//   format:   Input - one of {ISOFORMAT_STANDARD, ISOFORMAT_CARD, ISOFORMAT_COMPACT}
//	 extended: Input - if TRUE saves the extended data (created by the engine for better accuracy)
//						if FALSE removes the extended data from the model, to have a smaller model
// NOTE:
//   The format defines the size of the minutia: 6, 5 or 3 Bytes
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the model from SMslot has been stored to memory
//   FxISO_ModelNotCreated: no models in SMslot.
//   FxISO_InvalidModelFile: the model in SMslot is invalid
//   FxISO_InvalidMode: mode must be 0 or 1

_fxexport FXISOerror _fxcall FxISO_SM_CreateFromFing(void);
// Creates a model in SMslot from the fingerprint image in FingSlot
// RETURNED VALUE
//   FxISO_OK: the function succeeded, a model has been stored in SMslot
//   FxISO_LowNumberOfMinutiae: (WARNING) a low number of minutiae have been detected on the 
//                              fingerprint in FingSlot; anyway, a model has been stored in SMslot
//   FxISO_FingerprintNotAvailable: no fingerprints acquired/loaded in FingSlot 
//                                  (use FxISO_AcquireFingerprint or FxISO_LoadFingerprintFromFile before)
//   FxISO_CannotProcessFingerprint: the quality of the fingerprint in FingSlot is too low;
//                                   a model cannot be created

_fxexport FXISOerror _fxcall FxISO_SM_GetQuality(float *quality);
// Get the quality of the model into SMslot
// PARAMETERS
//   quality:  Output - the quality of the model in the range 0..1
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   FxISO_ModelNotCreated: no model in SMslot

_fxexport FXISOerror _fxcall FxISO_SM_GetNumMinutiae(int *nMinutiae);
// Get the number of minutiae of the model into SMslot
// PARAMETERS
//   nMinutiae:  Output - the number of minutiae of the model 
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   FxISO_ModelNotCreated: no model in SMslot

_fxexport FXISOerror _fxcall FxISO_SM_GetMinutia(int indexMinutia, int *pixelX, int *pixelY, int *angle360, int *quality100);
// Get info on one minutia of the model into SMslot
// PARAMETERS
//   indexMinutia: Input  - the index of the minutia, from 0 to nMinutiae-1
//   pixelX:       Output - the x position on the fingerprint image at 500DPI
//   pixelY:       Output - the y position on the fingerprint image at 500DPI
//   angle360:     Output - the minutia angle, from 0 to 359 degree
//   quality100:   Output - the quality from 1 to 100. Optional for some model format: 0 means "no quality computed"
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   FxISO_ModelNotCreated: no model in SMslot
//   FxISO_InvalidParameter: wrong minutia index

_fxexport FXISOerror _fxcall FxISO_SM_GetMaxSize(int format, int* Size);
// Returns the max model Size according to the required format, extended data included
// This function can be used to determine the size of the buffer the application should 
// create to hold a model.
// PARAMETERS
//   format: Input - one of {ISOFORMAT_STANDARD, ISOFORMAT_CARD, ISOFORMAT_COMPACT}
//   Size: Output - max size for a 1-model
// NOTE:
//   The format defines the size of the minutia: 6, 5 or 3 Bytes
//	 The Overall model Size is: n*Size where n is the number of 1-models used.
// RETURNED VALUE
//   FxISO_OK: the function succeeded

_fxexport FXISOerror _fxcall FxISO_SM_SetCreationMode(int speedMode);
// Sets a given operating mode for model creation
//
// PARAMETERS
//  speed mode: SPEED_MODE_STANDARD  (max. accuracy)
//              SPEED_MODE_FAST      (max. efficiency)
//
// RETURNED VALUE:
//   FxISO_OK: the function succeeded
//   FxISO_InvalidMode: Mode not supported


//------------------------------------------------------------------------------
// ---------------------------- MultiModel functions ---------------------------
//------------------------------------------------------------------------------
// All these functions begins with FxISO_MM_ and operates on the MultiModel object,
// also called MMslot
//------------------------------------------------------------------------------

_fxexport FXISOerror _fxcall FxISO_MM_LoadFromMemory(FxBuffer* model, int format);
// Loads(copies) a model from memory into MMslot
// PARAMETERS
//   model:  Input - the buffer structure with the model.
//					"memory" is a pointer to the memory region where a valid model is stored.
//					The size of the buffer is stored in "sizeUsed" field.
//   format: Input - one of {ISOFORMAT_STANDARD, ISOFORMAT_CARD, ISOFORMAT_COMPACT}
//					The application should know the format used to save the model.
// NOTE:
//   The format defines the size of the minutia: 6, 5 or 3 Bytes
// RETURNED VALUE
//   FxISO_OK: the function succeeded, a valid model has been loaded in MMslot
//   FxISO_InvalidModelFile: invalid model format.

_fxexport FXISOerror _fxcall FxISO_MM_SaveToMemory(FxBuffer* model, int format, BOOL extended);
// Writes the model from MMslot to a memory buffer
// PARAMETERS
//   model:  Output - the buffer structure with the model.
//					"memory" is a pointer to the memory region where a valid model is stored.
//					The size of the buffer is stored in "sizeUsed" field.
//					** NOTE **: if the "memory" pointer is NULL or the allocated size is 
//					insufficient the SDK will automatically allocates a memory region.
//					The user should free the allocated buffer calling FxISO_Mem_DeAllocateBuffer()
//
//   format:   Input - one of {ISOFORMAT_STANDARD, ISOFORMAT_CARD, ISOFORMAT_COMPACT}
//	 extended: Input - if TRUE saves the extended data (created by the engine for better accuracy)
//						if FALSE removes the extended data from the model, to have a smaller model
// NOTE:
//   The format defines the size of the minutia: 6, 5 or 3 Bytes
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the model from Fslot has been stored to memory
//   FxISO_ModelNotCreated: no models in MMslot.
//   FxISO_InvalidModelFile: the model in MMslot is invalid


_fxexport FXISOerror _fxcall FxISO_MM_GetInfo(int nFingerVect[10], int *nUnknownFinger);
// Get the information of how many fingerprints are stored in the Multi Model in MMslot
// PARAMETERS
//   nFingerVect[10]: Output - vector of 10 elements, reporting the number of samples for each finger
//			0: Right Thumb
//			1: Right Forefinger
//			2: Right Middle Finger
//			3: Right Ring Finger
//			4: Right Little Finger
//			5: Left Thumb
//			6: Left Forefinger
//			7: Left Middle Finger
//			8: Left Ring Finger
//			9: Left Little Finger
//   nUnknownFinger: Output - the number of samples for a finger of unknown position
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   FxISO_ModelNotCreated: no models in MMslot.
//   FxISO_InvalidModelFile: the model in MMslot is invalid

_fxexport FXISOerror _fxcall FxISO_MM_Add_SM(int idxFingerType);
// Add the model in SMslot to the Multiple Model in MMslot
// PARAMETERS
//   idxFingerType: Input - the index of the finger from 0 to 9 or -1 if the position is unknown
// NOTE:
//   Remember to clear the MMslot with FxISO_MM_DeleteAll() before starting a new Multi Model
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the model from SMslot has been stored to MMslot
//   FxISO_ModelNotCreated: no models in SMslot.
//   FxISO_InvalidModelFile: the model in MMslot or SMslot is invalid

_fxexport FXISOerror _fxcall FxISO_MM_Extract_SM(int idxFingerType, int nSample);
// Gets the requested model in MMslot and copies it to SMslot
// PARAMETERS
//   idxFingerType: Input - the index of the finger from 0 to 9 or -1 if the position is unknown
//   nSample: Input - the number of the sample to get
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the model from MMslot has been stored to SMslot
//   FxISO_ModelNotCreated: no models in MMslot with the specified idxFinger and nSample.
//   FxISO_InvalidModelFile: the model in MMslot or SMslot is invalid

_fxexport FXISOerror _fxcall FxISO_MM_DeleteSingleModel(int idxFingerType, int nSample);
// Deletes the specified model from MMslot
// PARAMETERS
//   idxFingerType: Input - the index of the finger from 0 to 9 or -1 if the position is unknown
//   nSample: Input - the number of the sample to delete
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the model from MMslot has been deleted
//   FxISO_ModelNotCreated: no models in MMslot with the specified idxFinger and nSample.

_fxexport FXISOerror _fxcall FxISO_MM_DeleteAll();
// Deletes ALL the models in MMslot
// RETURNED VALUE
//   FxISO_OK: the function succeeded, the models from MMslot has been deleted

//------------------------------------------------------------------------------
// ------------------------- Matching Engine functions -------------------------
// The following functions configures the matching engine and perform a match 
// between the two models loaded in SingleModel and MultiModel
//------------------------------------------------------------------------------

_fxexport FXISOerror _fxcall FxISO_Eng_SetSessionKey(BOOL enableChyper, unsigned char* key);
// Sets the session key used to encrypt/decrypt models;
//
//  FxISO uses the session key to encrypt/decrypt fingerprint models.
//  In order to create encrypted models, FxISO_SetSessionKey must be called before:
//    - FxISO_SM_SaveToMemory
//    - FxISO_MM_SaveToMemory
//  On the other hand, if a model has been encrypted by using a specific key,
//  the same key must loaded into the system (by using FxISO_SetSessionKey)
//  before calling the following function to reload the model:  
//    - FxISO_SM_LoadFromMemory
//    - FxISO_MM_LoadFromMemory
//  The same session key remain valid until the FxISO_End is called or another session
//    Key is loaded over the current one. 
//  Therefore it is RESPONSABILITY of the USER APPLICATION to STORE the KEYS used
//  to encrypt the models and RELOAD them into the system when required.   
//
// PARAMETERS
//	enableChyper: Input - if TRUE enables the encryption/decryption of the models
//						if FALSE the models are non encrypted.
//  key: Input - pointer to the memory region (16 bytes) where a session key is stored
//        the session key may assume every possible value (2^128 distinct values)
// RETURNED VALUE
//   FxISO_OK: the key has been set
//

_fxexport FXISOerror _fxcall FxISO_Eng_Matching(float mins, float* similarity);
// matches the fingerprint image loaded in FingSlot (or the 1-model loaded in SMslot)
//         with the (single or multiple) model loaded in MMslot.
// PARAMETERS
//   mins: Input - minimum similarity (for multiple-models)
//   similarity: Output - similarity resulting from the matching
//               0 indicate no similarity, 1 max similarity
//               a threshold must be used to Accept or Reject the User
// RETURNED VALUE
//   FxISO_OK: the function succeeded
//   FxISO_ModelNotAvailable: no model has been loaded in MMslot
//   FxISO_FingerprintNotAvailable: neither fingerprint images nor 1-models in FingSlot or SMslot
//   FxISO_CannotProcessFingerprint: the quality of the fingerprint image in FingSlot is too low;
//                                 the matching cannot be performed


//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif // HEADER_FXISODLL_H
