#ifndef SCANNER_H
#define SCANNER_H

#include <QImage>
#include <QDebug>
#include <qglobal.h>

#ifdef Q_OS_WIN32
#include "fxISOdll.h"
#endif

/**
 * @brief The Scanner class
 */
class Scanner
{
public:
    Scanner();
    QImage scanImage();
};

#endif // SCANNER_H
