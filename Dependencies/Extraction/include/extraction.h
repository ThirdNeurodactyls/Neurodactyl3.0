#ifndef EXTRACTION_H
#define EXTRACTION_H

#include "preprocessing_global.h"
#include "converter.h"
#include "extraction_global.h"
#include "orientationmap.h"


#include "arrayfire.h"
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"


#include <iostream>
#include <utility>


class EXTRACTIONSHARED_EXPORT Extraction
{

public:
    ///
    /// \brief kSize - size of square kernel
    ///
    int kSize;

    ///
    /// \brief cv_imgIn - Input image in skeleton format
    ///
    cv::Mat cv_imgIn;

    ///
    /// \brief cv_imgInOrig - Input image from which skeleton was created
    ///
    cv::Mat cv_imgInOrig;

    cv::Mat cv_imgOutRect;
    cv::Mat cv_imgOutSkelRect;

    cv::Mat cv_imgOut;

    Extraction();

    std::vector<std::pair<cv::Mat, std::string>> run();
    double CN(cv::Mat cv_block);

    int getKSize() const;
    void setKSize(int value);
    cv::Mat getCv_imgIn() const;
    void setCv_imgIn(const cv::Mat value);
    cv::Mat getCv_imgOut() const;
    void setCv_imgOut(const cv::Mat value);
    cv::Mat getCv_imgInOrig() const;
    void setCv_imgInOrig(const cv::Mat value);
    cv::Mat getCv_imgOutRect() const;
    void setCv_imgOutRect(const cv::Mat &value);
    cv::Mat getCv_imgOutSkelRect() const;
    void setCv_imgOutSkelRect(const cv::Mat &value);
};

#endif // EXTRACTION_H
