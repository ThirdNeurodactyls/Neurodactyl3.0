#ifndef GABOR_GLOBAL_H
#define GABOR_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(GABOR_LIBRARY)
#  define GABORSHARED_EXPORT Q_DECL_EXPORT
#else
#  define GABORSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // GABOR_GLOBAL_H
