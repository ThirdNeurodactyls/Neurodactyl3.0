#include "helpers.h"

af::array Helpers::compute_varianceInBlocks2D(af::array af_imgIn, int cpp_kSize)
{
    try {
        af_imgIn = af_imgIn.copy();

        //Input image dimensions
        int cpp_imgHeight = af_imgIn.dims(0) / cpp_kSize,
            cpp_imgWeight = af_imgIn.dims(1) / cpp_kSize;

        //Represent blocks as columns
        af_imgIn = af::unwrap(af_imgIn, cpp_kSize, cpp_kSize, cpp_kSize, cpp_kSize);

        //Compute variance for each block
        af_imgIn = af::var(af_imgIn);

        //Return image to default dimensions, reverse operation of af::unwrap
        af_imgIn = af::moddims(af_imgIn, cpp_imgHeight, cpp_imgWeight);

        return af_imgIn;
    }
    catch (af::exception &e) {
        std::cerr << "Error in CLASS ImageHelpers METHOD compute_varianceInBlocks2D()";
        std::cerr << e.what();

        return NULL;
    }
}

void Helpers::af_arrayInfo(af::array af_in, std::string cpp_varName, bool afPrint)
{
    std::cerr << cpp_varName + ":" << std::endl
              <<"Rows: "<<af_in.dims(0)<<std::endl
              <<"Cols: "<<af_in.dims(1)<<std::endl
              <<"Depth: "<<af_in.dims(2)<<std::endl
              <<"Memory: "<<af_in.bytes()/1024<<" kB";

    if (afPrint)
        af_print(af_in);
}
