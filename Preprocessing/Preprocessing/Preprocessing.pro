#-------------------------------------------------
#
# Project created by QtCreator 2018-01-27T15:18:24
#
#-------------------------------------------------

QT       += gui

TARGET = Preprocessing
TEMPLATE = lib

DEFINES += PREPROCESSING_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        preprocessing.cpp \
    gabor.cpp \
    orientationmap.cpp \
    helpers.cpp \
    imagehelpers.cpp \
    segmentation.cpp \
    normalisation.cpp \
    skeleton.cpp \
    converter.cpp \
    frequencymap.cpp

HEADERS += \
        preprocessing.h \
        preprocessing_global.h \ 
    gabor.h \
    orientationmap.h \
    helpers.h \
    segmentation.h \
    gabor_global.h \
    orientationmap_global.h \
    imagehelpers_global.h \
    segmentation_global.h \
    helpers_global.h \
    normalisation.h \
    normalisation_global.h \
    skeleton.h \
    skeleton_global.h \
    converter.h \
    converter_global.h \
    frequencymap.h \
    frequencymap_global.h \
    skeleton_global.h \
    segmentation.h \
    preprocessing_global.h \
    preprocessing.h \
    orientationmap_global.h \
    orientationmap.h \
    normalisation_global.h \
    normalisation.h \
    imagehelpers_global.h \
    imagehelpers.h \
    helpers_global.h \
    helpers.h \
    gabor_global.h \
    gabor.h \
    frequencymap_global.h \
    frequencymap.h \
    converter_global.h \
    converter.h

unix {
    target.path = /usr/lib
    INSTALLS += target

    LIBS += -L$$PWD/../../../../../../usr/local/lib/ -lopencv_world
    
    INCLUDEPATH += $$PWD/../../../../../../usr/local/include
    DEPENDPATH += $$PWD/../../../../../../usr/local/include
    
    LIBS += -laf
    
    INCLUDEPATH += $$PWD/../../../../../../usr/include
    DEPENDPATH += $$PWD/../../../../../../usr/include
}

win32 {
    LIBS += -L$$PWD/'../../../../Program Files/ArrayFire/v3/lib/' -laf

    INCLUDEPATH += $$PWD/'../../../../Program Files/ArrayFire/v3/include'
    DEPENDPATH += $$PWD/'../../../../Program Files/ArrayFire/v3/include'

    win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../opencv/build/x64/vc14/lib/ -lopencv_world340
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../opencv/build/x64/vc14/lib/ -lopencv_world340d

    INCLUDEPATH += $$PWD/../../../../opencv/build/include
    DEPENDPATH += $$PWD/../../../../opencv/build/include
}



