#include "segmentation.h"

#undef min
#undef max

Segmentation::Segmentation()
{
}

af::array Segmentation::run(af::array af_imgIn, int cpp_kSize, int cpp_varThreshold)
{
    try {
        //Deep copy of array
        af_imgIn = af_imgIn.copy();

        int cpp_blocksRowsNum = af_imgIn.dims(0) / cpp_kSize,
                cpp_blocksColsNum = af_imgIn.dims(1) / cpp_kSize;

        af::array af_mask(cpp_blocksRowsNum, cpp_blocksColsNum);

        //If variance in block < var_threshold replace with 0 (black color, block is assigned to be background)
        af::array af_variance = Helpers::compute_varianceInBlocks2D(af_imgIn, cpp_kSize);
        af::replace(af_mask,  af_variance > cpp_varThreshold, 0);
        af::replace(af_mask,  af_variance <= cpp_varThreshold, 255);

        //Idx -> store indices, val -> store values
        af::array af_idx, af_val;

        //Find boundaries of foreground image
        af::max(af_val, af_idx, af_mask);
        int cpp_top = af::min<float>(af_idx(af_val != 0));

        af::max(af_val, af_idx, af_mask, 1);
        int cpp_left = af::min<float>(af_idx.T()(af_val != 0));

        af_mask = af::flip(af::flip(af_mask, 1), 0);

        af::max(af_val, af_idx, af_mask);
        int cpp_down = af_mask.dims(0) - af::min<float>(af_idx(af_val != 0));

        af::max(af_val, af_idx, af_mask, 1);
        int cpp_right = af_mask.dims(1) - af::min<float>(af_idx.T()(af_val != 0));

        //For debugging purpose
//        std::cerr << " TOP " << cpp_top * cpp_blockSize <<
//                     " DOWN " << cpp_down * cpp_blockSize <<
//                     " LEFT " << cpp_left * cpp_blockSize <<
//                     " RIGHT " << cpp_right * cpp_blockSize;

        //Cropp image for further use
        af_imgIn = af_imgIn.rows(cpp_top * cpp_kSize, cpp_down * cpp_kSize - 1);
        af_imgIn = af_imgIn.cols(cpp_left * cpp_kSize, cpp_right * cpp_kSize - 1);

        return af_imgIn;
    }
    catch (af::exception &e) {
        std::cerr << "Error in CLASS Segmentation METHOD run(af::array &af_imgIn, int cpp_kSize, int cpp_varThreshold)";
        std::cerr << e.what();

        return NULL;
    }
}
