#ifndef GABOR_H
#define GABOR_H

#include "gabor_global.h"

#include "arrayfire.h"

#include "helpers.h"
#include "orientationmap.h"
#include "segmentation.h"

/* Libraries import */
#include <iomanip> //std::precision
#include <iostream>

typedef struct gaborSettings
{
    gaborSettings() {
        cpp_oMap_mode = 0;
        cpp_oMap_kSize = 17;
        cpp_oMap_useGauss = true;
        cpp_oMap_gaussKSize = 5;

        cpp_gabor_kSize = 17;
        cpp_gabor_gamma = 1.0f;
        cpp_gabor_lambda = 8.0f;
        cpp_gabor_psi = 0.0f;
        cpp_gabor_sigma = 4.0f;
    }

    /********************************/
    /* Orientation map */
    /********************************/
    ///
    /// \brief cpp_oMap_mode
    /// Orientation map mode.
    /// 1 - Quailty version, performance fallback
    /// 2 - Less quality version, performance improvement
    int cpp_oMap_mode;

    ///
    /// \brief cpp_oMap_kSize
    /// Size of square kernel in pixels for orientation map computation. Bigger kernel size results in bigger performance fallback.
    ///
    int cpp_oMap_kSize;

    ///
    /// \brief cpp_oMap_useGauss
    /// Whether use filtering with Gauss kernel to get results with fewer anomalies.
    bool cpp_oMap_useGauss;

    ///
    /// \brief cpp_oMap_gaussKSize
    /// Size of square Gauss kernel for filtering.
    int cpp_oMap_gaussKSize;

    /********************************/
    /* Gabor filtering */
    /********************************/
    ///
    /// \brief cpp_gabor_kSize
    /// Size of square kernels for Gabor filtering.
    int cpp_gabor_kSize;

    int cpp_gabor_gamma;
    int cpp_gabor_lambda;
    int cpp_gabor_psi;
    int cpp_gabor_sigma;
} GaborSettings;

class GABORSHARED_EXPORT Gabor
{
public:
    Gabor();
    Gabor(GaborSettings cpp_settings);

    af::array af_imgIn;
    af::array af_imgOut;

    GaborSettings cpp_settings;

    bool cpp_useTiming;
    std::map<std::string, double> timing;

    void run();

    /********************************/
    /* Helpers functions */
    /********************************/
    void printTiming();
    af::array compute_gaborKernels();

    /********************************/
    /* Getter and setters functions */
    /********************************/
    void setAf_imgIn(const af::array&);
    af::array getAf_imgIn();

    af::array getAf_imgOut();
};
#endif // GABOR_H
