#ifndef CONVERTER_H
#define CONVERTER_H

//Project imports
#include "converter_global.h"
#include "imagehelpers.h"

//Libraries imports
#include "arrayfire.h"
#include "opencv2/core/core.hpp"
#include <QImage>

class CONVERTERSHARED_EXPORT Converter
{
public:
    Converter();

    ///
    /// \brief afArray_to_QImage
    /// \param af_in
    /// \param cpp_host_pointer - QImage´s buffer, must exist until QImage is added on output widget
    /// \return
    ///

    static float** afArray_to_cpp2Darray(af::array);

    static QImage afArray_to_QImage(af::array af_in, uchar **cpp_host_pointer);
    static af::array QImage_to_afArray(const QImage img);

    static QImage cvMat_to_QImage(cv::Mat const&,  QImage::Format);
    static cv::Mat QImage_to_cvMat(const QImage, int);

    static af::array cvMat_to_afArray(cv::Mat&);
    static cv::Mat afArray_to_cvMat(af::array);
};

#endif // CONVERTER_H
