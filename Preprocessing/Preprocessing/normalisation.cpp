#include "normalisation.h"

Normalisation::Normalisation()
{

}

af::array Normalisation::run(af::array &af_imgIn)
{
    return af::histEqual( af_imgIn, af::histogram(af_imgIn, 256, 0, 255) );
}
