#include "converter.h"

Converter::Converter() {}

float** Converter::afArray_to_cpp2Darray(af::array af_in)
{
    float **cpp_out = new float*[af_in.dims(0)];
    float *cpp_af_in = af_in.host<float>();

    for (int i = 0; i < af_in.dims(0); i++)
        cpp_out[i] = new float[af_in.dims(1)];

    for (int i = 0; i < af_in.dims(0); i++) {
        for (int j = 0; j < af_in.dims(1); j++)
            cpp_out[i][j] = cpp_af_in[af_in.dims(0)*j + i];
    }

    af::freeHost(cpp_af_in);

    return cpp_out;
}

QImage Converter::afArray_to_QImage(af::array af_in, uchar** cpp_host_pointer)
{
    //deep copy of array
    af_in = af_in.copy();
    af_in = ImageHelpers::normalizeImage(af_in);
    //convert float af::array to uchar af::array
    af_in = af_in.as(u8);

    *cpp_host_pointer = af_in.host<uchar>();

    QTransform qt_t;
    QTransform qt_transform = qt_t.rotate(90);

    QImage qt_image((uchar*)(*cpp_host_pointer), af_in.dims(0), af_in.dims(1), af_in.dims(0), QImage::Format_Grayscale8);
    qt_image = qt_image.mirrored().transformed(qt_transform);

    return qt_image;
}

// converts QImage to af::array directly
af::array Converter::QImage_to_afArray(const QImage img)
{
    int total_length = img.byteCount();
    int bytesPerLine = img.bytesPerLine();
    int imgWidth = img.width();
    int imgHeight = img.height();
    int cnt=0;
    uchar* imgData = (uchar*)img.bits(); // get the raw pixel data;

    uchar * arrayData = nullptr;

    // deal with a special case, when each image row is terminated by \n\n
    if(total_length > imgWidth*imgHeight){
        arrayData = new uchar[imgWidth*imgHeight];
        for(int i=0; i<total_length; i++){
            if(i % bytesPerLine < imgWidth) // skip extra values beyond true image width
                arrayData[cnt++] = imgData[i];
        }
        af::array imgAF = af::array(af::dim4(imgWidth, imgHeight),arrayData).T();
        delete [] arrayData;
        return imgAF;
    }
    // otherwise
    else {
        float* cpp_imgData = new float[imgWidth*imgHeight];

        int counter = 0;
        for (int i = 0; i < imgWidth; i++) {
            for (int j = i; j < imgWidth*imgHeight; j += imgWidth) {
                cpp_imgData[counter] = imgData[j];
                counter++;
            }
        }

        af::array af_out(imgHeight, imgWidth, cpp_imgData);
        return af_out;
    }
}

QImage Converter::cvMat_to_QImage(cv::Mat const &mat, QImage::Format format)
{
    return QImage(mat.data, mat.cols, mat.rows, mat.step, format).copy();
}

cv::Mat Converter::QImage_to_cvMat(const QImage img, int format)
{
    return cv::Mat(img.height(), img.width(), format, const_cast<uchar*>(img.bits()), img.bytesPerLine()).clone();
}

af::array Converter::cvMat_to_afArray(cv::Mat &cv_in)
{
    cv::Mat helper;
    cv::transpose(cv_in, helper);
    return af::array(cv_in.rows, cv_in.cols, helper.data);
}

cv::Mat Converter::afArray_to_cvMat(af::array af_in)
{
    uchar* cpp_data = af_in.as(u8).T().host<uchar>();
    cv::Mat cv_out = cv::Mat(af_in.dims(0), af_in.dims(1), CV_8UC1, cpp_data).clone();

    af::freeHost(cpp_data);

    return cv_out;
}
