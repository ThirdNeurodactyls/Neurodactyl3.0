#include "orientationmap.h"

//#include "helpers.h"

OrientationMap::OrientationMap()
{
}

///
/// @brief OrientationMap::run
/// @param af_in - Arrayfire af::array input image
/// @param cpp_mode - 0 - Block-wise computation of orientation map. Split the image into blocks of rectangular size [kSize; kSize].
/// Then compute the direction of papillary lines in each block. The resultant image size will be [int(af_in.dims(0)/kSize); int(af_in.dims(1)/kSize)].
///                   1 - Pixel-wise computation of orinetation map. Split the image into blocks of rectangular size [kSize; kSize], where pixel is
/// center of block. Only odd numbers are allowed for this method type. The resultant image size will be the same as the input image size.
/// @param cpp_kSize - size of rectangular block of size [kSize; kSize].
/// @param cpp_retInVector - enable if the resultant array will be returned in vector.
/// @param cpp_useSmoothing - enable if to use Gauss filter for smoothing the values between blocks.
/// @param cpp_gauss_kSize - size of rectangular block of size [kSize; kSize]
/// @return
///
af::array OrientationMap::run(af::array af_in, int cpp_mode, int cpp_kSize, bool cpp_retInVector, bool cpp_useSmoothing, int cpp_gauss_kSize)
{
    try {
        af::array af_gx, af_gy,
                af_vx, af_vy,
                af_theta;

        //compute gradients Gx, Gy with horizontal, vertical sobel operator
        af::sobel(af_gx, af_gy, af_in, 3);

        af_vx = 2.0f*af_gx*af_gy;
        af_vy = af_gx*af_gx - af_gy*af_gy;

        if (cpp_mode == 0) {
            //block-wise computation
            af_vx = af::unwrap(af_vx, cpp_kSize, cpp_kSize, cpp_kSize, cpp_kSize);
            af_vy = af::unwrap(af_vy, cpp_kSize, cpp_kSize, cpp_kSize, cpp_kSize);
        }
        else if (cpp_mode == 1) {
            //pixel-wise computation
            af_vx = af::unwrap(af_vx, cpp_kSize, cpp_kSize, 1, 1, cpp_kSize/2, cpp_kSize/2);
            af_vy = af::unwrap(af_vy, cpp_kSize, cpp_kSize, 1, 1, cpp_kSize/2, cpp_kSize/2);
        }

        //af::transpose due to ArrayFire af::sum dim1 restriction for large arrays
        af_vx = af::sum(af::transpose(af_vx), 1);
        af_vy = af::sum(af::transpose(af_vy), 1);

        af_theta = 0.5f*af::atan2(af_vx, af_vy);
        af_theta = af::transpose(af_theta);

        if (cpp_useSmoothing == true) {
            af::array af_phi_x, af_phi_y;

            //moddims due to use of af::convolve2 operation, array need to be in original 2D state
            af_theta = af::moddims(af_theta, af_in.dims(0), af_in.dims(1));

            af_phi_x = af::cos(2*af_theta);
            af_phi_y = af::sin(2*af_theta);

            af_phi_x = af::convolve2(af_phi_x, af::gaussianKernel(cpp_gauss_kSize, cpp_gauss_kSize));
            af_phi_y = af::convolve2(af_phi_y, af::gaussianKernel(cpp_gauss_kSize, cpp_gauss_kSize));

            af_theta = 0.5f*af::atan2(af_phi_y, af_phi_x);
        }

        if (cpp_retInVector == true)
            return af::moddims(af_theta, 1, af_theta.elements());
        else
            return af::moddims(af_theta, af_in.dims(0), af_in.dims(1));
    } catch(af::exception &e) {
        std::cerr << e.what();

        return NULL;
    }
}
