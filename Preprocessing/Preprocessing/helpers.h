#ifndef HELPERS_H
#define HELPERS_H

#include "helpers_global.h"

#include "arrayfire.h"

#include <iostream>

class HELPERSSHARED_EXPORT Helpers
{
public:
    static af::array compute_varianceInBlocks2D(af::array af_imgIn, int cpp_kSize);
    //converts all values in af::array to interval <0,255>

    static void af_arrayInfo(af::array af_in, std::string cpp_varName, bool afPrint = false);
};

#endif // HELPERS_H
