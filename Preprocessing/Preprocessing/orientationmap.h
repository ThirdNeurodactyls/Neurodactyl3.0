#ifndef ORIENTATIONMAP_H
#define ORIENTATIONMAP_H

#include <iostream>
#include "orientationmap_global.h"

#include "arrayfire.h"

class ORIENTATIONMAPSHARED_EXPORT OrientationMap
{
public:
    OrientationMap();


    static af::array run(af::array af_imgIn, int cpp_mode, int cpp_kSize, bool cpp_retInVector = false, bool cpp_useSmoothing = true, int cpp_gaussKSize = 5);
};

#endif // ORIENTATIONMAP_H
