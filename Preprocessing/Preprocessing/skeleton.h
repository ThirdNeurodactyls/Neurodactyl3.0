#ifndef SKELETON_H
#define SKELETON_H

#include "opencv2/core/core.hpp"

#include "skeleton_global.h"

class SKELETONSHARED_EXPORT Skeleton
{
public:
    Skeleton();
    static cv::Mat run(cv::Mat& cv_imgIn);

    static void ThinSubiteration1(cv::Mat&, cv::Mat&);
    static void ThinSubiteration2(cv::Mat&, cv::Mat&);
    static void CreateSkeleton(cv::Mat&, cv::Mat&);
};

#endif // SKELETON_H
