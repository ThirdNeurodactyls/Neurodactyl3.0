#include "imagehelpers.h"

ImageHelpers::ImageHelpers()
{
}

af::array ImageHelpers::binarizeImage(af::array af_in, int cpp_bin_threshold)
{
    af::array af_out(af_in.dims(0), af_in.dims(1));
    af::replace(af_out, af_in > cpp_bin_threshold, 0);
    af::replace(af_out, af_in <= cpp_bin_threshold, 255);


    return af_out;
}

af::array ImageHelpers::invertImage(af::array af_in)
{
    af::array af_out(af_in.dims(0), af_in.dims(1));
    af::replace(af_out, af_in == 0, 0);
    af::replace(af_out, af_in == 255, 255);

    return af_out;
}

af::array ImageHelpers::normalizeImage(af::array af_in)
{
    //deep copy of array
    af_in = af_in.copy();

    float cpp_min = af::min<float>(af_in);
    float cpp_max = af::max<float>(af_in);
    af_in = 255.0f * (af_in - cpp_min) / (cpp_max - cpp_min);

    return af_in;
}
