#include "gabor.h"

Gabor::Gabor()
{
    this->cpp_useTiming = true;
}

Gabor::Gabor(GaborSettings cpp_settings)
{
    this->cpp_useTiming = true;
    this->cpp_settings = cpp_settings;
}

void Gabor::run()
{
    try {
        af::sync();
        af::timer overall = af::timer::start();

        /********************************/
        /* Image preparation */
        /********************************/
        af::sync();
        af::timer imagePrep = af::timer::start();

        //        Helpers::af_arrayInfo(this->af_imgIn, "af_imgIn");
        this->af_imgOut = this->af_imgIn.copy();
        //        Helpers::af_arrayInfo(this->af_imgOut, "af_imgOut");

        //Input image dimensions
        int cpp_imgHeight = af_imgIn.dims(0),
                cpp_imgWidth = af_imgIn.dims(1);

        //        std::cerr << "cpp_imgHeight: " << cpp_imgHeight << std::endl
        //                  << "cpp_imgWidth: " << cpp_imgWidth << std::endl;

        this->af_imgOut = af::unwrap(this->af_imgOut, this->cpp_settings.cpp_gabor_kSize, this->cpp_settings.cpp_gabor_kSize, 1, 1, this->cpp_settings.cpp_gabor_kSize/2, this->cpp_settings.cpp_gabor_kSize/2);
        //        Helpers::af_arrayInfo(this->af_imgOut, "af_imgOut");

        af::sync();
        this->timing["Image preparation"] = af::timer::stop(imagePrep);
        /********************************/
        /* Gabor kernels computation */
        /********************************/
        af::sync();
        af::timer gKernels = af::timer::start();

        af::array gaborKernels = this->compute_gaborKernels();

        af::sync();
        this->timing["Gabor kernels"] = af::timer::stop(gKernels);
        /********************************/
        /* Convolution with Gabor kernels */
        /********************************/
        af::sync();
        af::timer convolution = af::timer::start();

        this->af_imgOut = this->af_imgOut * gaborKernels;
        //        Helpers::af_arrayInfo(this->af_imgOut, "af_imgOut");

        this->af_imgOut = af::sum(this->af_imgOut.T(), 1);
        //        Helpers::af_arrayInfo(this->af_imgOut, "af_imgOut");

        this->af_imgOut = af::moddims(this->af_imgOut.T(), cpp_imgHeight, cpp_imgWidth);

        af::sync();
        this->timing["Convolution"] = af::timer::stop(convolution);

        //convert back to U8
        if (!this->af_imgOut.type() == u8)
            this->af_imgOut = af_imgOut.as(u8);

        af::sync();
        this->timing["Overall"] = af::timer::stop(overall);

        if (this->cpp_useTiming)
            this->printTiming();
    }
    catch (af::exception &e) {
        std::cerr << "Error in CLASS Gabor METHOD run()";
        std::cerr << e.what();
    }
}

void Gabor::printTiming()
{
    std::cerr << std::setprecision(2);
    std::cerr << "GABOR OPERATIONS DURATION: " << std::endl;

    std::cerr << "Image preparation: "
              << this->timing["Image preparation"]
              << " " << this->timing["Image preparation"]/this->timing["Overall"] * 100 << "%"
              << std::endl;

    std::cerr << "Gabor kernels computation: "
              << this->timing["Gabor kernels"]
              << " " << this->timing["Gabor kernels"]/this->timing["Overall"] * 100 << "%"
              << std::endl;

    std::cerr << "Convolution: "
              << this->timing["Convolution"]
              << " " << this->timing["Convolution"]/this->timing["Overall"] * 100 << "%"
              << std::endl;

    std::cerr << "OVERALL: "
              << this->timing["Overall"]
              << std::endl;

    std::cerr << std::endl;
}

af::array Gabor::compute_gaborKernels()
{
    try {
        af::array af_loopI, af_loopJ,
                af_kernel,
                af_X, af_Y;

        //        Helpers::af_arrayInfo(this->af_imgIn, "this->af_imgIn");
        af::array af_thetaRadians = OrientationMap::run(this->af_imgIn, 1, this->cpp_settings.cpp_oMap_kSize, true, false);
        //        Helpers::af_arrayInfo(af_thetaRadians, "af_thetaRadians");

        af_thetaRadians = af_thetaRadians + af::Pi/2;

        //double loop simulation, cycleI - outer loop, cycleJ - inner loop
        af_loopI = af::tile(af::seq(-this->cpp_settings.cpp_gabor_kSize/2, this->cpp_settings.cpp_gabor_kSize/2), this->cpp_settings.cpp_gabor_kSize);
        af_loopJ = af::moddims(af::tile(af::transpose(af::seq(-this->cpp_settings.cpp_gabor_kSize/2, this->cpp_settings.cpp_gabor_kSize/2)), this->cpp_settings.cpp_gabor_kSize, 1), this->cpp_settings.cpp_gabor_kSize * this->cpp_settings.cpp_gabor_kSize, 1);

        af_loopI = af::tile(af_loopI, 1, af_thetaRadians.elements());
        af_loopJ = af::tile(af_loopJ, 1, af_thetaRadians.elements());

        af_thetaRadians = af::tile(af_thetaRadians, this->cpp_settings.cpp_gabor_kSize * this->cpp_settings.cpp_gabor_kSize);

        af_X = -af_loopJ * af::sin(af_thetaRadians) + af_loopI * af::cos(af_thetaRadians);
        af_Y = af_loopJ * af::cos(af_thetaRadians) + af_loopI * af::sin(af_thetaRadians);

        af_kernel = af::exp(-(af_X * af_X + pow(this->cpp_settings.cpp_gabor_gamma * af_Y, 2)) / (2.0f * this->cpp_settings.cpp_gabor_sigma * this->cpp_settings.cpp_gabor_sigma)) * cos((2.0f * af::Pi / this->cpp_settings.cpp_gabor_lambda * af_X) + this->cpp_settings.cpp_gabor_psi);
        //        Helpers::af_arrayInfo(af_kernel, "af_kernel");

        return af_kernel;
    }
    catch (af::exception &e) {
        std::cerr << "Error in CLASS Gabor METHOD af::array Gabor::compute_gaborKernels()";
        std::cerr << e.what();

        return NULL;
    }
}

//Getters and setters
void Gabor::setAf_imgIn(const af::array &value) {
    try {
        if (value.type() == u8)
            this->af_imgIn = value.copy();
        else
            this->af_imgIn = value.copy().as(u8);
    }
    catch(af::exception &e) {
        std::cerr << "Error in CLASS Gabor METHOD void Gabor::setAf_imgIn(const af::array &value)";
        std::cerr << e.what();
    }
}

af::array Gabor::getAf_imgIn()
{
    try {
        return this->af_imgIn;
    }
    catch(af::exception &e) {
        std::cerr << "Error in CLASS Gabor METHOD af::array Gabor::getAf_imgIn()";
        std::cerr << e.what();
    }
}

af::array Gabor::getAf_imgOut()
{
    try {
        return this->af_imgOut;
    }
    catch(af::exception &e) {
        std::cerr << "Error in CLASS Gabor METHOD af::array Gabor::getAf_imgOut()";
        std::cerr << e.what();
    }
}
