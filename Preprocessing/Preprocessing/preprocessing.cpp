#include "preprocessing.h"

Preprocessing::Preprocessing()
{
    this->cpp_useTiming = true;
    g = new Gabor();
}

Preprocessing::Preprocessing(Settings cpp_settings)
{
    this->cpp_useTiming = true;
    this->cpp_settings = cpp_settings;

    /* Gabor settings structure */
    GaborSettings cpp_gaborSettings;

    /* Orientation map */
    cpp_gaborSettings.cpp_oMap_gaussKSize = cpp_settings.cpp_oMap_gaussKSize;
    cpp_gaborSettings.cpp_oMap_kSize = cpp_settings.cpp_oMap_kSize;
    cpp_gaborSettings.cpp_oMap_mode = cpp_settings.cpp_oMap_mode;
    cpp_gaborSettings.cpp_oMap_useGauss = cpp_settings.cpp_oMap_useGauss;

    /* Gabor settings */
    cpp_gaborSettings.cpp_gabor_kSize = cpp_settings.cpp_gabor_kSize;
    cpp_gaborSettings.cpp_gabor_gamma = cpp_settings.cpp_gabor_gamma;
    cpp_gaborSettings.cpp_gabor_lambda = cpp_settings.cpp_gabor_lambda;
    cpp_gaborSettings.cpp_gabor_psi = cpp_settings.cpp_gabor_psi;
    cpp_gaborSettings.cpp_gabor_sigma = cpp_settings.cpp_gabor_sigma;

    g = new Gabor(cpp_gaborSettings);
}

void Preprocessing::run()
{   
    try {
        af::sync();
        af::timer overall = af::timer::start();

        /********************************/
        /* Prevent input image mutation */
        /********************************/

        this->af_imgOut = this->af_imgIn.copy();
        //  Helpers::af_arrayInfo(this->af_imgIn, "af_imgIn");
        //  Helpers::af_arrayInfo(this->af_imgOut, "af_imgOut");

        /********************************/
        /* Histogram Equalization */
        /********************************/

        if (this->cpp_settings.cpp_use_histEqual) {
            af::sync();
            af::timer histEqual = af::timer::start();

            this->af_imgOut = Normalisation::run(this->af_imgOut);
            //  Helpers::af_arrayInfo(this->af_imgOut, "af_imgOut histEqual");

            af::sync();
            this->timing["Histogram equalization"] = af::timer::stop(histEqual);
        }

        /********************************/
        /* Segmentation */
        /********************************/
        if (this->cpp_settings.cpp_use_segm) {
            af::sync();
            af::timer segm = af::timer::start();

            this->af_imgOut = Segmentation::run(this->af_imgOut, this->cpp_settings.cpp_segm_kSize, this->cpp_settings.cpp_segm_varThreshold);
            //  Helpers::af_arrayInfo(this->af_imgOut, "af_imgOut segm");

            af::sync();
            this->timing["Segmentation"] = af::timer::stop(segm);
        }

        /********************************/
        /* Gabor filtering */
        /********************************/

        af::sync();
        af::timer gabor = af::timer::start();

        this->g->setAf_imgIn(this->af_imgIn);
        this->g->run();

        this->af_imgOut = this->g->getAf_imgOut();
        //  Helpers::af_arrayInfo(this->af_imgOut, "af_imgOut Gabor");

        af::sync();
        this->timing["Gabor filtering"] = af::timer::stop(gabor);

        /********************************/
        /* Binarization */
        /********************************/

        if (this->cpp_settings.cpp_use_binarization) {
            af::sync();
            af::timer binarization = af::timer::start();

            this->af_imgOut = ImageHelpers::binarizeImage(this->af_imgOut, this->cpp_settings.cpp_bin_threshold);
            //  Helpers::af_arrayInfo(this->af_imgOut, "af_imgOut binarization");

            af::sync();
            this->timing["Binarization"] = af::timer::stop(binarization);
        }

        /********************************/
        /* Skeletonization */
        /********************************/

        if (this->cpp_settings.cpp_use_skeletonization) {
            af::sync();
            af::timer skeleton = af::timer::start();

            this->af_imgOut = ImageHelpers::invertImage(this->af_imgOut);

            cv::Mat cv_img = Converter::afArray_to_cvMat(this->af_imgOut);

            cv_img = Skeleton::run(cv_img);
            cv::threshold(cv_img, cv_img, 0, 255, cv::THRESH_BINARY_INV);

            cv_img = this->removeFilterArtifacts(cv_img);

            //convert to 1 channel U8 grayscale image
            cv_img.convertTo(cv_img, CV_8UC1);

            this->af_imgOut = Converter::cvMat_to_afArray(cv_img);

            af::sync();
            this->timing["Skeletonization"] = af::timer::stop(skeleton);
        }
        af::sync();
        this->timing["Overall"] = af::timer::stop(overall);

        if (this->cpp_useTiming)
            this->printTiming();
    }
    catch (af::exception &e) {
        std::cerr << "Error in CLASS Preprocessing METHOD void Preprocessing::run()";
        std::cerr << e.what();
    }
}


/********************************/
/* Helpers */
/********************************/
void Preprocessing::printTiming()
{
    std::cerr << std::setprecision(2);
    std::cerr << "OPERATIONS DURATION: " << std::endl;

    if (this->cpp_settings.cpp_use_segm)
        std::cerr << "Segmentation: "
                  << this->timing["Segmentation"]
                  << " " << this->timing["Segmentation"]/this->timing["Overall"] * 100 << "%"
                                                                                       << std::endl;

    if (this->cpp_settings.cpp_use_histEqual)
        std::cerr << "Histogram Equalization: "
                  << this->timing["Histogram equalization"]
                  << " " << this->timing["Histogram equalization"]/this->timing["Overall"] * 100 << "%"
                                                                                                 << std::endl;

    std::cerr << "Gabor filtering: "
              << this->timing["Gabor filtering"]
              << " " << this->timing["Gabor filtering"]/this->timing["Overall"] * 100 << "%"
                                                                                      << std::endl;

    if (this->cpp_settings.cpp_use_binarization)
        std::cerr << "Binarization: "
                  << this->timing["Binarization"]
                  << " " << this->timing["Binarization"]/this->timing["Overall"] * 100 << "%"
                                                                                       << std::endl;

    if (this->cpp_settings.cpp_use_skeletonization)
        std::cerr << "Skeletonization: "
                  << this->timing["Skeletonization"]
                  << " " << this->timing["Skeletonization"]/this->timing["Overall"] * 100 << "%"
                                                                                          << std::endl;

    std::cerr << "OVERALL: "
              << this->timing["Overall"]
              << std::endl;

    std::cerr << std::endl;
}

cv::Mat Preprocessing::removeFilterArtifacts(cv::Mat cv_img)
{
    //replace left side filter artifacts
    cv::Rect roi(0, 0, this->cpp_settings.cpp_gabor_kSize/2, cv_img.rows);
    cv_img(roi) = 255;

    //replace upper side filter artifacts
    roi.width = cv_img.cols;
    roi.height = this->cpp_settings.cpp_gabor_kSize/2;

    cv_img(roi) = 255;

    //replace right side filter artifacts
    roi.x = cv_img.cols - this->cpp_settings.cpp_gabor_kSize/2;
    roi.width = this->cpp_settings.cpp_gabor_kSize/2;
    roi.height = cv_img.rows;

    cv_img(roi) = 255;

    //replace bottom side filter artifacts
    roi.x = 0;
    roi.y = cv_img.rows - this->cpp_settings.cpp_gabor_kSize/2;
    roi.width = cv_img.cols;
    roi.height = this->cpp_settings.cpp_gabor_kSize/2;

    cv_img(roi) = 255;

    return cv_img;
}

/********************************/
/* Getter and setters functions */
/********************************/
af::array Preprocessing::getAf_imgIn() const
{
    return af_imgIn;
}

void Preprocessing::setAf_imgIn(const af::array &value)
{
    try {
        if (value.type() == u8)
            this->af_imgIn = value.copy();
        else
            this->af_imgIn = value.copy().as(u8);
    }
    catch(af::exception &e) {
        std::cerr << "Error in CLASS Preprocessing METHOD void Preprocessing::setAf_imgIn(const af::array &value)";
        std::cerr << e.what();
    }
}

af::array Preprocessing::getAf_imgOut() const
{
    return af_imgOut;
}
