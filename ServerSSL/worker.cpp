#include "worker.h"

Worker::Worker(qintptr ID, QMutex* mutex)
{
    this->socketDescriptor = ID;
    this->mutex = mutex;
}

Worker::~Worker() {}

void Worker::process()
{   
    qDebug() << THREAD_STARTED;

    if (NULL == serverSocket)static const QString REGISTER_IMAGE = "REGISTER_IMAGE";
    static const QString FIND_IMAGE = "FIND_IMAGE";
    static const QString LOGIN_IMAGE = "LOGIN_IMAGE";
    {
        serverSocket = new QSslSocket();

        if(!serverSocket->setSocketDescriptor(this->socketDescriptor))
        {
            qDebug() << SETTING_DESCRIPTOR_FAILED;
            return;
        }

        serverSocket->setLocalCertificate(CERTIFICATE_PATH);
        serverSocket->setPrivateKey(PRIVATE_KEY_PATH);
        serverSocket->setProtocol(QSsl::TlsV1_2OrLater);
        serverSocket->startServerEncryption();
    }

    connect(serverSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()), Qt::DirectConnection);
    connect(serverSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
}

void Worker::onDisconnectClient(qintptr descriptor)
{
    if (descriptor == this->socketDescriptor || descriptor == -1)
    {
        if (NULL != this->serverSocket && this->serverSocket->isOpen())
        {
            this->serverSocket->close();
        }
        emit finished();
    }
}

void Worker::onReadyRead()
{
    if (!IMAGE_READ_MODE)
        decideContent();
    else
        imageRead();
}

void Worker::onDisconnected()
{
    qDebug() << "Client " << socketDescriptor << " Disconnected";

    emit clientDisconnected(socketDescriptor);
    this->serverSocket->deleteLater();
    emit finished();
}

void Worker::onLogInStatus(qintptr desc, int id)
{
    if (this->socketDescriptor == desc)
    {
        this->id = id;
        QString output = id != UNKNOWN_DUMMY_ID ? (id != NEW_DUMMY_ID ? ( id != INACTIVE_DUMMY_ID ? LOGIN_SUCCESS : LOGIN_INACTIVE) : LOGIN_NEW) : LOGIN_FAILED;
        this->serverSocket->write(output.toStdString().c_str());
    }
}

void Worker::onRegistrationStatus(qintptr desc, bool status, bool finger)
{
    if (this->socketDescriptor == desc)
    {
        QString output;
        if (!finger)
        {
            output = status ? REGISTER_SUCCESS : REGISTER_FAILED;
        }
        else
        {
            output = status ? FINGER_REGISTER_SUCCESS : FINGER_REGISTER_FAILED;
            emitStatus(output);
            this->xyttmp = "";
        }
        this->serverSocket->write(output.toStdString().c_str());
    }
}

void Worker::decideContent()
{
    QString message = serverSocket->readLine();
    message.chop(1);

    QString output;

    if (QString::compare(message, TEXT_MESSAGE) == 0)
    {
        manageTextContent();
        output = TEXT_HANDLED;
    }
    else if (QString::compare(message, IMAGE_MESSAGE) == 0)
    {
        manageImageContent();
        output = IMAGE_HANDLED;
    }
    else if (QString::compare(message, REGISTRATION) == 0)
    {
        manageRegistration();
    }
    else if (QString::compare(message, LOGIN) == 0)
    {
        manageLogin();
    }
    else
    {
        output = MESSAGE_NOT_RECOGNIZED;
    }

    if (!output.isEmpty())
    {
        serverSocket->write(output.toStdString().c_str());
    }
}

void Worker::manageTextContent()
{
    emit guiTextUpdate(this->id, this->username, serverSocket->readAll());
}

void Worker::manageImageContent()
{
    IMAGE_READ_MODE = true;
    imageArray = new QByteArray();

    this->imageType = serverSocket->readLine();
    this->imageType.chop(1);

    if (QString::compare(LOGIN_IMAGE, this->imageType) == 0)
    {
        this->username = serverSocket->readLine();
        this->username.chop(1);
    }

    QString size = serverSocket->readLine();
    size.chop(1);

    imageSize = size.toInt();

    this->imageRead();
}

void Worker::imageRead()
{
    imageArray->append(serverSocket->readAll());
    if (imageArray->size() >= imageSize)
        manageFinishedImage();
}

void Worker::manageFinishedImage()
{
    QImage img;
    img.loadFromData(*imageArray, IMAGE_RECEIVE_FORMAT.toStdString().c_str());

    IMAGE_READ_MODE = false;
    if (!img.isNull())
    {
        QPixmap pix = QPixmap::fromImage(img);
        emit guiImageShow(pix.scaled(LABEL_WIDTH,LABEL_HEIGHT, Qt::KeepAspectRatio));
        emitStatus(IMAGE_RECEIVED+": "+img.text(IMAGE_DATA_NAME));

        if (img.width() > 500 || img.height() > 500)
        {
            QString text = IMAGE_TOO_BIG+": "+img.text(IMAGE_DATA_NAME);
            emitStatus(text);
            serverSocket->write(text.toStdString().c_str());
        }
        else
        {
            QImage* skeleton = processImage(img);
            if (skeleton != NULL && !skeleton->isNull())
            {
                this->xyttmp = extractMarkants(skeleton, img);
                if (QString::compare(this->xyttmp, "") != 0)
                {
                    if (QString::compare(REGISTER_IMAGE, this->imageType) == 0)
                    {
                        emit registerFingerpreint(this->socketDescriptor, this->id, this->xyttmp);
                    }
                    else if (QString::compare(LOGIN_IMAGE, this->imageType) == 0)
                    {
                        emit requestLoginData(this->socketDescriptor, this->username);
                    }
                    else if (QString::compare(FIND_IMAGE, this->imageType) == 0)
                    {
                        emit requestAllData(this->socketDescriptor);
                    }
                    else {
                        emitStatus(IMAGE_TYPE_UNKNOWN);
                    }
                }
                else
                {
                    emitStatus(EXTRACTION_FAIL);
                }
            }
        }
    }
    else
    {
        emitStatus(IMAGE_CORRUPTED);
    }
}

QString Worker::extractMarkants(QImage *skeleton, QImage original)
{
    Extraction* e = new Extraction();
    e->setKSize(EXTRACTION_SIZE);
    af::array arr = ImageHelpers::normalizeImage(Converter::QImage_to_afArray(*skeleton));
    e->setCv_imgIn(Converter::afArray_to_cvMat(arr));
    arr = Converter::QImage_to_afArray(original);
    e->setCv_imgInOrig(Converter::afArray_to_cvMat(arr));

    std::vector<std::pair<cv::Mat, std::string>> vector = e->run();

    QStringList name_list;
    QStringList coordinates;

    for (unsigned int i = 0; i < vector.size(); i++) {
        QString name = MARKANT_NAME + QString::number(this->socketDescriptor)+ "-" + QString::number(i) + MARKANT_TYPE;
        cv::imwrite(name.toStdString(), vector[i].first);

        name_list << name;
        coordinates << QString::fromStdString(vector[i].second);
    }

    QString output = neuralIdentify(name_list);

    for (auto n : name_list)
    {
        QFile::remove(n);
    }
    return buildXYT(output, coordinates);
}

QString Worker::neuralIdentify(const QStringList &name_list)
{
    QString exe(LOCAL_PYTHON_ENV + " " + SCRIPT_PATH + " --graph=" + MODEL_PATH +
                " --labels=" + LABEL_PATH + " --input_layer=" + INPUT_LAYER + " --output_layer=" + OUTPUT_LAYER + " --path=" + MARCANT_PATH +
                " --input_height=" + IMG_WIDTH + " --input_width=" + IMG_HEIGHT + " --image=");

    QString names = "";

    for (int i = 0; i < name_list.length(); i++)
    {
        names += name_list[i] + (i < name_list.length()-1 ? PYTHON_DELIM : "");
    }

    QString output = this->executePythonScript(exe+names);
    output.replace("\n", "");

    //qDebug() << output;

    return output;
}

QString Worker::buildXYT(const QString &data, const QStringList& coords)
{
    QStringList results = data.split(NEURAL_DELIM);
    results.removeAt(0);
    QString xyt = "";

    for (int i = 0; i < results.length(); i++)
    {
        QStringList mp = results[i].split(NEURAL_ENTRY_DELIM);
        QStringList xy = coords[i].split(COORDS_DELIM);
        if (!(QString::compare(mp[0], IGNORE_CLASS) == 0) && mp[1].toFloat() > PROB_TRESHOLD)
        {
            int angle = floor((xy[2].toFloat()*180/M_PI) < 0 ? (xy[2].toFloat()*180/M_PI)+180 : (xy[2].toFloat()*180/M_PI));
            xyt += xy[0] + NEURAL_ENTRY_DELIM + xy[1] + NEURAL_ENTRY_DELIM + QString::number(angle) + XYT_DELIM;
        }
    }
    return xyt;
}

bool Worker::bozorthCompare(const QString& one, const QList<QString>& others)
{
    bool valid = false;
    QString dummy_f = QString::number(this->socketDescriptor) + XYT_FILENAME;
    if (makeFile(one, dummy_f))
    {
        for (int i = 0; i < others.length(); i++)
        {
            QString dummy_x = QString::number(i)+ "-" + QString::number(this->socketDescriptor) + XYT_FILENAME;
            if(makeFile(others[i], dummy_x))
            {
                QString output = executeProcees(BOZ_PATH, QStringList() << "-n" << "200" << dummy_f << dummy_x);
                output.replace("\n", "");
                qDebug() << "Bozorth: " << output;
                if (output.toInt() >= BOZ_TRESHOLD) {
                    valid = true;
                    break;
                }
                QFile::remove(dummy_x);
            }
            else
            {
                emitStatus(BOZ_FILE_PROBLEM);
                break;
            }
        }
        QFile::remove(dummy_f);
    }
    else
    {
        emitStatus(BOZ_FILE_PROBLEM);
    }
    return valid;
}

bool Worker::makeFile(const QString &data, const QString &filename)
{
    QFile f(filename);
    if (f.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&f);
        stream << data;
        f.close();
        return true;
    }
    else
    {
        return false;
    }
}

QString Worker::executeProcees(QString program, QStringList args)
{
    QProcess* identify = new QProcess(this);
    identify->start(program, args);
    identify->waitForFinished(-1);
    return identify->readAllStandardOutput();
}

QString Worker::executePythonScript(QString exe)
{
    QProcess process;
    process.setProcessChannelMode(QProcess::MergedChannels);
    process.start(exe);
    process.waitForFinished(-1);
    return process.readAllStandardOutput();
}

void Worker::manageRegistration()
{
    QString user(serverSocket->readLine());
    user = user.left(user.length()-1);
    QString hash(serverSocket->readLine());
    emit registration(this->socketDescriptor, user, hash);
}

void Worker::manageLogin()
{
    QString user(serverSocket->readLine());
    user = user.left(user.length()-1);
    QString hash(serverSocket->readLine());
    this->username = user;
    emit logIn(this->socketDescriptor, user, hash);
}

QImage* Worker::processImage(const QImage& img)
{
    Preprocessing* p = new Preprocessing();
    p->setAf_imgIn(Converter::QImage_to_afArray(img));
    p->run();
    QImage* processed_img = new QImage(Converter::afArray_to_QImage(p->getAf_imgOut(), &this->imgBuffer));
    if (!processed_img->isNull())
    {
        QPixmap pix = QPixmap::fromImage(*processed_img);
        emit guiFilteredShow(pix.scaled(LABEL_WIDTH,LABEL_HEIGHT, Qt::KeepAspectRatio));
        emitStatus(IMAGE_PROCESSED);
        serverSocket->write(IMAGE_PROCESSED.toStdString().c_str());

        return processed_img;
    }
    else
    {
        emitStatus(IMAGE_NOT_PROCESSED);
        serverSocket->write(IMAGE_NOT_PROCESSED.toStdString().c_str());

        return NULL;
    }
}

void Worker::emitStatus(const QString &message)
{
    emit sendStatusMessage(this->id, this->username, message);
}

void Worker::verifyLogin(qintptr desc, const QList<QString>& data)
{
    if (this->socketDescriptor == desc)
    {
        int id = data.first().toInt();
        QString output = id != UNKNOWN_DUMMY_ID ? (id != NEW_DUMMY_ID ? ( id != INACTIVE_DUMMY_ID ? LOGIN_SUCCESS : LOGIN_INACTIVE) : LOGIN_NEW) : LOGIN_FAILED;
        if (QString::compare(output, LOGIN_SUCCESS) == 0 && !bozorthCompare(this->xyttmp, QList<QString>(data.mid(1))))
        {
            output = LOGIN_FAILED;
        }
        if (QString::compare(output, LOGIN_SUCCESS) == 0)
        {
            this->id = id;
            emit loginSuccess(this->socketDescriptor, this->id, this->username);
        }
        this->serverSocket->write(output.toStdString().c_str());
        //qDebug() << this->xyttmp;
        this->xyttmp = "";
    }
}

void Worker::findUserByFingerprint(qintptr desc, const QList<QList<QString>> &data)
{
    if (this->socketDescriptor == desc)
    {
        QString output = NO_MATCH;
        for (auto l : data)
        {
            if (bozorthCompare(this->xyttmp, QList<QString>() << l.last()))
            {
                QString output = "Fingerprint belongs to user" + l[1] + " with id " + l.first();
                break;
            }
        }
        emitStatus(output);
        this->serverSocket->write(output.toStdString().c_str());
        this->xyttmp = "";
    }
}
