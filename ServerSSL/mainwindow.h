#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSslSocket>
#include <QTcpServer>
#include <QTcpSocket>
#include <QBuffer>
#include <QImage>
#include <QPixmap>
#include <QVariant>
#include <QIcon>
#include <qglobal.h>
#include <QDebug>
#include <QPair>

#include "server.h"
#include "logger.h"

namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class - represents main window of server application
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    /**
     * @brief Logger* logger - instance of logger class used in logging of server activity
     */
    Logger* logger;

public:
    /**
     * @brief MainWindow - constructor of MainWindow class that sets up app window, cretes logger instance and connects signals from instance of Server class
     * @param QWidget* parent
     */
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

private slots:
    /**
     * @brief log_message - writes message to debug console and status bar of application
     * @param const QString& message
     */
    void log_message(const QString& message);

    /**
     * @brief onServerStarted - calls log_message() method with ingormations server startup
     * @param cont QString& host - IP address or hostname of server
     * @param bool started - true if server started successuflly
     * @param int port - port where server is listetning
     */
    void onServerStarted(bool started, const QString& host, int port);

    /**
     * @brief onServerStopped - logs server stopped message
     */
    void onServerStopped();

    /**
     * @brief onUiTextUpdate - update certain text field with message from client including informations about client
     * @param int id - id of client in database
     * @param const QString& name - username of client
     * @param const QByteArray& arr - message to show
     */
    void onUiTextUpdate(int id, const QString& name, const QByteArray& arr);

    /**
     * @brief onUiImageShow - shows original image received from client in certain label in MainWindow
     * @param QPixmap img - image to be shown
     */
    void onUiImageShow(QPixmap img);

    /**
     * @brief onUiFilteredShow - shows filtered image, that is skeleton of image in certain label in Main Window
     * @param QPixmap img - processed image to be shown
     */
    void onUiFilteredShow(QPixmap img);

    /**
     * @brief onClientsChanged - reloads list of logged clients in MainWindow with new data
     * @param const QMap<qintptr, QPair<int, QString>>& map - new list of clients including socket descriptor, database id and username
     */
    void onClientsChanged(const QMap<qintptr, QPair<int, QString>>& map);

    /**
     * @brief onSendNewClients - reloads list of users in database diplayed in MainWindow, usually loaded on server start, adition of new user or changing of user status
     * @param const QList<QList<QString>>& clients - list of user data
     */
    void onSendNewClients(const QList<QList<QString>>& clients);

    /**
     * @brief onUpdateClient - calls updateClient() method of Server class to change status of selected user
     * @param const QString& status - new status to be update to database
     */
    void onUpdateClient(const QString& status);

    /**
     * @brief on_startServer_clicked - calls startServer() method of Server class to create connection
     */
    void on_startServer_clicked();

    /**
     * @brief on_stopServer_clicked - calls sendStopServer() method of Server class to stop server communication
     */
    void on_stopServer_clicked();

    /**
     * @brief on_disconnectClient_clicked - calls disconnectClient() method of Server class on selected client if list of clients contains at least one entry
     */
    void on_disconnectClient_clicked();

    /**
     * @brief on_disconnectAllClients_clicked - calls disconnectClient() method of Server class with special identificator to disconnect all clients
     */
    void on_disconnectAllClients_clicked();

    /**
     * @brief on_activateClient_clicked - calls onUpdateClient() method with status 'active'
     */
    void on_activateClient_clicked();

    /**
     * @brief on_deactivateClient_clicked - calls onUpdateClient() method with status 'inactive'
     */
    void on_deactivateClient_clicked();

private:
    /**
     * @brief Ui::MainWindow ui - instance of Ui::MainWindow containing poiters to all window components
     */
    Ui::MainWindow *ui;

    /**
     * @brief Server server - instance of Server class
     */
    Server server;
};

#endif // MAINWINDOW_H
