#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->ipEdit->appendPlainText(HOST_NAME);

    this->logger = new Logger(0, LOG_FILE_NAME, ui->logEdit);

    connect(&server, SIGNAL(serverStarted(bool, const QString&, int)), this, SLOT(onServerStarted(bool, const QString&, int)));
    connect(&server, SIGNAL(serverStopped()), this, SLOT(onServerStopped()));
    connect(&server, SIGNAL(uiTextUpdate(int,const QString&,const QByteArray&)), this, SLOT(onUiTextUpdate(int,const QString&,const QByteArray&)));
    connect(&server, SIGNAL(uiImageShow(QPixmap)), this, SLOT(onUiImageShow(QPixmap)));
    connect(&server, SIGNAL(showStatusMessage(QString)), this, SLOT(log_message(QString)));
    connect(&server, SIGNAL(uiFilteredShow(QPixmap)), this, SLOT(onUiFilteredShow(QPixmap)));
    connect(&server, SIGNAL(clientsChanged(const QMap<qintptr, QPair<int, QString>>&)), this, SLOT(onClientsChanged(const QMap<qintptr, QPair<int, QString>>&)));
    connect(&server, SIGNAL(sendNewClients(const QList<QList<QString>>&)), this, SLOT(onSendNewClients(const QList<QList<QString>>&)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::log_message(const QString& message)
{
    logger->write(message);
    ui->statusBar->showMessage(message);
}

/* UI events */

void MainWindow::on_startServer_clicked()
{
    server.startServer(ui->ipEdit->toPlainText(), PORT_NUMBER);
}

void MainWindow::on_stopServer_clicked()
{
    server.sendStopServer();
}

void MainWindow::on_disconnectClient_clicked()
{
    if (ui->clientList->currentText() != "")
    {
        server.disconnectClient(ui->clientList->currentText().split(" ")[0].toInt());
    }
    else
    {
        log_message(NO_CLIENTS);
    }
}

void MainWindow::on_disconnectAllClients_clicked()
{
    if (ui->clientList->currentText() != "")
    {
        server.disconnectClient(-1);
    }
    else
    {
        log_message(NO_CLIENTS);
    }
}

/* Signal methods */

void MainWindow::onServerStarted(bool started, const QString& host, int port)
{
    if (started)
    {
        log_message(SERVER_STARTED + host + ":" + QString::number(port));
    }
    else
    {
        log_message(SERVER_NOT_STARTED + host + ":" + QString::number(port));
    }

}

void MainWindow::onServerStopped()
{
    log_message(SERVER_STOPPED_SUCCESS);
}

void MainWindow::onUiTextUpdate(int id, const QString& name, const QByteArray& arr)
{
    QString text = "["+QString::number(id)+"]:["+name+"]:"+MESSAGE_TYPE+": "+arr;
    ui->plainTextEdit->appendPlainText(text);
    log_message(text);
}

void MainWindow::onUiImageShow(QPixmap pix)
{
    ui->original->setPixmap(pix);
}

void MainWindow::onUiFilteredShow(QPixmap pix)
{
    ui->filtered->setPixmap(pix);
}

void MainWindow::onSendNewClients(const QList<QList<QString>>& clients)
{
    ui->newClientList->clear();
    for (auto l : clients)
    {
        ui->newClientList->addItem(l[0] + " " + l[1] + " " + l[2]);
    }
}

void MainWindow::onClientsChanged(const QMap<qintptr, QPair<int, QString>>& map)
{
    ui->clientList->clear();
    for (auto c : map.toStdMap())
    {
        ui->clientList->addItem(QString::number(c.first) + " " + (c.second.first != -1 ? "[" + QString::number(c.second.first) + "] " : "") + c.second.second);
    }
}

void MainWindow::onUpdateClient(const QString &status)
{
    if (ui->newClientList->currentText() != "")
    {
        QList<QString> parsed = ui->newClientList->currentText().split(" ");
        server.updateClient(parsed[0].toInt(), status);
        ui->newClientList->removeItem(ui->newClientList->currentIndex());
        ui->newClientList->addItem(parsed[0] + " " + parsed[1] + " " + status);
    }
    else
    {
        log_message(NO_NEW_CLIENTS);
    }
}

void MainWindow::on_activateClient_clicked()
{
    this->onUpdateClient(STATUS_ACTIVE);
}

void MainWindow::on_deactivateClient_clicked()
{
    this->onUpdateClient(STATUS_INACTIVE);
}
