#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QDebug>
#include <QSslSocket>
#include <QTcpServer>
#include <QPixmap>
#include <QMap>
#include <QList>
#include <QMutex>
#include <QPair>
#include <QHostAddress>

#include "worker.h"
#include "database.h"

/**
 * @brief The Server class is a main class of this application extending QSslServer class. Listens for connections
 * from clients on given address and port. For each connection creates Worker class instance puts it to separate thread
 */
class Server : public QTcpServer
{
    Q_OBJECT

public:
    /**
     * @brief Server - constructor of Server class. Registers qintptr type and creates mutex for Worker class instances
     * @param QObject* parent
     */
    Server(QObject* parent = 0);

    /**
     * @brief startServer - if not started, starts to listen on given port and address. Sends information about
     * outcome to MainWindow class. Also connects to database using instance of Database class
     * @param const QString& host - IP address or name of server
     * @param int port
     */
    void startServer(const QString& host, int port);

    /**
     * @brief sendStopServer - if startted, stops listening and safely disconnets all clients. Informs MainWindow about outcome.
     * Also closes connection to database
     */
    void sendStopServer();

    /**
     * @brief updateClient - calls updateUserStatus() method of Database class to alter status of user
     * @param int id - based on this id user is identified in database
     * @param QString& status - new status of user
     */
    void updateClient(int id, const QString& status);

private:
//    QMap<qintptr, Worker* > threads;

    /**
     * @brief QMap<qintptr, QPair<int, QString>> clients - map of currently connected clients. Socket decriptor created on connection acts as Key. Value consists of
     * ID od client in database and his username
     */
    QMap<qintptr, QPair<int, QString>> clients;

    /**
     * @brief QMutex* mutex - Synchronization tool to prevent simultaneous access to memory by multiple threads
     */
    QMutex* mutex;

    /**
     * @brief Database db - instance of Database class
     */
    Database db;

signals:
    /**
     * @brief uiTextUpdate - signals MainWindow class with message from client identified by username and database id
     * @param id - id of user in database
     * @param username
     * @param arr - received message
     */
    void uiTextUpdate(int id,const QString& username,const QByteArray& arr);

    /**
     * @brief uiImageShow - signals received image from client to be shown in MainWindow
     * @param img
     */
    void uiImageShow(QPixmap img);

    /**
     * @brief uiFilteredShow - signals filtered image received from preprocessing to be showin in MainWindow
     * @param img
     */
    void uiFilteredShow(QPixmap img);

    /**
     * @brief showStatusMessage - signals message to be shown and logged in MainWindow
     * @param message
     */
    void showStatusMessage(QString message);

    /**
     * @brief serverStarted - signals outcome of startServer() method to MainWindow class
     * @param status - true if server started successfully, false otherwise
     * @param host
     * @param port
     */
    void serverStarted(bool status, const QString& host, int port);

    /**
     * @brief serverStopped - signals to MainWindow that server was stopped
     */
    void serverStopped();

    /**
     * @brief clientsChanged - signals change in map of connected clients, emits updated structure
     * @param const QMap<qintptr, QPair<int, QString>>& map - structure containg data of all currently connected clients
     */
    void clientsChanged(const QMap<qintptr, QPair<int, QString>>& map);

    /**
     * @brief disconnectClient - signals to instances of Worker class to disconnect client with given socket descriptor. If given value
     * is -1 all clients are disconnected and their threads safely finished
     * @param qintptr desc - socket descriptor of client to be diconnected. All clients disconnected if -1
     */
    void disconnectClient(qintptr desc);

    /**
     * @brief logInStatus - signals status of login attempt to certain client (Worker) thread based on socket descriptor
     * @param qintptr desc - socket descriptor of receiving client
     * @param int id - id of logged client (if negative value, login unsiccessful)
     */
    void logInStatus(qintptr desc, int id);

    /**
     * @brief registrationStatus - signals status of registration attempt to certain client (Worker) thread based on socket descriptor
     * @param qintptr desc - socket descriptor of receiving client
     * @param bool res - true if registration successful, else false
     * @param bool finger - true if reacting on registration of fingerprint
     */
    void registrationStatus(qintptr desc, bool res, bool finger);

    /**
     * @brief sendNewClients - signals change in list of clients in database
     * @param const QList<QList<QString>>& clients - list containing id, username and status of all clients in database
     */
    void sendNewClients(const QList<QList<QString>>& clients) ;

    /**
     * @brief sendLoginData - emits all fingerprints of user to Worker class for detection
     * @param const QList<QString>& data
     */
    void sendLoginData(qintptr desc, const QList<QString>& data);

    /**
     * @brief sendAllData - emits reference to all fingerprints to identification process
     * @param const QList<QList<QString>>& data
     */
    void sendAllData(qintptr desc, const QList<QList<QString>>& data);

protected:
    /**
     * @brief incomingConnection - on incoming connection creates instance of Worker and QThread classes, sets connections, moves Worker to thread,
     * starts it, updates list of clients and signals event to be logged
     * @param qintptr socketDescriptor - unique identificator of client in current set of connected clients
     */
    void incomingConnection(qintptr socketDescriptor);

private slots:
    /**
     * @brief errorString - writes error message to debug console
     * @param err - error message
     */
    void errorString(QString err);

    /**
     * @brief onStatusMessage - receives status message from Worker thread, formats it and emits showStatusMessage() signal
     * @param int id - database id of client connected to certain Worker class
     * @param QString username - username of this client
     * @param QString message
     */
    void onStatusMessage(int id, QString username, QString message);

    /**
     * @brief onClientDisconnected - forwards message of client disconnected event, removes client from clients map and emits clientsChanged() signal
     * @param qintptr desc - descriptor of client disconeccted
     */
    void onClientDisconnected(qintptr desc);

    /**
     * @brief onRegistration - attempts to create user in database using createUser() method of Database class,
     * emits registrationStatus() and sendNewClients() signals
     * @param qintptr desc - socket descriptor of clienconst QString&t
     * @param const QString& name - username of client
     * @param const QString& hash - hashed password of client
     */
    void onRegistration(qintptr desc, const QString& name, const QString& hash);

    /**
     * @brief onLogIn - attempts to log client by calling logInUser() method of Database class, emtis signal logInStatus() signal and manages map of clients based on outcome
     * @param qintptr desc - socket descriptor of client
     * @param const QString& name - username of client
     * @param const QString& hash - hashed password of client
     */
    void onLogIn(qintptr desc, const QString& name, const QString& hash);

    /**
     * @brief onRegisterFingerprint -
     * @param int id
     * @param qintptr desc
     * @param const QString& xyt
     */
    void onRegisterFingerprint(qintptr desc, int id, const QString& xyt);

    /**
     * @brief onRequestLoginData -
     * @param qintptr desc
     * @param const QString& username
     */
    void onRequestLoginData(qintptr desc, const QString& username);

    /**
     * @brief onRequesAllData -
     * @param qintptr desc
     */
    void onRequesAllData(qintptr desc);

    /**
     * @brief handleLoginSuccess -
     * @param qintptr desc
     * @param int id
     * @param QString name
     */
    void handleLoginSuccess(qintptr desc, int id, QString name);
};

#endif // SERVER_H
