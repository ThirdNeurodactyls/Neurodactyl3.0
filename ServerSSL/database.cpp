#include "database.h"

Database::Database()
{
    this->logger = new Logger(0, LOG_DATABASE_FILE_NAME);
}


void Database::createConnection(){
    db = QSqlDatabase::addDatabase(DATABASSE_DRIVER);

    db.setHostName(DATABASSE_HOSTNAME);
    db.setPort(DATABASSE_PORT);
    db.setDatabaseName(DATABASSE_NAME);
    db.setUserName(DATABASSE_USERNAME);
    db.setPassword(DATABASSE_PASSWORD);

    if(!db.open())
        qWarning() << "ERROR: " << db.lastError();
}


void Database::closeConnection(){
   db.close();
}

int Database::logInUser(QString username, QString password){
    QSqlQuery query;
    QString status;

    query.prepare("SELECT id, status, username FROM person WHERE username = (:username) AND password = AES_ENCRYPT((:pass), (:key))");
    query.bindValue(":username", username);
    query.bindValue(":pass", password);
    query.bindValue(":key", QString(QCryptographicHash::hash((DATABASSE_KEY.toLocal8Bit()),QCryptographicHash::Sha256).toHex()));

    if(!query.exec()){
      qDebug()<< query.lastError().text();
    }
    if(query.first()){
      status = query.value(1).toString();
      if (status == STATUS_ACTIVE){
          logger->write("User " + query.value(2).toString() +  "login successful");
          return query.value(0).toInt();
      }
      else if(status == STATUS_INACTIVE){
          logger->write("User " + query.value(2).toString() +  " login unsuccessful, user INACTIVE");
          return INACTIVE_DUMMY_ID;
      }
      else{
          logger->write("User " + query.value(2).toString() +  " login unsuccessful, user NEW");
          return NEW_DUMMY_ID;
      }
    }
    else{
        logger->write("Login unsuccessful, user unknown");
        return UNKNOWN_DUMMY_ID;
    }
}

bool Database::createUser(QString username, QString password){
    QSqlQuery query;

    query.prepare("INSERT INTO person (username,password,status) "
                  "VALUES ((:username), AES_ENCRYPT(:password, :key),'new')");
    query.bindValue(":username", username);
    query.bindValue(":password", password);
    query.bindValue(":key", QString(QCryptographicHash::hash((DATABASSE_KEY.toLocal8Bit()),QCryptographicHash::Sha256).toHex()));

    if(!query.exec())
      qDebug()<< query.lastError().text();

    if(query.lastInsertId().isNull()){
        logger->write("User " + username +  " not created");
        return false;
    }
    else{
        logger->write("User " + username +  " created");
        return true;
    }
}

bool Database::saveFingerprint(int id_user, QString markants){
    QSqlQuery query;

    query.prepare("INSERT INTO fingerprint (id_user, markants) "
                  "VALUES ((:id_user), (AES_ENCRYPT(:markants, :key)))");
    query.bindValue(":id_user", id_user);
    query.bindValue(":markants", markants);
    query.bindValue(":key", QString(QCryptographicHash::hash((DATABASSE_KEY.toLocal8Bit()),QCryptographicHash::Sha256).toHex()));

    if(!query.exec())
        qDebug()<< query.lastError().text();

    if(query.lastInsertId().isNull()){
        logger->write("Id user " + QString::number(id_user) +  ": fingerprint not saved");
        return false;
    }
    else{
        logger->write("Id user " + QString::number(id_user) +  ": fingerprint saved");
        return true;
    }
}

QList<QList<QString>> Database::getFingerprints(){
    QSqlQuery query;
    QList<QList<QString>> fingerprints;

    query.prepare("SELECT t1.id, username, AES_DECRYPT(markants, (:key_dec)) FROM person t1 LEFT JOIN fingerprint t2 ON t2.id_user = t1.id");
    query.bindValue(":key_dec", QString(QCryptographicHash::hash((DATABASSE_KEY.toLocal8Bit()),QCryptographicHash::Sha256).toHex()));

    if(!query.exec())
      qDebug()<< query.lastError().text();

    if(query.first()){
        do{
            QList<QString> list;
            list << QString::number(query.value(0).toInt()) << query.value(1).toString() << query.value(2).toString();
            fingerprints << list;
        }while(query.next());
        return fingerprints;
    }
    else{
        return fingerprints;
    }
}

QList<QList<QString>> Database::getSingUpRequest(){
    QSqlQuery query;
    QList<QList<QString>> users;

    query.prepare("SELECT id, username, status FROM person");

    if(!query.exec())
      qDebug()<< query.lastError().text();

    if(query.first()){
        do{
            QList<QString> list;
            list << QString::number(query.value(0).toInt()) << query.value(1).toString() << query.value(2).toString();
            users << list;
        }while(query.next());
      return users;
    }
    else
      return users;
}

void Database::updateUserStatus(int id_user, QString status){
    QSqlQuery query;

    query.prepare("UPDATE person SET status = :status WHERE id = :id_user");
    query.bindValue(":status", status);
    query.bindValue(":id_user", id_user);

    if(!query.exec()){
        qDebug()<< query.lastError().text();
    }
    logger->write("Id user " + QString::number(id_user) +  ": status changed " + status);
}

QList<QString> Database::logInUserFingerprint(QString username){
    QSqlQuery query;
    QString status;
    QList<QString> markants;

    query.prepare("SELECT t1.id, t1.status, AES_DECRYPT(t2.markants, (:key_dec)) FROM person t1 LEFT JOIN fingerprint t2 ON t2.id_user = t1.id WHERE username = (:username)");
    query.bindValue(":username", username);
    query.bindValue(":key_dec", QString(QCryptographicHash::hash((DATABASSE_KEY.toLocal8Bit()),QCryptographicHash::Sha256).toHex()));

    if(!query.exec())
      qDebug()<< query.lastError().text();

    if(query.first()){
      status = query.value(1).toString();
      if (status == STATUS_ACTIVE){
          logger->write("User " + username +  " login, has permission");
          markants << QString::number(query.value(0).toInt());
          do{
              markants << query.value(2).toString();
          }while(query.next());
      }
      else if (status == STATUS_NEW){
          markants << QString::number(NEW_DUMMY_ID);
      }
      else{
          logger->write("User " + query.value(2).toString() +  " login unsuccessful, user is not ACTIVE");
          markants << QString::number(INACTIVE_DUMMY_ID);
      }
    }
    else{
        logger->write("Login unsuccessful, user unknown");
        markants << QString::number(UNKNOWN_DUMMY_ID);
    }
    return markants;
}


