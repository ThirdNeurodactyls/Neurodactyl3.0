#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>

#include <QDebug>
#include <QCryptographicHash>
#include "constants.h"
#include "logger.h"

/**
 * @brief The Database class - provides interace for application to work with database
 */
class Database
{
public:
    /**
     * @brief Database - default constructor
     */
    Database();

    /**
     * @brief createConnection - tries to connect to database based on information in config file
     */
    void createConnection();

    /**
     * @brief closeConnection - closes connection to database
     */
    void closeConnection();

    /**
     * @brief logInUser - ties to verify user presence in database based on provided data. Return id of client
     * or dummy value indicating various problems with login attempt
     * @param QString username
     * @param QString password - hash of user password
     * @param QString markants - data provided from fingerprint analysis during ogging in with fingerprint
     * @return int client id
     */
    int logInUser(QString username, QString password);

    /**
     * @brief createUser - attempts to create new user in database based on provided data. Return bool value of reult
     * @param QString username
     * @param QString password - hashed user password
     * @return bool
     */
    bool createUser(QString username, QString password);

    /**
     * @brief saveFingerprint - adds fingerprint data to client entry based on provided ID
     * @param id_user - id of user that will have fingerprint assigned to him
     * @param markants - file containing information about fingerprint
     * @return bool - was action success?
     */
    bool saveFingerprint(int id_user, QString markants);

    /**
     * @brief getSingUpRequest - returns list of all clients in database and their statuses
     * @return QList<QList<QString>>
     */
    QList<QList<QString>> getSingUpRequest();

    /**
     * @brief updateUserStatus - changes status of client in databse
     * @param int id_user - id of user whos status will be changed
     * @param QString status
     */
    void updateUserStatus(int id_user, QString status);

    /**
     * @brief getFingerprints - get all fingerprints for comparing
     * @param QString markants
     * @return QString
     */
    QList<QList<QString>> getFingerprints();

    /**
     * @brief logInUserFingerprint - get all fingerprints for user identification
     * @param username
     * @return
     */
    QList<QString> logInUserFingerprint(QString username);

private:
    QSqlDatabase db;
    Logger* logger;
};

#endif // DATABASE_H
