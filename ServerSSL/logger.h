#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QPlainTextEdit>
#include <QDebug>
#include <QDir>

#include "constants.h"

/**
 * @brief The Logger class - class responsible fo logging messages to file system, plainTextEdit if provided or debug console
 */
class Logger : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Logger - constructor of Logger class. Opens or creates file on given path (including path)
     * and prepares it for writing
     * @param QObject* parent
     * @param const QString& file - filename including path of file to be opened or created
     * @param QPlainTextEdit edit - QPlainTexEdit object to log messages into. Optional parameter
     */
    explicit Logger(QObject* parent, const QString& file, QPlainTextEdit* edit=0);
    ~Logger();

    /**
     * @brief setConsoleLogging - turns on and off lgging to debug console
     * @param bool value
     */
    void setConsoleLogging(bool value);

private:
    /**
     * @brief pathFromFilename - parses path out of filename providet to Logger class in constructor
     * @param const QString& str - filename
     * @return QString
     */
    QString pathFromFilename(const QString& str);

    /**
     * @brief QFile* file - pointer to opened file
     */
    QFile* file;

    /**
     * @brief QPlainTextEdit* edit - Optional parameter of Logger class, if present messges will be logged here as well
     */
    QPlainTextEdit* edit;

    /**
     * @brief bool console - setting set by setConsoleLogging() method
     */
    bool console;

    /**
     * @brief logEdit - setting of turining logging to QPlainTextEdit on and off
     */
    bool logEdit;

public slots:
    /**
     * @brief write - main method od Logger class. Loggs given messgae to file, optionally to QPlainTextEdit or console with timestamp
     * @param const QString& text - message to be logged
     */
    void write(const QString& text);
};

#endif // LOGGER_H
