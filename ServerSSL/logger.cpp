#include "logger.h"

Logger::Logger(QObject *parent, const QString& file, QPlainTextEdit* edit) : QObject(parent)
{
    this->edit = edit;
    this->console = DEFAULT_LOG_CONSOLE;
    this->logEdit = DEFAULT_LOG_EDIT;
    if (!file.isEmpty())
    {
        QString path = this->pathFromFilename(file);
        if (!path.isEmpty())
        {
            QDir dir(path);
            dir.mkpath(".");
        }
        this->file = new QFile();
        this->file->setFileName(file);
        this->file->open(QIODevice::Append | QIODevice::Text);
    }
}

Logger::~Logger()
{
    if (this->file != 0)
    {
        this->file->close();
    }
}

void Logger::setConsoleLogging(bool value) { this->console = value; }

void Logger::write(const QString &text)
{
    QString output = "["+QDateTime::currentDateTime().toString(LOG_DATETIME_FORMAT) +"]\n    "+text;
    QTextStream out(this->file);
    out.setCodec("UTF-8");
    if (this->file != 0)
    {
        out << output << "\n";
    }
    if (this->console)
    {
        qDebug() << output;
    }
    if (this->edit != 0 && this->logEdit)
    {
        this->edit->appendPlainText(output);
    }
}

QString Logger::pathFromFilename(const QString& filename)
{
    std::string str = filename.toStdString();
    std::size_t pos = str.find_last_of("/\\");
    if (pos != std::string::npos)
    {
        return QString::fromStdString(str.substr(0, pos));
    }
    else
    {
        return "";
    }

}
