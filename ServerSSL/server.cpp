#include "server.h"

Server::Server(QObject *parent) : QTcpServer(parent)
{
    qRegisterMetaType<qintptr>("qintptr");
    qRegisterMetaType<QList<QString>>("QList<QString>");
    qRegisterMetaType<QList<QList<QString>>>("QList<QList<QString>>");
    this->mutex = new QMutex();
}

void Server::startServer(const QString& host, int port)
{
    if (!this->isListening())
    {
        if (!this->listen(QHostAddress(host), port))
        {
            emit serverStarted(false, host, port);
        }
        else
        {
            emit serverStarted(true, host, port);
        }
    }
    else
    {
        emit showStatusMessage(SERVER_ALREADY_STARTED);
        qDebug() << SERVER_ALREADY_STARTED;
    }
    db.createConnection();
    emit sendNewClients(db.getSingUpRequest());
}

void Server::sendStopServer()
{
    if (this->isListening())
    {
        this->close();
        qDebug() << SERVER_STOPPED_SUCCESS;
        emit disconnectClient(-1);
        emit serverStopped();
    }
    else
    {
        emit showStatusMessage(SERVER_NOT_RUNNING);
    }
    db.closeConnection();
}

void Server::incomingConnection(qintptr socketDescriptor)
{
    qDebug() << socketDescriptor << CLIENT_CONNECTING;

    Worker* w = new Worker(socketDescriptor, this->mutex);

    connect(w, SIGNAL(guiTextUpdate(int,const QString&,const QByteArray&)), this, SIGNAL(uiTextUpdate(int,const QString&,const QByteArray&)));
    connect(w, SIGNAL(guiImageShow(QPixmap)), this, SIGNAL(uiImageShow(QPixmap)));
    connect(w, SIGNAL(guiFilteredShow(QPixmap)), this, SIGNAL(uiFilteredShow(QPixmap)));
    connect(w, SIGNAL(sendStatusMessage(int, QString, QString)), this, SLOT(onStatusMessage(int, QString, QString)));
    connect(w, SIGNAL(clientDisconnected(qintptr)), this, SLOT(onClientDisconnected(qintptr)));
    connect(w, SIGNAL(registration(qintptr,QString,QString)), this, SLOT(onRegistration(qintptr,QString,QString)));
    connect(w, SIGNAL(logIn(qintptr,QString,QString)), this, SLOT(onLogIn(qintptr,QString,QString)));
    connect(w, SIGNAL(registerFingerpreint(qintptr,int,const QString&)), this, SLOT(onRegisterFingerprint(qintptr,int,const QString&)));
    connect(w, SIGNAL(requestLoginData(qintptr,const QString&)), this, SLOT(onRequestLoginData(qintptr,const QString&)));
    connect(w, SIGNAL(requestAllData(qintptr)), this, SLOT(onRequesAllData(qintptr)));
    connect(w, SIGNAL(loginSuccess(qintptr,int,QString)), this, SLOT(handleLoginSuccess(qintptr,int,QString)));

    connect(this, SIGNAL(disconnectClient(qintptr)), w, SLOT(onDisconnectClient(qintptr)));
    connect(this, SIGNAL(logInStatus(qintptr,int)), w, SLOT(onLogInStatus(qintptr,int)));
    connect(this, SIGNAL(registrationStatus(qintptr,bool,bool)), w, SLOT(onRegistrationStatus(qintptr,bool,bool)));
    connect(this, SIGNAL(sendLoginData(qintptr,const QList<QString>&)), w, SLOT(verifyLogin(qintptr,const QList<QString>&)));
    connect(this, SIGNAL(sendAllData(qintptr,const QList<QList<QString>>&)), w, SLOT(findUserByFingerprint(qintptr,const QList<QList<QString>>&)));

    QThread* qt = new QThread();

    connect(qt, SIGNAL (started()), w, SLOT (process()));
    connect(w, SIGNAL (finished()), qt, SLOT (quit()));
    connect(w, SIGNAL (finished()), w, SLOT (deleteLater()));
    connect(qt, SIGNAL (finished()), qt, SLOT (deleteLater()));

    w->moveToThread(qt);
    qt->start();

    onStatusMessage(CLIENT_ID_DUMMY, CLIENT_NAME_DUMMY, CLIENT_CONNECTED);
    clients.insert(socketDescriptor, QPair<int, QString>(CLIENT_ID_DUMMY, CLIENT_NAME_DUMMY));
    emit clientsChanged(clients);
}

void Server::errorString(QString err)
{
    qDebug() << ERROR_MESSAGE << err;
}

void Server::onClientDisconnected(qintptr desc)
{
    if (clients.contains(desc))
    {
        onStatusMessage(clients[desc].first, clients[desc].second, CLIENT_DISCONNECTED);
        clients.remove(desc);
    }
    emit clientsChanged(clients);
}

void Server::onLogIn(qintptr desc, const QString &name, const QString &hash)
{
    int id = this->db.logInUser(name, hash);
    emit logInStatus(desc, id);
    if (clients.contains(desc) && id != UNKNOWN_DUMMY_ID && id != NEW_DUMMY_ID && id != INACTIVE_DUMMY_ID)
    {
        handleLoginSuccess(desc, id, name);
    }
    else
    {
        onStatusMessage(CLIENT_ID_DUMMY, name, CLIENT_LOGIN_ATTEMPTED);
    }
}

void Server::handleLoginSuccess(qintptr desc, int id, QString name){
    clients[desc] = QPair<int, QString>(id, name);
    onStatusMessage(clients[desc].first, clients[desc].second, CLIENT_LOGGED);
    emit clientsChanged(clients);
}

void Server::onRegistration(qintptr desc, const QString &name, const QString &hash)
{
    emit registrationStatus(desc, this->db.createUser(name, hash), false);
    emit sendNewClients(db.getSingUpRequest());
}

void Server::onStatusMessage(int id, QString name, QString message) {
    emit showStatusMessage("["+QString::number(id)+"]:["+name+"]: "+message);
}

void Server::updateClient(int id, const QString& status)
{
    db.updateUserStatus(id, status);
}

void Server::onRegisterFingerprint(qintptr desc, int id, const QString &xyt)
{
    if (!(id == -1 || xyt.isNull()))
    {
        emit registrationStatus(desc, db.saveFingerprint(id, xyt), true);
    }
    else
    {
        onStatusMessage(id, "unknown", NO_FINGERPRINT);
    }
}

void Server::onRequestLoginData(qintptr desc, const QString &username)
{
    emit sendLoginData(desc, db.logInUserFingerprint(username));
}

void Server::onRequesAllData(qintptr desc)
{
    emit sendAllData(desc, db.getFingerprints());
}
