#-------------------------------------------------
#
# Project created by QtCreator 2017-10-18T13:03:33
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ServerSSL
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    server.cpp \
    worker.cpp \
    logger.cpp \
    database.cpp 

HEADERS += \
        mainwindow.h \
    server.h \
    worker.h \
    constants.h \
    logger.h \
    database.h 

FORMS += \
        mainwindow.ui


unix {
    unix:!macx: LIBS += -L/usr/local/lib/ -lopencv_world

    unix:!macx: LIBS += -laf

    INCLUDEPATH += /usr/local/include
    DEPENDPATH += /usr/local/include

    unix:!macx: LIBS += -L'/home/test/tp2018/main_project/Neurodactyl3.0/Dependencies/preprocessing/' -lPreprocessing

    INCLUDEPATH += '/home/test/tp2018/main_project/Neurodactyl3.0/Dependencies/preprocessing/include'
    DEPENDPATH += '/home/test/tp2018/main_project/Neurodactyl3.0/Dependencies/preprocessing/include'

    unix:!macx: LIBS += -L$$PWD/../Dependencies/Extraction/ -lExtraction

    INCLUDEPATH += $$PWD/../Dependencies/Extraction/include
    DEPENDPATH += $$PWD/../Dependencies/Extraction/include

}


win32 {
    unix:!macx|win32: LIBS += -L'C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/opencv/build/x64/vc14/lib/' -lopencv_world340

    INCLUDEPATH += 'C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/opencv/build/include'
    DEPENDPATH += 'C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/opencv/build/include'

    unix:!macx|win32: LIBS += -L'C:/Program Files/ArrayFire/v3/lib/' -laf

    INCLUDEPATH += 'C:/Program Files/ArrayFire/v3/include'
    DEPENDPATH += 'C:/Program Files/ArrayFire/v3/include'

    unix:!macx|win32: LIBS += -L$$PWD/../Dependencies/preprocessing/ -lPreprocessing

    INCLUDEPATH += $$PWD/../Dependencies/preprocessing/include
    DEPENDPATH += $$PWD/../Dependencies/preprocessing/include
}
