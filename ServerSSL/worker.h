#ifndef THREAD_H
#define THREAD_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QTcpServer>
#include <QSslSocket>
#include <QBuffer>
#include <QImage>
#include <QPixmap>
#include <QFile>
#include <QMutex>
#include <QProcess>
#include <QtMath>

#include "preprocessing.h"
#include "converter.h"
#include "constants.h"
#include "database.h"
#include "extraction.h"

/**
 * @brief The Worker class - each instance of this class represents connected client and processes its requests. Eeach worker instance lives in separated thread.
 */
class Worker : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Worker - constructor based on socket descriptor. Adds socket descriptor to classes QSslSocket and stores pointer to instance of QMutex
     * @param qintptr desc - socket descriptor
     * @param QMutex* mutex - mutex instance received from server
     */
    Worker(qintptr desc, QMutex* mutex);

    ~Worker();

public slots:
    /**
     * @brief process - method executed by thread upon calling its start() method. Sets workers QSslSocket based on sicket descriptor,
     * and establishes SSL encrypted connection with client app. Connects signals to manage incoming communication and client disconnection
     */
    void process();

    /**
     * @brief onReadyRead - if not in image read mode, calls decideContent() method, else reads image
     */
    void onReadyRead();

    /**
     * @brief onDisconnected - slot reacting when client app disconnects. Informs Server about this behaviour and safely ends own existance
     */
    void onDisconnected();

    /**
     * @brief onDisconnectClient - if received socket descriptor equals one of the instance or special identificator (-1), closes SSL connection and
     * ends own existance
     * @param qintptr desc - socket desctriptor
     */
    void onDisconnectClient(qintptr desc);

    /**
     * @brief onLogInStatus - based on id received from server after login attempt informs client app about outcome of this attempt
     * @param qintptr desc - socket descriptor
     * @param int id - id received from database or dummy value in case of failure
     */
    void onLogInStatus(qintptr desc, int id);

    /**
     * @brief onRegistrationStatus - based on status from server after registration attempt informs client app about outcome of this attempt
     * @param qintptr desc - socket descriptor
     * @param bool status - true if registration successful else false
     * @param bool finger - true if reacting on registration of fingerprint
     */
    void onRegistrationStatus(qintptr desc, bool status, bool finger);

    /**
     * @brief verifyLogin -
     * @param qintptr desc
     * @param QList<QString>& data
     */
    void verifyLogin(qintptr desc, const QList<QString>& data);

    /**
     * @brief findUserByFingerprint -
     * @param qintptr desc
     * @param const QList<QList<QString>>& data
     */
    void findUserByFingerprint(qintptr desc, const QList<QList<QString>>& data);

private:
    /**
     * @brief QSslSocket* serverSocket - socket used for secured connection with client app
     */
    QSslSocket* serverSocket = NULL;

    /**
     * @brief QMutex* mutex - pointer to instance of QMutex created and held in Server class
     */
    QMutex* mutex;

    /**
     * @brief bool IMAGE_READ_MODE - if true, every received data are considered to be image data
     */
    bool IMAGE_READ_MODE = false;

    /**
     * @brief qintptr socketDescriptor - unique identificator of client in current set of connected clients
     */
    qintptr socketDescriptor;

    /**
     * @brief QString username - username of client in database, before accepted login dummy value is used
     */
    QString username = CLIENT_NAME_DUMMY;

    /**
     * @brief int id - if of client in database, before accepted login dummy value is used
     */
    int id = UNKNOWN_DUMMY_ID;

    /**
     * @brief uchar* imgBuffer - buffer of currently processed image needed in preprocessing process
     */
    uchar* imgBuffer;

    /**
     * @brief QByteArray* imageArray - byte array used for storing image data during image receiving
     */
    QByteArray* imageArray;

    /**
     * @brief qint32 imageSize - expected size of image to be received sent by client app
     */
    qint32 imageSize;

    /**
     * @brief imageType - identificator deterinating what should be done with image
     */
    QString imageType;

    /**
     * @brief QString xyttmp - temporary .xyt file storage
     */
    QString xyttmp;

    //data management
    /**
     * @brief decideContent - reads message from client on readyRead() signal and calls different methods based on its content. Messages from client should consist of
     * two parts, type identificator in form of specific string and the message
     */
    void decideContent();

    /**
     * @brief manageTextContent - called if received message is text content, signals this messgae to be viewed in MainWindow together with username and if of cliend sending it
     */
    void manageTextContent();

    /**
     * @brief manageImageContent - called if received message signals image content, sets IMAGE_READ_MODE to true, receives size of image and calls imageRead() method
     */
    void manageImageContent();

    /**
     * @brief imageRead - reads data from client socket, if size of received image data stored in imageArray is equal or more than expected image size, calls manageFinishedImage() methdod
     */
    void imageRead();

    /**
     * @brief manageFinishedImage - saves receied image data to QImage, sends it to be viewed in server, turns off IMAGE_READ_MODE and sends it to preprocessing by calling processImage()
     */
    void manageFinishedImage();

    /**
     * @brief processImage - creates instance of Preprocessing class and starts preprocessing process. If successful, sends image skeleton to be viewed in MainWindow and return it. Also informs client about
     * outcome.
     * @param const QImage& img - image received from client app
     * @return QImage* - processed image pointer
     */
    QImage* processImage(const QImage& img);

    /**
     * @brief extractMarkants - creates instance of Extraction class and extracts markants form image skeleton, call identificartion using neral network and creates .xyt file
     * @param QImage skeleton - processed image pointer
     * @param QImage original - original image received from client
     * @return QString - .xyt file
     */
    QString extractMarkants(QImage* skeleton, QImage original);

    /**
     * @brief executeProcees -
     * @param program
     * @param args
     * @return
     */
    QString executeProcees(QString program, QStringList args);

    /**
     * @brief executePythonScript -
     * @param exe
     * @return
     */
    QString executePythonScript(QString exe);

    /**
     * @brief bozorthCompare -
     * @param const QString& one
     * @param const QList<QString>& others
     * @return
     */
    bool bozorthCompare(const QString& one, const QList<QString>& others);

    /**
     * @brief makeFile -
     * @param const QString& data
     * @param const QString& filename
     * @return
     */
    bool makeFile(const QString& data, const QString& filename);

    /**
     * @brief buildXYT -
     * @param const QString& data
     * @param const QStringList& coords
     * @return QString
     */
    QString buildXYT(const QString& data, const QStringList& coords);

    /**
     * @brief neuralIdentify
     * @param const QStringList& name_list
     * @return QString
     */
    QString neuralIdentify(const QStringList& name_list);

    /**
     * @brief manageRegistration - called if received message signals registration process, reads username and password hash and signals registration() for Server
     */
    void manageRegistration();

    /**
     * @brief manageLogin - - called if received message signals login process, reads username and password hash and signals logIn() for Server. Also sets local username variable
     */
    void manageLogin();

    /**
     * @brief emitStatus - emits message together with id and username of client using sendStatusMessage() signal
     * @param const QString& message
     */
    void emitStatus(const QString& message);

signals:
    /**
     * @brief clientDisconnected - signals when client disconnecsts
     * @param qintptr desc - socket descriptor
     */
    void clientDisconnected(qintptr desc);

    /**
     * @brief guiTextUpdate - signals message from client to be shown in MainWindow
     * @param int id
     * @param const QString& username
     * @param const QByteArray& arr - message in byte form
     */
    void guiTextUpdate(int id, const QString& username, const QByteArray& arr);

    /**
     * @brief guiImageShow - signals original image received from client to be shown in MainWindow
     * @param QPixmap img
     */
    void guiImageShow(QPixmap img);

    /**
     * @brief guiFilteredShow - signals filtered image skeleton to be shown in MainWindow
     * @param QPixmap img
     */
    void guiFilteredShow(QPixmap img);

    /**
     * @brief sendStatusMessage - emits message to be logged and shown in MainWindow
     * @param int id
     * @param QString username
     * @param QString message
     */
    void sendStatusMessage(int id, QString username, QString message);

    /**
     * @brief finished - signal emited on thread finished
     */
    void finished();

    /**
     * @brief logIn - emits signal to server to attempt login of user based on credentials
     * @param qintptr desc - socket descriptor
     * @param const QString& name - username of client
     * @param const QString& hash - hashed password
     */
    void logIn(qintptr desc, const QString& name, const QString& hash);

    /**
     * @brief registration - emits signal to server to attempt registration of user based on credentials
     * @param qintptr desc - socket descriptor
     * @param const QString& name - username of client
     * @param const QString& hash - hashed password
     */
    void registration(qintptr desc, const QString& name, const QString& hash);

    /**
     * @brief registerFingerpreint - send request for fingerprint registration based on user id
     * @param desc - socket descriptor
     * @param id - id of user in database
     * @param xyt - .xyt file to be registered
     */
    void registerFingerpreint(qintptr desc, int id, const QString& xyt);

    /**
     * @brief requestLoginData - requests identification vectors fom database based on username
     * @param desc - socket descriptor
     * @param const QString& username
     */
    void requestLoginData(qintptr desc, const QString& username);

    /**
     * @brief requestAllData - requests all data to find owner of fingerprint
     * @param desc - socket descriptor
     */
    void requestAllData(qintptr desc);

    /**
     * @brief loginSuccess -
     * @param qintptr desc
     * @param int id
     * @param QString name
     */
    void loginSuccess(qintptr desc, int id, QString name);
};

#endif // THREAD_H
