#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QObject>

// STRING MESSAGES

// Communication messages identifiers
static const QString TEXT_MESSAGE = "TEXT";
static const QString IMAGE_MESSAGE = "IMAGE";
static const QString REGISTRATION = "REGISTER";
static const QString LOGIN = "LOGIN";
static const QString LOGIN_SUCCESS = "LOGIN_SUCCESS";
static const QString LOGIN_FAILED = "LOGIN_FAILED";
static const QString LOGIN_NEW = "LOGIN_NEW";
static const QString LOGIN_INACTIVE = "LOGIN_INACTIVE";
static const QString REGISTER_SUCCESS = "REGISTER_SUCCESS";
static const QString REGISTER_FAILED = "REGISTER_FAILED";
static const QString REGISTER_IMAGE = "REGISTER_IMAGE";
static const QString FIND_IMAGE = "FIND_IMAGE";
static const QString LOGIN_IMAGE = "LOGIN_IMAGE";
static const QString FINGER_REGISTER_SUCCESS = "Fingerprint registrated successfully";
static const QString FINGER_REGISTER_FAILED = "Fingerprint registration unsuccessful";

// Communication responses
static const QString TEXT_HANDLED = "Thank you for Your text message";
static const QString IMAGE_HANDLED = "Thank you for Your image. ";
static const QString MESSAGE_NOT_RECOGNIZED = "Message was not recognized";
static const QString IMAGE_RECEIVED = "Image received successuflly";
static const QString IMAGE_CORRUPTED = "Image was not received successuflly";
static const QString IMAGE_TOO_BIG = "Image too large to process";
static const QString IMAGE_PROCESSED = "Image processed successfuly";
static const QString IMAGE_NOT_PROCESSED = "Processing unsuccessful. Image format probably unsupported";

// Status messages
static const QString SERVER_NOT_STARTED = "Sever could not be started on port ";
static const QString SERVER_NOT_RUNNING = "Server is currently not running";
static const QString SERVER_STARTED = "Sever successfully started listening on ";
static const QString SERVER_ALREADY_STARTED = "Server is already running";
static const QString SERVER_STOPPED_SUCCESS = "Server stopped successfully";
static const QString CLIENT_NAME_DUMMY = "undefined";
static const int CLIENT_ID_DUMMY = -1;
static const QString CLIENT_CONNECTING = " Connecting...";
static const QString CLIENT_CONNECTED = "Client Connected";
static const QString CLIENT_LOGGED = "Client loggedIn";
static const QString CLIENT_LOGIN_ATTEMPTED = "Client attempted to log In";
static const QString CLIENT_DISCONNECTED ="Client Disconnected";
static const QString NO_CLIENTS = "List of clients is currently empty";
static const QString NO_NEW_CLIENTS = "No new clients at the moment";
static const QString ERROR_MESSAGE = "Error... ";
static const QString THREAD_STARTED = "Thread started";
static const QString SETTING_DESCRIPTOR_FAILED = "Setting socket descriptor failed. Thread not activated";
static const QString NO_PATH_SPECIFIED = "No path was specified";
static const QString WINDOWS_ONLY = "This functionality is WINDOWS only at the moment";
static const QString EXTRACTION_FAIL = "Extraction process was unsuccessful";
static const QString IMAGE_TYPE_UNKNOWN = "Image status unrecognized";
static const QString NO_FINGERPRINT = "Problem during fingerprint registration, data not received";
static const QString NO_MATCH = "No match found in database";

// Log types
static const QString MESSAGE_TYPE = "MESSAGE";

// IDENTIFICATION
static const QString XYT_FILENAME = "f.txt";
static const QString BOZ_FILE_PROBLEM = "Problem during identification process";
static const QString BOZ_PATH = "/home/test/tp2018/main_project/Neurodactyl3.0/Dependencies/bozorth3/bin/bozorth3";
static const int BOZ_TRESHOLD = 20;
static const float PROB_TRESHOLD = 0.6;
static const QString IGNORE_CLASS = "ignore";
static const QString NEURAL_DELIM = "###";
static const QString PYTHON_DELIM = "^^";
static const QString NEURAL_ENTRY_DELIM = " ";
static const QString COORDS_DELIM = ";";
static const QString XYT_DELIM = "\n";
static const QString MARKANT_NAME = "markant";
static const QString MARKANT_TYPE = ".jpg";

// PYTHON EXE PATHS
static const QString LOCAL_PYTHON_ENV = "/home/test/Downloads/anaconda3/bin/python3";
static const QString SCRIPT_PATH = "/home/test/tp2018/main_project/Neurodactyl3.0/Python/label_image.py";
static const QString MODEL_PATH = "/home/test/tp2018/main_project/Neurodactyl3.0/Dependencies/resnet_1.2/output_graph.pb";
static const QString LABEL_PATH = "/home/test/tp2018/main_project/Neurodactyl3.0/Dependencies/resnet_1.2/output_labels.txt";
static const QString INPUT_LAYER = "Placeholder";
static const QString OUTPUT_LAYER = "final_result";
static const QString MARCANT_PATH = "/home/test/tp2018/main_project/Neurodactyl3.0/Builds/ServerBuild/debug/";
static const QString IMG_WIDTH = "224";
static const QString IMG_HEIGHT = "224";

// OTHER CONSTANTS
static const QString HOST_NAME = "147.175.106.8";
static const int PORT_NUMBER = 10000;

static const bool DEFAULT_LOG_CONSOLE = true;
static const bool DEFAULT_LOG_EDIT = true;
static const QString LOG_FILE_NAME = "logs/serverLog.txt";
static const QString LOG_DATETIME_FORMAT = "dd.MM.yyyy hh:mm:ss.zzz";

static const QString CERTIFICATE_PATH = "certificate.pem";
static const QString PRIVATE_KEY_PATH = "key.pem";

static const QString IMAGE_RECEIVE_FORMAT = "PNG";
static const QString IMAGE_DATA_NAME = "name";

static const int EXTRACTION_SIZE = 17;

static const int LABEL_WIDTH = 261;
static const int LABEL_HEIGHT = 331;

// Database setings
static const QString DATABASSE_NAME = "tp2018";
static const QString DATABASSE_DRIVER = "QMYSQL";
static const QString DATABASSE_HOSTNAME = "localhost";
static const int DATABASSE_PORT = 3306;
static const QString DATABASSE_USERNAME = "test_user";
static const QString DATABASSE_PASSWORD = "test_user";
static const QString DATABASSE_KEY = "test";


// Database user statuses
static const QString STATUS_ACTIVE = "active";
static const QString STATUS_INACTIVE = "inactive";
static const QString STATUS_NEW = "new";

// dummy ids per statuses
static const int INACTIVE_DUMMY_ID = -3;
static const int NEW_DUMMY_ID = -2;
static const int UNKNOWN_DUMMY_ID = -1;

static const QString LOG_DATABASE_FILE_NAME = "logs/databaseLog.txt";


#endif // CONSTANTS_H
