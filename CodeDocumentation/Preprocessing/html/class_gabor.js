var class_gabor =
[
    [ "Gabor", "class_gabor.html#a3fe3114495a15e8dda75c9a282b16fdd", null ],
    [ "Gabor", "class_gabor.html#afd5f50a5ef0fccda39ea10852baacac8", null ],
    [ "compute_gaborKernels", "class_gabor.html#ab57ff73c9ff283763fa2304b49841ab9", null ],
    [ "getAf_imgIn", "class_gabor.html#aebf684d0022b13e8f611192504098811", null ],
    [ "getAf_imgOut", "class_gabor.html#ace5f1f3553736389277e3e8e8de82f24", null ],
    [ "printTiming", "class_gabor.html#aa4a547fbe6dd9d4f9bf68913c37e8199", null ],
    [ "run", "class_gabor.html#a8405c8c9f133945653a84f2a52676e64", null ],
    [ "setAf_imgIn", "class_gabor.html#a760372772eb22b0d7db0a90478e90caa", null ],
    [ "af_imgIn", "class_gabor.html#a8b4e4107229f8397d2d5392da554da69", null ],
    [ "af_imgOut", "class_gabor.html#aad8cf9a48fc47a98d5ba887a0bfaa4dd", null ],
    [ "cpp_settings", "class_gabor.html#a368bf13c365d5df5f69252a7c7bcc17f", null ],
    [ "cpp_useTiming", "class_gabor.html#a97bf3b3bf489e5b607807d51e02a8704", null ],
    [ "timing", "class_gabor.html#acebf847ecc6c530cadad719af57b0b07", null ]
];