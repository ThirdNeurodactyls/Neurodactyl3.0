var dir_34ab003271e9b725ecc3cd9f82a09047 =
[
    [ "converter.cpp", "converter_8cpp.html", null ],
    [ "converter.h", "converter_8h.html", [
      [ "Converter", "class_converter.html", "class_converter" ]
    ] ],
    [ "converter_global.h", "converter__global_8h.html", "converter__global_8h" ],
    [ "frequencymap.cpp", "frequencymap_8cpp.html", null ],
    [ "frequencymap.h", "frequencymap_8h.html", [
      [ "FrequencyMap", "class_frequency_map.html", "class_frequency_map" ]
    ] ],
    [ "frequencymap_global.h", "frequencymap__global_8h.html", "frequencymap__global_8h" ],
    [ "gabor.cpp", "gabor_8cpp.html", null ],
    [ "gabor.h", "gabor_8h.html", "gabor_8h" ],
    [ "gabor_global.h", "gabor__global_8h.html", "gabor__global_8h" ],
    [ "helpers.cpp", "helpers_8cpp.html", null ],
    [ "helpers.h", "helpers_8h.html", [
      [ "Helpers", "class_helpers.html", null ]
    ] ],
    [ "helpers_global.h", "helpers__global_8h.html", "helpers__global_8h" ],
    [ "imagehelpers.cpp", "imagehelpers_8cpp.html", null ],
    [ "imagehelpers.h", "imagehelpers_8h.html", [
      [ "ImageHelpers", "class_image_helpers.html", "class_image_helpers" ]
    ] ],
    [ "imagehelpers_global.h", "imagehelpers__global_8h.html", "imagehelpers__global_8h" ],
    [ "normalisation.cpp", "normalisation_8cpp.html", null ],
    [ "normalisation.h", "normalisation_8h.html", [
      [ "Normalisation", "class_normalisation.html", "class_normalisation" ]
    ] ],
    [ "normalisation_global.h", "normalisation__global_8h.html", "normalisation__global_8h" ],
    [ "orientationmap.cpp", "orientationmap_8cpp.html", null ],
    [ "orientationmap.h", "orientationmap_8h.html", [
      [ "OrientationMap", "class_orientation_map.html", "class_orientation_map" ]
    ] ],
    [ "orientationmap_global.h", "orientationmap__global_8h.html", "orientationmap__global_8h" ],
    [ "preprocessing.cpp", "preprocessing_8cpp.html", null ],
    [ "preprocessing.h", "preprocessing_8h.html", "preprocessing_8h" ],
    [ "preprocessing_global.h", "preprocessing__global_8h.html", "preprocessing__global_8h" ],
    [ "segmentation.cpp", "segmentation_8cpp.html", null ],
    [ "segmentation.h", "segmentation_8h.html", [
      [ "Segmentation", "class_segmentation.html", "class_segmentation" ]
    ] ],
    [ "segmentation_global.h", "segmentation__global_8h.html", "segmentation__global_8h" ],
    [ "skeleton.cpp", "skeleton_8cpp.html", null ],
    [ "skeleton.h", "skeleton_8h.html", [
      [ "Skeleton", "class_skeleton.html", "class_skeleton" ]
    ] ],
    [ "skeleton_global.h", "skeleton__global_8h.html", "skeleton__global_8h" ]
];