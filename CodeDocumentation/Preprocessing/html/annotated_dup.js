var annotated_dup =
[
    [ "Converter", "class_converter.html", "class_converter" ],
    [ "FrequencyMap", "class_frequency_map.html", "class_frequency_map" ],
    [ "Gabor", "class_gabor.html", "class_gabor" ],
    [ "gaborSettings", "structgabor_settings.html", "structgabor_settings" ],
    [ "Helpers", "class_helpers.html", null ],
    [ "ImageHelpers", "class_image_helpers.html", "class_image_helpers" ],
    [ "Normalisation", "class_normalisation.html", "class_normalisation" ],
    [ "OrientationMap", "class_orientation_map.html", "class_orientation_map" ],
    [ "Preprocessing", "class_preprocessing.html", "class_preprocessing" ],
    [ "Segmentation", "class_segmentation.html", "class_segmentation" ],
    [ "settings", "structsettings.html", "structsettings" ],
    [ "Skeleton", "class_skeleton.html", "class_skeleton" ]
];