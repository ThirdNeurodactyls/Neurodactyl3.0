var searchData=
[
  ['compute_5fgaborkernels',['compute_gaborKernels',['../class_gabor.html#ab57ff73c9ff283763fa2304b49841ab9',1,'Gabor']]],
  ['compute_5fvarianceinblocks2d',['compute_varianceInBlocks2D',['../class_helpers.html#aa513941ad407e2cd95110d84e73f5d1c',1,'Helpers']]],
  ['converter',['Converter',['../class_converter.html#a1de81f3e06093411e5d27ce882bc010f',1,'Converter']]],
  ['createskeleton',['CreateSkeleton',['../class_skeleton.html#a8314ba9a9960d2b43cc65cc678e3a5a6',1,'Skeleton']]],
  ['cvmat_5fto_5fafarray',['cvMat_to_afArray',['../class_converter.html#ae8a4ed556ccf8e9abeccb75d1f887e72',1,'Converter']]],
  ['cvmat_5fto_5fqimage',['cvMat_to_QImage',['../class_converter.html#aac0dd940b05e318ab0d5eed8fc8ea92b',1,'Converter']]]
];
