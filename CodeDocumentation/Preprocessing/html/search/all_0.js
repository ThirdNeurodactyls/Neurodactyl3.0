var searchData=
[
  ['af_5farrayinfo',['af_arrayInfo',['../class_helpers.html#ae1e23a02595732f6bdf9b2c42ff481f4',1,'Helpers']]],
  ['af_5fimgin',['af_imgIn',['../class_gabor.html#a8b4e4107229f8397d2d5392da554da69',1,'Gabor::af_imgIn()'],['../class_preprocessing.html#a64385fd75c0effe858014f60751920fb',1,'Preprocessing::af_imgIn()']]],
  ['af_5fimgout',['af_imgOut',['../class_gabor.html#aad8cf9a48fc47a98d5ba887a0bfaa4dd',1,'Gabor::af_imgOut()'],['../class_preprocessing.html#a076a96f0609b55e4994ff64635458e38',1,'Preprocessing::af_imgOut()']]],
  ['afarray_5fto_5fcpp2darray',['afArray_to_cpp2Darray',['../class_converter.html#af7cad43c9ef69b7f0ba4d24ff2acc379',1,'Converter']]],
  ['afarray_5fto_5fcvmat',['afArray_to_cvMat',['../class_converter.html#a2e52aba963d8ce546bf500e024f9684c',1,'Converter']]],
  ['afarray_5fto_5fqimage',['afArray_to_QImage',['../class_converter.html#a8913be5740b47e05a06f564c16b83c7e',1,'Converter']]]
];
