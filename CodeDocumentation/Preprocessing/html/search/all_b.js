var searchData=
[
  ['removefilterartifacts',['removeFilterArtifacts',['../class_preprocessing.html#a488601ea7706fc66ad7ae53784e2ab14',1,'Preprocessing']]],
  ['run',['run',['../class_gabor.html#a8405c8c9f133945653a84f2a52676e64',1,'Gabor::run()'],['../class_normalisation.html#a1ca35cc9d1211eb5a3b713a24fd1339d',1,'Normalisation::run()'],['../class_orientation_map.html#a55ebdf797caeb1a7e8aa3fb5ffeaa412',1,'OrientationMap::run()'],['../class_preprocessing.html#a4afb81f514b04823e70b652dcf5d239f',1,'Preprocessing::run()'],['../class_segmentation.html#a146c39b0e823fa4b76909ca66354f9bf',1,'Segmentation::run()'],['../class_skeleton.html#a7e3a559b097f4a1436a903f6509d30d2',1,'Skeleton::run()']]]
];
