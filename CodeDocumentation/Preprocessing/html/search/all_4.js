var searchData=
[
  ['g',['g',['../class_preprocessing.html#a3f929b327fc26b030ddf1675eb4e1056',1,'Preprocessing']]],
  ['gabor',['Gabor',['../class_gabor.html',1,'Gabor'],['../class_gabor.html#a3fe3114495a15e8dda75c9a282b16fdd',1,'Gabor::Gabor()'],['../class_gabor.html#afd5f50a5ef0fccda39ea10852baacac8',1,'Gabor::Gabor(GaborSettings cpp_settings)']]],
  ['gabor_2ecpp',['gabor.cpp',['../gabor_8cpp.html',1,'']]],
  ['gabor_2eh',['gabor.h',['../gabor_8h.html',1,'']]],
  ['gabor_5fglobal_2eh',['gabor_global.h',['../gabor__global_8h.html',1,'']]],
  ['gaborsettings',['gaborSettings',['../structgabor_settings.html',1,'gaborSettings'],['../structgabor_settings.html#a8bc94ae6c3eb25bf5ad866aff0c407f0',1,'gaborSettings::gaborSettings()'],['../gabor_8h.html#a11b9b094544d17eeb876e38e915ba41f',1,'GaborSettings():&#160;gabor.h']]],
  ['gaborshared_5fexport',['GABORSHARED_EXPORT',['../gabor__global_8h.html#a9d263b6d053c341377faf356c128bad2',1,'gabor_global.h']]],
  ['getaf_5fimgin',['getAf_imgIn',['../class_gabor.html#aebf684d0022b13e8f611192504098811',1,'Gabor::getAf_imgIn()'],['../class_preprocessing.html#a0d8c665993d7704c8aaa5f1bdedbe1f3',1,'Preprocessing::getAf_imgIn()']]],
  ['getaf_5fimgout',['getAf_imgOut',['../class_gabor.html#ace5f1f3553736389277e3e8e8de82f24',1,'Gabor::getAf_imgOut()'],['../class_preprocessing.html#ab3a3997bfb99c8606d8502a1419b5377',1,'Preprocessing::getAf_imgOut()']]]
];
