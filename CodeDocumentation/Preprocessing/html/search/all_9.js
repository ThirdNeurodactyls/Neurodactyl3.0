var searchData=
[
  ['preprocessing',['Preprocessing',['../class_preprocessing.html',1,'Preprocessing'],['../class_preprocessing.html#ad162a3c39dd11ae2d40f00b5f5474901',1,'Preprocessing::Preprocessing()'],['../class_preprocessing.html#afb5c6c394359a1ce72189828cd88a7dd',1,'Preprocessing::Preprocessing(Settings cpp_settings)']]],
  ['preprocessing_2ecpp',['preprocessing.cpp',['../preprocessing_8cpp.html',1,'']]],
  ['preprocessing_2eh',['preprocessing.h',['../preprocessing_8h.html',1,'']]],
  ['preprocessing_5fglobal_2eh',['preprocessing_global.h',['../preprocessing__global_8h.html',1,'']]],
  ['preprocessingshared_5fexport',['PREPROCESSINGSHARED_EXPORT',['../preprocessing__global_8h.html#a80f43702825e4b288f95d488095865a6',1,'preprocessing_global.h']]],
  ['printtiming',['printTiming',['../class_gabor.html#aa4a547fbe6dd9d4f9bf68913c37e8199',1,'Gabor::printTiming()'],['../class_preprocessing.html#a23b2c58e84c761721a3182d774e113e5',1,'Preprocessing::printTiming()']]]
];
