var structgabor_settings =
[
    [ "gaborSettings", "structgabor_settings.html#a8bc94ae6c3eb25bf5ad866aff0c407f0", null ],
    [ "cpp_gabor_gamma", "structgabor_settings.html#acdb55d751822c37fed550c2cf252cb3c", null ],
    [ "cpp_gabor_kSize", "structgabor_settings.html#a5af59d1c8ac007cccc64b99c9a28805a", null ],
    [ "cpp_gabor_lambda", "structgabor_settings.html#ace930fb6c06572b1644f1b6f90690cb6", null ],
    [ "cpp_gabor_psi", "structgabor_settings.html#ae7c5e805d427aa0bb89d8dfa4749be23", null ],
    [ "cpp_gabor_sigma", "structgabor_settings.html#aefd84f7be47cd1d4914d761fdfa8f571", null ],
    [ "cpp_oMap_gaussKSize", "structgabor_settings.html#a9572f8508b01b114a711deec9195dec0", null ],
    [ "cpp_oMap_kSize", "structgabor_settings.html#aa8221421ddc68d045772ceb702eb207c", null ],
    [ "cpp_oMap_mode", "structgabor_settings.html#ad0890bcbb700b8268713b200fed9cc9b", null ],
    [ "cpp_oMap_useGauss", "structgabor_settings.html#a9fe06260efe8b974ccfc2819df455a44", null ]
];