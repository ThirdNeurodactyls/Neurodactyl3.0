var class_preprocessing =
[
    [ "Preprocessing", "class_preprocessing.html#ad162a3c39dd11ae2d40f00b5f5474901", null ],
    [ "Preprocessing", "class_preprocessing.html#afb5c6c394359a1ce72189828cd88a7dd", null ],
    [ "getAf_imgIn", "class_preprocessing.html#a0d8c665993d7704c8aaa5f1bdedbe1f3", null ],
    [ "getAf_imgOut", "class_preprocessing.html#ab3a3997bfb99c8606d8502a1419b5377", null ],
    [ "printTiming", "class_preprocessing.html#a23b2c58e84c761721a3182d774e113e5", null ],
    [ "removeFilterArtifacts", "class_preprocessing.html#a488601ea7706fc66ad7ae53784e2ab14", null ],
    [ "run", "class_preprocessing.html#a4afb81f514b04823e70b652dcf5d239f", null ],
    [ "setAf_imgIn", "class_preprocessing.html#a8941da0d5bf811a4264618ef32f0d038", null ],
    [ "af_imgIn", "class_preprocessing.html#a64385fd75c0effe858014f60751920fb", null ],
    [ "af_imgOut", "class_preprocessing.html#a076a96f0609b55e4994ff64635458e38", null ],
    [ "cpp_settings", "class_preprocessing.html#a983409d5e2d96767bce394d29812a851", null ],
    [ "cpp_useTiming", "class_preprocessing.html#a671908c2fd3b65f4d29faf777c0e6a19", null ],
    [ "g", "class_preprocessing.html#a3f929b327fc26b030ddf1675eb4e1056", null ],
    [ "timing", "class_preprocessing.html#af231fe859fa2514a269eb1d4cd45e0c2", null ]
];