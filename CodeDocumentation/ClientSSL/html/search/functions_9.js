var searchData=
[
  ['sendimage',['sendImage',['../class_client.html#a8f64e5632784307f9acf83d888065078',1,'Client']]],
  ['sendreply',['sendReply',['../class_client.html#a90441ae47c272467545f6178c51f53e9',1,'Client']]],
  ['sendstatusmessage',['sendStatusMessage',['../class_client.html#af97c4e8553703ba6f4548028c7e87f82',1,'Client']]],
  ['sendtext',['sendText',['../class_client.html#a814cf00a35fe5394263414d6dbbb124c',1,'Client']]],
  ['setpassword',['setPassword',['../class_login_dialog.html#a159da6196e3e9ff8f31d857f6466ee06',1,'LoginDialog']]],
  ['setupdialog',['setUpDialog',['../class_default_dialog.html#a7d471697b5bbbf041955ed7c3d4da131',1,'DefaultDialog::setUpDialog()'],['../class_login_dialog.html#aefb7adf28d5e3b4bb2a093dfbf4d9c29',1,'LoginDialog::setUpDialog()'],['../class_register_dialog.html#a6fbbb31c57beec97768bed352159cef1',1,'RegisterDialog::setUpDialog()']]],
  ['setusername',['setUsername',['../class_login_dialog.html#a1c37fa69509756eb5f815064de14f191',1,'LoginDialog']]],
  ['setusernamelist',['setUsernameList',['../class_login_dialog.html#a525c6a694b4639c3b5cec06812a4ea89',1,'LoginDialog']]],
  ['sslerr',['sslErr',['../class_client.html#a48ae4df8c4318f356e045a637cc28fe6',1,'Client']]]
];
