var class_server =
[
    [ "Server", "class_server.html#a1950ac036d86af898428d7ba39bbf048", null ],
    [ "clientsChanged", "class_server.html#a3da221f7298d444e5cd5ac2d24bb7179", null ],
    [ "disconnectClient", "class_server.html#a92c5823f86c7f1081e40850552565584", null ],
    [ "incomingConnection", "class_server.html#a3952d37b7bc53a4377f411d9ccaa9d62", null ],
    [ "logInStatus", "class_server.html#a9ea9fac039a8ec425006a18f290e3b67", null ],
    [ "registrationStatus", "class_server.html#a76ccc777c065e9ee23509b77c61be2d5", null ],
    [ "sendAllData", "class_server.html#a9d565bfcb56ce41f66f0dceb39c3dc91", null ],
    [ "sendLoginData", "class_server.html#a0334df77fbe27916f3146fa6abe20422", null ],
    [ "sendNewClients", "class_server.html#a4e7605e4c6ed083b78d6b49064400374", null ],
    [ "sendStopServer", "class_server.html#a09fc34723caf0102dcf85ebc08455a99", null ],
    [ "serverStarted", "class_server.html#a33ecbf552556b08a180c28ff4887c276", null ],
    [ "serverStopped", "class_server.html#a88939467463534026b5738a59f03ecf5", null ],
    [ "showStatusMessage", "class_server.html#a8668ec2e85cede2cb470db1a606aa116", null ],
    [ "startServer", "class_server.html#ab33f0388b40cff9e5a2a0bea4b8b45b2", null ],
    [ "uiFilteredShow", "class_server.html#aa8ea86d084aa9ea2530e19e68df1421d", null ],
    [ "uiImageShow", "class_server.html#a6619ce945541519ae2d596703c5e9c74", null ],
    [ "uiTextUpdate", "class_server.html#a501eec62005448b5f08e13748945a44d", null ],
    [ "updateClient", "class_server.html#a01647b5cb4a8859c4c99694e1d997878", null ]
];