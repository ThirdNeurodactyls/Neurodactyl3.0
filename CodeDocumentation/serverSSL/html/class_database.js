var class_database =
[
    [ "Database", "class_database.html#a4703c80e6969d33565ea340f768fdadf", null ],
    [ "closeConnection", "class_database.html#a0075810141faa0c57c5d80c03a3025ac", null ],
    [ "createConnection", "class_database.html#ae695b0e78a60fdc43e810b2cf0110299", null ],
    [ "createUser", "class_database.html#ab9d3a37703190aa1715d015ca989ca6d", null ],
    [ "getFingerprints", "class_database.html#ac5bbeee41378c905b8cccdb487b1f9c9", null ],
    [ "getSingUpRequest", "class_database.html#a6a865e066f53f8622d8deb70a96df309", null ],
    [ "logInUser", "class_database.html#ae1fe07979e285926c60c1da6bec017b6", null ],
    [ "logInUserFingerprint", "class_database.html#a5828ac63d7606500566d5e21c08eb109", null ],
    [ "saveFingerprint", "class_database.html#a054c13b3b91e40cd1ba2b2110190e22c", null ],
    [ "updateUserStatus", "class_database.html#aa98451a4b2fd797bc50c43cdffc47dd7", null ]
];